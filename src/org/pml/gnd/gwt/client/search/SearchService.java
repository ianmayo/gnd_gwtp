package org.pml.gnd.gwt.client.search;

import java.util.Map;

import org.bitbucket.es4gwt.client.result.SearchResult;
import org.bitbucket.es4gwt.client.result.SearchResultDiff;
import org.bitbucket.es4gwt.client.result.SearchResultJSON;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.bitbucket.es4gwt.shared.spec.SearchRequestConverter;
import org.pml.gnd.gwt.client.event.SearchResultChangeEvent;
import org.pml.gnd.gwt.client.model.Facet;

import com.google.common.collect.Maps;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.Dictionary;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class SearchService {

	private static final String ES_HOST = Dictionary.getDictionary("URL").get("ElasticSearch");
	private static final String BASE_URL = ES_HOST + "/data/dataset/_search?";
	private static final String STATIC_URL_PARAMS = "pretty=true&source=";
	private static final String SEARCH_URL = BASE_URL + STATIC_URL_PARAMS;

	private final Map<SearchRequest, SearchResult> cache = Maps.newHashMap();
	private final EventBus eventBus;

	private SearchResult cachedFacets;
	private SearchResult cachedTrialFacet;

	@Inject
	public SearchService(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public void send(final SearchRequest request, final AsyncCallback<SearchResult> callback) {
		if (cache.containsKey(request)) {
			// return cached result
			final SearchResult cachedResult = cache.get(request);
			callback.onSuccess(cachedResult);
			// send request anyway to update cache and notify users if the result has changed
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(	SEARCH_URL + SearchRequestConverter.toRequestString(request,
																						Facet.SEARCH_PARAMS,
																						Facet.RESULTS_FIELDS),
									new AsyncCallback<SearchResultJSON>() {

										@Override
										public void onSuccess(SearchResultJSON result) {
											// update cache anyway in case we didn't detect a change
											cache.put(request, result);
											// For search result change notifications testing
											// result.getHit(1).setId("newid");
											SearchResultDiff diff = new SearchResultDiff(cachedResult, result);
											if (diff.hasChanges()) {
												SearchResultChangeEvent.fire(eventBus, request, result);
											}
										}

										@Override
										public void onFailure(Throwable caught) {
											GWT.log("Cache update failed", caught);
										}
									});

		} else {
			// init cache for this specific request
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(	SEARCH_URL + SearchRequestConverter.toRequestString(request,
																						Facet.SEARCH_PARAMS,
																						Facet.RESULTS_FIELDS),
									new AsyncCallback<SearchResultJSON>() {

										@Override
										public void onSuccess(SearchResultJSON result) {
											cache.put(request, result);
											callback.onSuccess(result);
										}

										@Override
										public void onFailure(Throwable caught) {
											callback.onFailure(caught);
										}
									});

		}
	}

	public void facetsOnly(final AsyncCallback<SearchResult> callback) {
		if (cachedFacets == null) {
			// init cache
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(	SEARCH_URL + SearchRequestConverter.facetsOnly(Facet.SEARCH_PARAMS),
									new AsyncCallback<SearchResultJSON>() {

										@Override
										public void onSuccess(SearchResultJSON result) {
											cachedFacets = result;
											callback.onSuccess(cachedFacets);
										}

										@Override
										public void onFailure(Throwable caught) {
											callback.onFailure(caught);
										}

									});
		} else
			callback.onSuccess(cachedFacets);
	}

	public void trialFacet(final AsyncCallback<SearchResult> callback) {
		if (cachedTrialFacet == null) {
			// init cache
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(	SEARCH_URL + SearchRequestConverter.facetsOnly(Facet.TRIAL),
									new AsyncCallback<SearchResultJSON>() {

										@Override
										public void onSuccess(SearchResultJSON result) {
											cachedTrialFacet = result;
											callback.onSuccess(result);
										}

										@Override
										public void onFailure(Throwable caught) {
											callback.onFailure(caught);
										}

									});
		} else
			callback.onSuccess(cachedTrialFacet);
	}

}
