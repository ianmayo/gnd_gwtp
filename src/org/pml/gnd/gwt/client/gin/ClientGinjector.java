package org.pml.gnd.gwt.client.gin;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.place.DocumentIdSetInUrlGatekeeper;
import org.pml.gnd.gwt.client.ui.document.DocumentMainPresenter;
import org.pml.gnd.gwt.client.ui.document.InfoPresenter;
import org.pml.gnd.gwt.client.ui.document.TablePresenter;
import org.pml.gnd.gwt.client.ui.main.HomePresenter;
import org.pml.gnd.gwt.client.ui.main.MainPresenter;
import org.pml.gnd.gwt.client.ui.search.SearchMainPresenter;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.inject.client.AsyncProvider;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import org.pml.gnd.gwt.client.ui.document.GraphPresenter;
import org.pml.gnd.gwt.client.ui.document.ChartPresenter;
import org.pml.gnd.gwt.client.ui.multidoc.MultidocMapPresenter;

@GinModules({ ClientModule.class })
public interface ClientGinjector extends Ginjector {

	// ////////
	// GWTP //
	// //////

	EventBus getEventBus();

	PlaceManager getPlaceManager();

	// /////////////
	// Datastore //
	// ///////////

	DataStore getIDataStore();

	// ///////////////
	// GateKeepers //
	// /////////////

	DocumentIdSetInUrlGatekeeper getDocumentIdSetInUrlGatekeeper();

	// //////////////
	// Presenters //
	// ////////////

	Provider<MainPresenter> getMainPresenter();

	Provider<HomePresenter> getHomePresenter();

	AsyncProvider<SearchMainPresenter> getSearchMainPresenter();

	AsyncProvider<DocumentMainPresenter> getDocumentMainPresenter();

	AsyncProvider<InfoPresenter> getInfoPresenter();

	AsyncProvider<TablePresenter> getTablePresenter();

	AsyncProvider<GraphPresenter> getGraphPresenter();

	AsyncProvider<ChartPresenter> getChartPresenter();

	AsyncProvider<MultidocMapPresenter> getMultidocMapPresenter();

}
