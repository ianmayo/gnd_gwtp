package org.pml.gnd.gwt.client.gin;

import org.pml.gnd.gwt.client.datastore.CouchDbDataStore;
import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.place.ClientPlaceManager;
import org.pml.gnd.gwt.client.place.DefaultPlace;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.search.SearchService;
import org.pml.gnd.gwt.client.ui.cart.ManageCartPresenter;
import org.pml.gnd.gwt.client.ui.cart.ManageCartView;
import org.pml.gnd.gwt.client.ui.document.ChartPresenter;
import org.pml.gnd.gwt.client.ui.document.ChartView;
import org.pml.gnd.gwt.client.ui.document.DocumentMainPresenter;
import org.pml.gnd.gwt.client.ui.document.DocumentMainView;
import org.pml.gnd.gwt.client.ui.document.GraphPresenter;
import org.pml.gnd.gwt.client.ui.document.GraphView;
import org.pml.gnd.gwt.client.ui.document.InfoPresenter;
import org.pml.gnd.gwt.client.ui.document.InfoView;
import org.pml.gnd.gwt.client.ui.document.TablePresenter;
import org.pml.gnd.gwt.client.ui.document.TableView;
import org.pml.gnd.gwt.client.ui.main.HomePresenter;
import org.pml.gnd.gwt.client.ui.main.HomeView;
import org.pml.gnd.gwt.client.ui.main.MainPresenter;
import org.pml.gnd.gwt.client.ui.main.MainView;
import org.pml.gnd.gwt.client.ui.multidoc.MultidocMapPresenter;
import org.pml.gnd.gwt.client.ui.multidoc.MultidocMapView;
import org.pml.gnd.gwt.client.ui.search.PermalinkDialogPresenter;
import org.pml.gnd.gwt.client.ui.search.PermalinkDialogView;
import org.pml.gnd.gwt.client.ui.search.ResultChangeNotifierPresenter;
import org.pml.gnd.gwt.client.ui.search.ResultChangeNotifierView;
import org.pml.gnd.gwt.client.ui.search.ResultTablePresenter;
import org.pml.gnd.gwt.client.ui.search.ResultTableView;
import org.pml.gnd.gwt.client.ui.search.SearchFormPresenter;
import org.pml.gnd.gwt.client.ui.search.SearchFormView;
import org.pml.gnd.gwt.client.ui.search.SearchMainPresenter;
import org.pml.gnd.gwt.client.ui.search.SearchMainView;
import org.pml.gnd.gwt.client.ui.search.TimelinePresenter;
import org.pml.gnd.gwt.client.ui.search.TimelineView;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;

public class ClientModule extends AbstractPresenterModule
{

	@Override
	protected void configure()
	{

		// //////////
		// Places //
		// ////////

		install(new DefaultModule(ClientPlaceManager.class));
		bindConstant().annotatedWith(DefaultPlace.class).to(NameTokens.home);

		// /////////////
		// Datastore //
		// ///////////

		bind(DataStore.class).to(CouchDbDataStore.class);
		bind(SearchService.class).asEagerSingleton();

		// /////////////////////
		// Presenter Widgets //
		// ///////////////////

		bindSingletonPresenterWidget(ResultTablePresenter.class,
				ResultTablePresenter.MyView.class, ResultTableView.class);

		bindSingletonPresenterWidget(TimelinePresenter.class,
				TimelinePresenter.MyView.class, TimelineView.class);
		
		bindSingletonPresenterWidget(org.pml.gnd.gwt.client.ui.search.ChartPresenter.class,
				org.pml.gnd.gwt.client.ui.search.ChartPresenter.MyView.class, org.pml.gnd.gwt.client.ui.search.ChartView.class);

		bindSingletonPresenterWidget(SearchFormPresenter.class,
				SearchFormPresenter.MyView.class, SearchFormView.class);

		// ////////////////
		// Dialog Boxes //
		// //////////////

		bindPresenterWidget(ManageCartPresenter.class,
				ManageCartPresenter.MyView.class, ManageCartView.class);

		bindPresenterWidget(PermalinkDialogPresenter.class,
				PermalinkDialogPresenter.MyView.class, PermalinkDialogView.class);

		bindSingletonPresenterWidget(ResultChangeNotifierPresenter.class,
				ResultChangeNotifierPresenter.MyView.class,
				ResultChangeNotifierView.class);

		// //////////////
		// Presenters //
		// ////////////

		bindPresenter(MainPresenter.class, MainPresenter.MyView.class,
				MainView.class, MainPresenter.MyProxy.class);

		bindPresenter(HomePresenter.class, HomePresenter.MyView.class,
				HomeView.class, HomePresenter.MyProxy.class);

		bindPresenter(SearchMainPresenter.class, SearchMainPresenter.MyView.class,
				SearchMainView.class, SearchMainPresenter.MyProxy.class);

		bindPresenter(DocumentMainPresenter.class,
				DocumentMainPresenter.MyView.class, DocumentMainView.class,
				DocumentMainPresenter.MyProxy.class);

		bindPresenter(InfoPresenter.class, InfoPresenter.MyView.class,
				InfoView.class, InfoPresenter.MyProxy.class);

		bindPresenter(TablePresenter.class, TablePresenter.MyView.class,
				TableView.class, TablePresenter.MyProxy.class);

		bindPresenter(GraphPresenter.class, GraphPresenter.MyView.class,
				GraphView.class, GraphPresenter.MyProxy.class);

		bindPresenter(ChartPresenter.class, ChartPresenter.MyView.class,
				ChartView.class, ChartPresenter.MyProxy.class);

		bindPresenter(MultidocMapPresenter.class,
				MultidocMapPresenter.MyView.class, MultidocMapView.class,
				MultidocMapPresenter.MyProxy.class);
	}
}
