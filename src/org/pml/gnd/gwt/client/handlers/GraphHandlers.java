package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface GraphHandlers extends UiHandlers {

	void onLeftAxisBoxChange(String value);

	void onRightAxisBoxChange(String value);

	void onPermalinkClick();

}
