package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface SearchResultTableHandlers extends UiHandlers {

	void itemSelected();

	void itemDeselected();

	void headerClicked(boolean selected);

	String getTarget(String id);

}
