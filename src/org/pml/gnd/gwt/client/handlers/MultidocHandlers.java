package org.pml.gnd.gwt.client.handlers;

import org.pml.gnd.gwt.client.ui.multidoc.DiaryEntry;
import org.pml.gnd.gwt.client.ui.multidoc.LegendItem;

import com.gwtplatform.mvp.client.UiHandlers;

public interface MultidocHandlers extends UiHandlers
{

	void onLegendItemSelected(LegendItem selectedObject);

	void onDiaryItemSelected(DiaryEntry selectedObject);

	void onSliderValueChange();

	void onPermalinkClick();

}
