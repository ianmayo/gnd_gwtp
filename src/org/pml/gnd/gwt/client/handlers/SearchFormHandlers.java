package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface SearchFormHandlers extends UiHandlers {

	void searchParamsChanged();

}
