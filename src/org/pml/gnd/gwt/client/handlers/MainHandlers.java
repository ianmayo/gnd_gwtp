package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface MainHandlers extends UiHandlers {

	void onShoppingCartIconClick();

	void onSearchSubmit();

}
