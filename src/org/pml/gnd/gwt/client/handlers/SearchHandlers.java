package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface SearchHandlers extends UiHandlers {

	void addToCartButtonPushed();

	void onSaveSearchLinkClick();

	void onShowTable();

	void onShowTimeLine();

	void onShowChart();

}
