package org.pml.gnd.gwt.client.handlers;

import java.util.HashMap;

import org.pml.gnd.gwt.client.model.Facet;

import com.gwtplatform.mvp.client.UiHandlers;

public interface InfoHandlers extends UiHandlers {

	void submitDocument(HashMap<Facet, String> newDataSet);

}
