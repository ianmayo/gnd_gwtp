package org.pml.gnd.gwt.client.handlers;

import com.gwtplatform.mvp.client.UiHandlers;

public interface ChartHandlers extends UiHandlers
{
	void addToShoppingCart(String id);
}
