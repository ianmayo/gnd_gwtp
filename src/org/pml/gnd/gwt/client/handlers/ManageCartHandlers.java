package org.pml.gnd.gwt.client.handlers;

import org.bitbucket.es4gwt.client.result.ResultTableElement;

import com.gwtplatform.mvp.client.UiHandlers;

public interface ManageCartHandlers extends UiHandlers {

	void removeItemButtonClicked(ResultTableElement item);

	void removeAllConfirmed();

	void onViewButtonClick();

}
