package org.pml.gnd.gwt.client.datastore;

import org.pml.gnd.gwt.client.model.CouchView;

import com.google.gwt.core.client.JavaScriptObject;

class CouchDbView extends JavaScriptObject implements CouchView {

	protected CouchDbView() {
	}

	@Override
	public final native String getId() /*-{
		return this.id;
	}-*/;

	@Override
	public final native String getValue() /*-{
		return this.value;
	}-*/;

}
