/**
 * 
 */
package org.pml.gnd.gwt.client.datastore;

import java.util.HashMap;
import java.util.List;

import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.model.Metadata;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author othman
 */
public interface DataStore {

	void getRecentDocuments(AsyncCallback<Document> callback);

	void getDocumentById(String id, AsyncCallback<Document> callback);

	void getDocumentMetadata(String id, AsyncCallback<Metadata> callback);

	void getDocumentsByIds(List<String> keys, AsyncCallback<Document> callback);

	void updateDocument(Document document,
			HashMap<Facet, String> newDataSet, AsyncCallback<Document> asyncCallback);
}
