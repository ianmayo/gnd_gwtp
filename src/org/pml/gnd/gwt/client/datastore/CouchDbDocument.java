package org.pml.gnd.gwt.client.datastore;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.pml.gnd.gwt.client.model.DataEntry;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Location;
import org.pml.gnd.gwt.client.model.Metadata;
import org.pml.gnd.gwt.client.model.Narrative;
import org.pml.gnd.gwt.client.model.Track;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

class CouchDbDocument extends JavaScriptObject implements Document
{

	protected CouchDbDocument()
	{
	}

	@Override
	public final native String getId() /*-{
		return this._id;
	}-*/;

	@Override
	public final JSONArray getMeasurement(String measurement)
	{
		JSONObject json = new JSONObject(this);
		JSONValue val = json.get(measurement);
		if (val == null)
		{
			return null;
		}
		return val.isArray();
	}

	private boolean hasMeasurement(String measurement)
	{
		return new JSONObject(this).containsKey(measurement);
	}

	private JSONObject getMeasurementObject(String measurement)
	{
		JSONObject json = new JSONObject(this);
		JSONValue val = json.get(measurement);
		if (val == null)
		{
			return null;
		}
		return val.isObject();
	}

	@Override
	public final native Metadata getMetadata() /*-{
		return this.metadata;
	}-*/;

	@Override
	public final native Location getLocation() /*-{
		return this.location;
	}-*/;

	@Override
	public final Iterator<DataEntry> timeIterator()
	{
		List<String> time = Lists.newArrayList();
		time.add(TIME);
		return new DataIterator(Predicates.in(time));
	}

	@Override
	public final Iterator<DataEntry> dataIterator()
	{
		return new DataIterator();
	}

	@Override
	public final Iterator<DataEntry> trackDataIterator()
	{
		List<String> trackDataTypes = Lists.newArrayList();
		trackDataTypes.add(TIME);
		trackDataTypes.add(Track.LATITUDE);
		trackDataTypes.add(Track.LONGITUDE);
		trackDataTypes.add(LOCATION);
		return new DataIterator(Predicates.in(trackDataTypes));
	}

	@Override
	public final Iterator<DataEntry> narrativeDataIterator()
	{
		List<String> narrativeDataTypes = Lists.newArrayList();
		narrativeDataTypes.add(TIME);
		narrativeDataTypes.add(Narrative.OBSERVATION);
		return new DataIterator(Predicates.in(narrativeDataTypes));
	}

	private class DataIterator implements Iterator<DataEntry>
	{

		private int i = 0;
		private final int size;
		private final Map<String, JSONArray> measurements = Maps.newHashMap();
		private final Predicate<String> datatypeFilter;

		DataIterator()
		{
			datatypeFilter = Predicates.alwaysTrue();
			size = getMeasurement(Document.TIME).size();
			loadDataTypes();
		}

		DataIterator(Predicate<String> datatypeFilter)
		{
			this.datatypeFilter = datatypeFilter;
			size = getMeasurement(Document.TIME).size();
			loadDataTypes();
			
			//handle the new specification
			if (hasMeasurement(LOCATION) && datatypeFilter.apply(LOCATION))
			{
				//get location json
				CouchDbLocation location = (CouchDbLocation) getMeasurementObject(
						LOCATION).getJavaScriptObject();
				
				//check if number of timestamps is same as number of coordinates.
				Preconditions.checkState(location.getNumberOfCoordinates() == size);
				
				if (size > 0)
				{
					JSONArray latitude = new JSONArray();
					JSONArray longitude = new JSONArray();
					
					//populate array of latitude and longitude so that existing logic in tracklayer can process these arrays.
					//TODO after we phase out old specification, we can refactor logic in TrackLayer to work only for new format.
					
					for (int i = 0; i < size; i++)
					{
						latitude.set(i, new JSONNumber(location.getLatitude(i)));
						longitude.set(i, new JSONNumber(location.getLongitude(i)));
					}
					measurements.put(Track.LATITUDE, latitude);
					measurements.put(Track.LONGITUDE, longitude);
				}
			}
		}

		private void loadDataTypes()
		{
			int numCols = getMetadata().getDatatypeCount();
			for (int i = 0; i < numCols; i++)
			{
				String datatype = getMetadata().getDatatype(i);
				if (datatypeFilter.apply(datatype))
				{
					JSONArray measurement = getMeasurement(datatype);
					if (measurement != null)
					{
						Preconditions.checkState(measurement.size() == size);
						measurements.put(datatype, measurement);
					}
				}
			}
		}

		@Override
		public boolean hasNext()
		{
			return i < size;
		}

		@Override
		public DataEntry next()
		{
			if (!hasNext())
				throw new IndexOutOfBoundsException("Trying to access index " + (i + 1)
						+ ", max index : " + (size - 1));
			return new CouchDbDataEntry(i++);
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}

		private class CouchDbDataEntry implements DataEntry
		{

			final int i;

			public CouchDbDataEntry(int i)
			{
				this.i = i;
			}

			private JSONValue getMeasurement(String dataType)
			{
				return measurements.get(dataType).get(i);
			}

			@Override
			public String getMeasurementAsString(String dataType)
			{
				JSONValue value = getMeasurement(dataType);
				JSONString valueAsString = value.isString();
				if (valueAsString != null)
					return valueAsString.stringValue();
				else
					return value.toString();
			}

			@Override
			public Double getMeasurementAsDouble(String dataType)
			{
				JSONNumber valueAsNumber = getMeasurement(dataType).isNumber();
				if (valueAsNumber == null)
					throw new IllegalArgumentException("Data Type " + dataType
							+ " is not a number");
				return valueAsNumber.doubleValue();
			}

			@Override
			public Date getTime()
			{
				return DateFormats.CUSTOM.parse(getMeasurementAsString(Document.TIME));
			}

			@Override
			public boolean containsKey(String dataType)
			{
				return measurements.containsKey(dataType);
			}
		}
	}

	@Override
	public final Date getLastTimeMeasurement()
	{
		JSONArray times = getMeasurement(Document.TIME);
		return DateFormats.CUSTOM.parse(times.get(times.size() - 1).isString()
				.stringValue());
	}

}
