/**
 * 
 */
package org.pml.gnd.gwt.client.datastore;

import java.util.Iterator;
import java.util.List;

import org.pml.gnd.gwt.client.model.GeoBounds;
import org.pml.gnd.gwt.client.model.Metadata;
import org.pml.gnd.gwt.client.model.Narrative;
import org.pml.gnd.gwt.client.model.Track;

import com.google.common.collect.Lists;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * 
 * Dataset TO object "metadata" : { "data_type" : ["lat", "lon", "time"], "name" : "bike25" "platform" : "vehicle 1232",
 * "platform_type" : "bike", "trial" : "2012/Feb/1433", "sensor" : "GARMIN-GO300", "type" : "track", "sensor_type" :
 * "GPS", "geo_bounds" : { "tl":[50.3, -2.4],"br":[50.1, -2.1] }, "time_bounds" : { "start": "2012-02-27T14:46:02+0000",
 * "end":"2012-02-27T14:55:00+0000"} }
 * 
 * @author othman
 * 
 */
class CouchDbMetadata extends JavaScriptObject implements Metadata {

	protected CouchDbMetadata() {
	}

	@Override
	public final String getId() {
		return getPlatform() + "-" + getTrial();
	}

	@Override
	public final native int getDatatypeCount() /*-{
		if (this.data_type == null) {
			return 0;
		}
		return this.data_type.length;
	}-*/;

	@Override
	public final native String getDatatype(int index) /*-{
		return this.data_type[index];
	}-*/;

	@Override
	public final native String getPlatform() /*-{
		return this.platform;
	}-*/;

	@Override
	public final native String getName() /*-{
		return this.name;
	}-*/;

	@Override
	public final native String getPlatformType() /*-{
		return this.platform_type;
	}-*/;

	@Override
	public final native String getTrial() /*-{
		return this.trial;
	}-*/;

	@Override
	public final native String getSensor() /*-{
		return this.sensor;
	}-*/;

	@Override
	public final native String getSensorType() /*-{
		return this.sensor_type;
	}-*/;

	@Override
	public final native String getType() /*-{
		return this.type;
	}-*/;

	@Override
	public final native GeoBounds getGeoBounds() /*-{
		return this.geo_bounds;
	}-*/;

	@Override
	public final native String getTimeBoundsStart() /*-{
		if (this.time_bounds != null)
			return this.time_bounds.start;
		else
			return null;
	}-*/;

	@Override
	public final native String getTimeBoundsEnd() /*-{
		if (this.time_bounds != null)
			return this.time_bounds.end;
		else
			return null;
	}-*/;

	@Override
	public final List<String> listDataTypes() {
		List<String> toReturn = Lists.newArrayList();
		for (Iterator<String> i = dataTypes(); i.hasNext();)
			toReturn.add(i.next());
		return toReturn;
	}

	@Override
	public final Iterator<String> dataTypes() {
		return new DataTypeIterator();
	}

	private final class DataTypeIterator implements Iterator<String> {

		private int current = 0;

		@Override
		public boolean hasNext() {
			return current < getDatatypeCount();
		}

		@Override
		public String next() {
			if (!hasNext())
				throw new IndexOutOfBoundsException();
			return getDatatype(current++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	@Override
	public final boolean isTrack() {
		Iterator<String> i = dataTypes();
		while (i.hasNext()) {
			String dataType = i.next();
			if (dataType.equals(Track.LATITUDE) || dataType.equals(Track.LONGITUDE))
				return true;
		}
		return false;
	}

	@Override
	public final boolean isNarrative() {
		Iterator<String> i = dataTypes();
		while (i.hasNext()) {
			String dataType = i.next();
			if (dataType.equals(Narrative.OBSERVATION))
				return true;
		}
		return false;
	}

}
