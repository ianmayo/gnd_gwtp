/**
 * 
 */
package org.pml.gnd.gwt.client.datastore;

import java.util.HashMap;
import java.util.Map;

import org.pml.gnd.gwt.client.model.Document;

import com.google.inject.Singleton;

/**
 * @author othman
 * 
 */
@Singleton
class DocumentCache {

	private Map<String, Document> cachedDoc = new HashMap<String, Document>();

	// TODO create constructor that gives some indication of how many docs to store. we may wish to tune this value,
	// TODO track "first in, first out" so that we ditch the least recently used documents.

	/**
	 * @return the cachedDoc
	 */
	public Document getCachedDoc(String id) {
		return cachedDoc.get(id);
	}

	/**
	 * store the document in the cache
	 * 
	 * @param id
	 * @param document
	 */
	public void storeDocument(final String id, final Document document) {
		cachedDoc.put(id, document);
	}

	public boolean contains(String id) {
		return cachedDoc.containsKey(id);
	}

}
