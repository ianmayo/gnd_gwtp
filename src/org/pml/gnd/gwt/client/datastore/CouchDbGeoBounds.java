package org.pml.gnd.gwt.client.datastore;

import org.pml.gnd.gwt.client.model.GeoBounds;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset TO object "metadata" : { "data_type" : ["lat", "lon", "time"], "platform" : "vehicle 1232", "platform_type" :
 * "bike", "trial" : "2012/Feb/1433", "sensor" : "GARMIN-GO300", "type" : "track", "sensor_type" : "GPS", "geo_bounds" :
 * { "tl":[50.3, -2.4],"br":[50.1, -2.1] }, "time_bounds" : { "start": "2012-02-27T14:46:02+0000",
 * "end":"2012-02-27T14:55:00+0000"} }
 * 
 * @author Munawar
 * 
 */
class CouchDbGeoBounds extends JavaScriptObject implements GeoBounds {

	protected CouchDbGeoBounds() {
	}

	public final native int getTotalTl() /*-{
		if (this.tl == null) {
			return 0;
		}
		return this.tl.length;
	}-*/;

	public final native String getTl(int index) /*-{
		return this.tl[index];
	}-*/;

	public final native int getTotalBr() /*-{
		if (this.br == null) {
			return 0;
		}
		return this.br.length;
	}-*/;

	public final native String getBr(int index) /*-{
		return this.br[index];
	}-*/;
}