/**
 * 
 */
package org.pml.gnd.gwt.client.datastore;

import org.pml.gnd.gwt.client.model.Location;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * 
 * Dataset TO object "location" : {"type": "MultiPoint","coordinates": [ [1.1,
 * 3.1],[2.5, 4.4],[1.6,5,4]]}
 * 
 * @author Akash
 * 
 */
class CouchDbLocation extends JavaScriptObject implements Location
{
	protected CouchDbLocation()
	{
	}
	
	@Override
	public final native String getType() /*-{
		return this.type;
	}-*/;

	@Override
	public final native String getCoordinates() /*-{
		return this.coordinates;
	}-*/;

	@Override
	public final native int getNumberOfCoordinates()/*-{
		if (this.coordinates == null) {
			return 0;
		}
		return this.coordinates.length;
	}-*/;
	
	@Override
	public final native double getLatitude(int index) /*-{
		return this.coordinates[index][0];
	}-*/;

	@Override
	public final native double getLongitude(int index) /*-{
		return this.coordinates[index][1];
	}-*/;

}
