/**
 * 
 */
package org.pml.gnd.gwt.client.datastore;

import java.util.HashMap;
import java.util.List;

import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.model.Metadata;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.Dictionary;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

/**
 * concrete implementation of datastore.
 * 
 * @author othman
 * 
 */
@Singleton
public class CouchDbDataStore implements DataStore
{

	private static final String COUCH_DB_HOST = Dictionary.getDictionary("URL")
			.get("CouchDB");
	private static final String WHATS_NEW_DATASET_URL = COUCH_DB_HOST
			+ "/tracks/_design/tracks/_view/track_listing?limit=10";
	private static final String COUCH_DB_DOCUMENT_URL = COUCH_DB_HOST
			+ "/tracks/";
	private static final String COUCH_DB_DOCUMENTS_URL = COUCH_DB_DOCUMENT_URL
			+ "_all_docs?keys=";
	private static final String COUCH_DB_INCLUDE_DOCUMENTS = "&include_docs=true";
	private static final String COUCH_DB_DOCUMENT_METADATA_URL = COUCH_DB_HOST
			+ "/tracks/_design/tracks/_show/metadata/";

	private final DocumentCache _docCache = new DocumentCache();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.pml.gnd.gwt.client.datastore.IDataStore#getRecentDocuments(com.google
	 * .gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void getRecentDocuments(final AsyncCallback<Document> callback)
	{

		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder
				.requestObject(WHATS_NEW_DATASET_URL, new IDocumentWrapper(callback));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.pml.gnd.gwt.client.datastore.IDataStore#getDocumentById(java.lang
	 * .String , com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void getDocumentById(final String id,
			final AsyncCallback<Document> callback)
	{
		if (id == null)
		{
			GWT.log("MISTAKENLY TRYING TO RETRIEVE NULL DOCUMENT");
			return;
		}

		if (isCached(id))
		{
			callback.onSuccess(cached(id));
		}
		else
		{
			// not in cache, we'll have to fetch it then
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(COUCH_DB_DOCUMENT_URL + id, new IDocumentWrapper(
					callback)
			{

				@Override
				public void onSuccess(CouchDbDocument result)
				{
					// store our local copy
					_docCache.storeDocument(id, result);

					// pass on the good news
					super.onSuccess(result);
				}
			});
		}

	}

	@Override
	public void getDocumentsByIds(List<String> keys,
			final AsyncCallback<Document> callback)
	{
		if (keys.size() == 0)
		{
			GWT.log("MISTAKENLY TRYING TO RETRIEVE NULL DOCUMENT");
			return;
		}

		JSONArray array = new JSONArray();
		for (int keyIndex = 0; keyIndex < keys.size(); keyIndex++)
		{
			if (isCached(keys.get(keyIndex)))
			{
				callback.onSuccess(cached(keys.get(keyIndex)));
			}
			else
			{
				array.set(array.size(), new JSONString(keys.get(keyIndex)));
			}
		}

		if (array.size() > 0)
		{

			String url = URL.encode(COUCH_DB_DOCUMENTS_URL + array.toString()
					+ COUCH_DB_INCLUDE_DOCUMENTS);

			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.requestObject(url, new AsyncCallback<JavaScriptObject>()
			{

				@Override
				public void onFailure(Throwable caught)
				{
					callback.onFailure(caught);
				}

				@Override
				public void onSuccess(JavaScriptObject result)
				{

					JSONArray documentsArray = new JSONObject(result).get("rows")
							.isArray();

					// store our local copy
					for (int i = 0; i < documentsArray.size(); i++)
					{
						JSONObject docJSONObject = documentsArray.get(i).isObject();
						if (docJSONObject.containsKey("error"))
						{
							// throw exception for invalid key.
							callback.onFailure(new Exception(docJSONObject.get("key")
									.toString() + " key not found."));
						}
						else
						{
							CouchDbDocument document = (CouchDbDocument) docJSONObject
									.get("doc").isObject().getJavaScriptObject();
							_docCache.storeDocument(document.getId(), document);

							// pass on the good news
							callback.onSuccess(document);
						}
					}
				}
			});
		}
	}

	@Override
	public void getDocumentMetadata(String id, AsyncCallback<Metadata> callback)
	{
		if (isCached(id))
		{
			callback.onSuccess(cached(id).getMetadata());
		}
		else
		{
			// not in cache, we'll have to fetch it then
			JsonpRequestBuilder builder = new JsonpRequestBuilder();
			builder.setPredeterminedId(id);
			builder.requestObject(COUCH_DB_DOCUMENT_METADATA_URL + id,
					new IMetaDataWrapper(callback));
		}
	}

	private boolean isCached(String id)
	{
		return _docCache.contains(id);
	}

	private Document cached(String id)
	{
		return _docCache.getCachedDoc(id);
	}

	private static class CallbackWrapper<WRAPPER, JSO extends WRAPPER> implements
			AsyncCallback<JSO>
	{

		/**
		 * where we send the results to
		 * 
		 */
		private final AsyncCallback<WRAPPER> _myCallback;

		/**
		 * constructor
		 * 
		 * @param callback
		 *          where the results get sent back to
		 */
		public CallbackWrapper(final AsyncCallback<WRAPPER> callback)
		{
			_myCallback = callback;
		}

		@Override
		public void onFailure(final Throwable caught)
		{
			_myCallback.onFailure(caught);
		}

		@Override
		public void onSuccess(final JSO result)
		{
			_myCallback.onSuccess(result);
		}

	}

	/**
	 * this class is used to bridge the gap between the IDocument that is handled
	 * around the rest of the system, and the CouchDbDocument, which is the
	 * concrete implementation within the model package
	 * 
	 * @author ian
	 * 
	 */
	private static class IDocumentWrapper extends
			CallbackWrapper<Document, CouchDbDocument>
	{

		public IDocumentWrapper(AsyncCallback<Document> callback)
		{
			super(callback);
		}
	}

	private static class IMetaDataWrapper extends
			CallbackWrapper<Metadata, CouchDbMetadata>
	{

		public IMetaDataWrapper(AsyncCallback<Metadata> callback)
		{
			super(callback);
		}
	}

	/**
	 * Method to update metadata of document as per passed 'newDataSet'
	 */

	@Override
	public void updateDocument(Document document,
			HashMap<Facet, String> newDataSet,
			final AsyncCallback<Document> asyncCallback)
	{
		try
		{
			String id = document.getId();

			// get metadata to modify
			CouchDbMetadata metadata = (CouchDbMetadata) document.getMetadata();

			for (Facet facet : newDataSet.keySet())
			{
				// pushing changes to metadata
				new JSONObject(metadata).put(facet.toFacetNameString(), new JSONString(
						newDataSet.get(facet)));
			}

			final JSONObject doc = new JSONObject((CouchDbDocument) document);

			// setting the updated metadata to document
			doc.put("metadata", new JSONObject(metadata));

			String url = COUCH_DB_DOCUMENT_URL + id;

			// JsonpRequestBuilder can fire GET requests only, hence using
			// RequestBuilder
			RequestBuilder builder = new RequestBuilder(RequestBuilder.PUT, url);

			// Pass modified document
			builder.setRequestData(doc.toString());

			builder.setCallback(new RequestCallback()
			{

				@Override
				public void onResponseReceived(Request request, Response response)
				{
					if (response.getStatusCode() == Response.SC_CREATED)
					{
						// Update the document with new revision
						doc.put("_rev",JSONParser.parseStrict(response.getText()).isObject().get("rev").isString());

						// pass on the good news
						asyncCallback.onSuccess((Document) doc.getJavaScriptObject());
					}
				}

				@Override
				public void onError(Request request, Throwable exception)
				{
					asyncCallback.onFailure(exception);
				}
			});

			// FIRE
			builder.send();
		}
		catch (RequestException e)
		{
			Window.alert(e.getMessage());
			e.printStackTrace();
		}

	}

}
