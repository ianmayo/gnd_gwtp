package org.pml.gnd.gwt.client.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.pml.gnd.gwt.client.event.ItemRemovedFromCartEvent;
import org.pml.gnd.gwt.client.event.ItemsAddedToCartEvent;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ShoppingCart {

	private final Set<ResultTableElement> items = Sets.newHashSet();
	private final EventBus eventBus;

	@Inject
	public ShoppingCart(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public void addAll(Collection<ResultTableElement> items) {
		checkNotNull(items);
		this.items.addAll(items);
		ItemsAddedToCartEvent.fire(eventBus);
	}
	
	public void add(ResultTableElement item) {
		checkNotNull(item);
		this.items.add(item);
		ItemsAddedToCartEvent.fire(eventBus);
	}

	public void remove(@Nullable ResultTableElement item) {
		if (item == null)
			return;
		items.remove(item);
		ItemRemovedFromCartEvent.fire(eventBus);
	}

	public void clear() {
		items.clear();
		ItemRemovedFromCartEvent.fire(eventBus);
	}

	public List<ResultTableElement> listItems() {
		return ImmutableList.copyOf(items);
	}

	public int countItems() {
		return items.size();
	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

}
