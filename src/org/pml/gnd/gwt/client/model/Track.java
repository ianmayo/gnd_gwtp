package org.pml.gnd.gwt.client.model;

public interface Track extends Document {

	public static final String LATITUDE = "lat";

	public static final String LONGITUDE = "lon";

}
