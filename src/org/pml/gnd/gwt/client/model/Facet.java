package org.pml.gnd.gwt.client.model;

import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;

public enum Facet implements ElasticFacet {

	NAME,
	PLATFORM,
	PLATFORM_TYPE,
	SENSOR,
	SENSOR_TYPE,
	TRIAL,
	DATA_TYPE,
	TYPE,
	START,
	END;

	public static final Facet[] SEARCH_PARAMS = { PLATFORM, PLATFORM_TYPE, SENSOR, SENSOR_TYPE, TRIAL, DATA_TYPE };

	public static final Facet[] RESULTS_FIELDS = {	NAME,
													PLATFORM,
													PLATFORM_TYPE,
													SENSOR,
													SENSOR_TYPE,
													TRIAL,
													TYPE,
													START,
													END };

	@Override
	public String toRequestString() {
		switch (this) {
		case NAME:
			return "metadata.name";
		case PLATFORM:
			return "metadata.platform";
		case PLATFORM_TYPE:
			return "metadata.platform_type";
		case SENSOR:
			return "metadata.sensor";
		case SENSOR_TYPE:
			return "metadata.sensor_type";
		case TRIAL:
			return "metadata.trial";
		case DATA_TYPE:
			return "metadata.data_type";
		case TYPE:
			return "metadata.type";
		case START:
			return "metadata.time_bounds.start";
		case END:
			return "metadata.time_bounds.end";
		default:
			throw new UnsupportedOperationException("toRequestString() method is not supported for facet " + name());
		}
	}
	
	public String toFacetNameString() {
		switch (this) {
		case NAME:
			return "name";
		case PLATFORM:
			return "platform";
		case PLATFORM_TYPE:
			return "platform_type";
		case SENSOR:
			return "sensor";
		case SENSOR_TYPE:
			return "sensor_type";
		case TRIAL:
			return "trial";
		case DATA_TYPE:
			return "data_type";
		case TYPE:
			return "type";
		default:
			throw new UnsupportedOperationException("toFacetNameString() method is not supported for facet " + name());
		}
	}
	
	

	@Override
	public String toString() {
		return toRequestString();
	}

	public String toDisplayString() {
		switch (this) {
		case NAME:
			return "Name";
		case PLATFORM:
			return "Platform";
		case PLATFORM_TYPE:
			return "Platform Type";
		case SENSOR:
			return "Sensor";
		case SENSOR_TYPE:
			return "Sensor Type";
		case TRIAL:
			return "Trial";
		case DATA_TYPE:
			return "Data Type";
		default:
			throw new UnsupportedOperationException("toDisplayString() method is not supported for Facet." + name());
		}
	}

}
