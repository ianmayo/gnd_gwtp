package org.pml.gnd.gwt.client.model;

public interface GeoBounds {

	int getTotalTl();

	String getTl(int index);

	int getTotalBr();

	String getBr(int index);

}
