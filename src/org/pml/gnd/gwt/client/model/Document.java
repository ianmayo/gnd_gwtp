/**
 * 
 */
package org.pml.gnd.gwt.client.model;

import java.util.Date;
import java.util.Iterator;

import com.google.gwt.json.client.JSONArray;

/**
 * @author othman
 * 
 */
public interface Document {

	/**
	 * the name given to time data (which may get special handling)
	 * 
	 */
	static final String TIME = "time";
	static final String LOCATION = "location";

	String getId();
	
	JSONArray getMeasurement(String measurement);

	Metadata getMetadata();

	Iterator<DataEntry> timeIterator();

	Iterator<DataEntry> dataIterator();

	Iterator<DataEntry> trackDataIterator();

	Iterator<DataEntry> narrativeDataIterator();

	Date getLastTimeMeasurement();

	Location getLocation();

}
