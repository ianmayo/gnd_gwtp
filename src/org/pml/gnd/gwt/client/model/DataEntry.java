package org.pml.gnd.gwt.client.model;

import java.util.Date;

public interface DataEntry {

	String getMeasurementAsString(String dataType);

	Double getMeasurementAsDouble(String dataType);
	
	boolean containsKey(String dataType);

	Date getTime();

}
