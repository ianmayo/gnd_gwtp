package org.pml.gnd.gwt.client.model;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONArray;

/**
 * 
 * @author Akash
 * 
 */
public interface Location
{

	/**
	 * An ID composed of location fields
	 */
	String getType();

	String getCoordinates();

	int getNumberOfCoordinates();

	double getLatitude(int index);

	double getLongitude(int index);
}
