/**
 * 
 */
package org.pml.gnd.gwt.client.model;

/**
 * @author akash
 * 
 */
public interface CouchView {

	String getId();

	String getValue();
}
