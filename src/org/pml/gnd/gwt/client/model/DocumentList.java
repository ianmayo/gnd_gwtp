package org.pml.gnd.gwt.client.model;

import java.util.Iterator;

// TODO Investigate using JsArray<Document>
public interface DocumentList {

	int getNumItems();

	Document getHit(int i);

	Iterator<Document> iterator();

}
