package org.pml.gnd.gwt.client.model;

import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Munawar
 * 
 */
public interface Metadata {

	/**
	 * An ID composed of Metadata fields
	 */
	String getId();

	int getDatatypeCount();

	String getDatatype(int index);

	List<String> listDataTypes();

	Iterator<String> dataTypes();

	String getPlatform();

	String getName();

	String getPlatformType();

	String getTrial();

	String getSensor();

	String getSensorType();

	String getType();

	GeoBounds getGeoBounds();

	String getTimeBoundsStart();

	String getTimeBoundsEnd();

	boolean isTrack();

	boolean isNarrative();

}
