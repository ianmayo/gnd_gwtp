package org.pml.gnd.gwt.client.model;

public interface Selectable {

	void setSelected(boolean purchased);

	boolean isSelected();
}
