package org.pml.gnd.gwt.client.model;

import org.pml.gnd.gwt.client.event.CurrentDocumentChangeEvent;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * 
 * Holds a reference to the {@link Document} currently viewed in the {@link BrowseInfoPresenter}
 * 
 * @author Mikael Couzic
 * 
 */
@Singleton
public class CurrentDocument {

	private final EventBus eventBus;

	private String id;
	private Metadata metadata;
	private Document document;

	@Inject
	public CurrentDocument(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public String getId() {
		if (id == null)
			throw new IllegalStateException("ID not set");
		return id;
	}

	public Metadata getMetadata() {
		if (metadata == null)
			throw new IllegalStateException("Metadata not set");
		return metadata;
	}

	public Document get() {
		if (document == null)
			throw new IllegalStateException("Document not set");
		return document;
	}

	public boolean isIdSet() {
		return id != null;
	}

	public boolean isMetadataSet() {
		return metadata != null;
	}

	public boolean isSet() {
		return document != null;
	}

	public void setId(String id) {
		if (id == null)
			throw new NullPointerException();
		if (!id.equals(this.id)) {
			this.metadata = null;
			this.document = null;
			this.id = id;
			CurrentDocumentChangeEvent.fire(eventBus, id);
		}
	}

	/**
	 * An alternative method to set the current document. This is useful for Presenters that only display the document's
	 * metadata, in those cases loading the complete document is a waste. Calling this method will fire a
	 * {@link CurrentDocumentChangeEvent}
	 * 
	 * @param metadata
	 *            The metadata of the new current document
	 * @param id
	 *            The document ID
	 */
	public void setMetadata(Metadata metadata, String id) {
		if (metadata == null || id == null)
			throw new NullPointerException();
		this.id = id;
		this.metadata = metadata;
		this.document = null;
		CurrentDocumentChangeEvent.fire(eventBus, id);
	}

	/**
	 * Calling this method will fire a {@link CurrentDocumentChangeEvent}
	 * 
	 * @param document
	 *            The new current document
	 * @param id
	 *            The document ID
	 */
	public void set(Document document, String id) {
		if (document == null || id == null)
			throw new NullPointerException();
		this.id = id;
		this.metadata = document.getMetadata();
		this.document = document;
		CurrentDocumentChangeEvent.fire(eventBus, id);
	}

}
