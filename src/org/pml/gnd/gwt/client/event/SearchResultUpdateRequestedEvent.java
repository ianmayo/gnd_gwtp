package org.pml.gnd.gwt.client.event;

import static com.google.common.base.Preconditions.checkNotNull;

import org.bitbucket.es4gwt.client.result.SearchResultDiff;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class SearchResultUpdateRequestedEvent extends GwtEvent<SearchResultUpdateRequestedEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	public interface Handler extends EventHandler {

		void onSearchResultUpdateRequested(SearchResultUpdateRequestedEvent event);
	}

	private final SearchRequest request;
	private final SearchResultDiff resultDiff;

	public SearchResultUpdateRequestedEvent(SearchRequest request, SearchResultDiff resultDiff) {
		checkNotNull(request);
		checkNotNull(resultDiff);
		this.request = request;
		this.resultDiff = resultDiff;
	}

	public SearchRequest getRequest() {
		return request;
	}

	public SearchResultDiff getResultDiff() {
		return resultDiff;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onSearchResultUpdateRequested(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus source, SearchRequest request, SearchResultDiff resultDiff) {
		source.fireEvent(new SearchResultUpdateRequestedEvent(request, resultDiff));
	}
}
