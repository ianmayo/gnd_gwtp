package org.pml.gnd.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

public class DocumentPresenterRevealedEvent extends GwtEvent<DocumentPresenterRevealedEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();
	private final String nameToken;

	public interface Handler extends EventHandler {

		void onDocumentPresenterRevealed(DocumentPresenterRevealedEvent event);
	}

	public DocumentPresenterRevealedEvent(String nameToken) {
		this.nameToken = nameToken;
	}

	public String getNameToken() {
		return nameToken;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onDocumentPresenterRevealed(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source, String nameToken) {
		source.fireEvent(new DocumentPresenterRevealedEvent(nameToken));
	}
}
