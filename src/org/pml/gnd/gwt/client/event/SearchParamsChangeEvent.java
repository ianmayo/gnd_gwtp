package org.pml.gnd.gwt.client.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SearchParamsChangeEvent extends GwtEvent<SearchParamsChangeEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	public interface Handler extends EventHandler {

		void onSearchParamsChange(SearchParamsChangeEvent event);
	}

	public interface HasSearchParamsChangeHandlers extends HasHandlers {

		HandlerRegistration addSearchParamsChangeHandler(Handler handler);
	}

	public SearchParamsChangeEvent() {
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onSearchParamsChange(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus source) {
		source.fireEvent(new SearchParamsChangeEvent());
	}
}
