package org.pml.gnd.gwt.client.event;

import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

/**
 * 
 * Event fired by the {@link CurrentDocument} when it is reset
 * 
 * @author Mikael Couzic
 * 
 */
public class CurrentDocumentChangeEvent extends GwtEvent<CurrentDocumentChangeEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	private final String documentId;

	public interface Handler extends EventHandler {

		void onCurrentDocumentChange(CurrentDocumentChangeEvent event);

	}

	public interface HasEventHandlers extends HasHandlers {

		HandlerRegistration addCurrentDocumentChangeHandler(Handler handler);
	}

	public CurrentDocumentChangeEvent(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onCurrentDocumentChange(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	/**
	 * @param source
	 *            The {@link EventBus} the event will be fired on
	 * @param document
	 *            The {@link Document} newly set as the {@link CurrentDocument}
	 */
	public static void fire(EventBus source, String documentId) {
		source.fireEvent(new CurrentDocumentChangeEvent(documentId));
	}

}
