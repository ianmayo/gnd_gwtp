package org.pml.gnd.gwt.client.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class ItemRemovedFromCartEvent extends GwtEvent<ItemRemovedFromCartEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	public interface Handler extends EventHandler {

		void onItemRemovedFromCart(ItemRemovedFromCartEvent event);
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onItemRemovedFromCart(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus source) {
		source.fireEvent(new ItemRemovedFromCartEvent());
	}
}
