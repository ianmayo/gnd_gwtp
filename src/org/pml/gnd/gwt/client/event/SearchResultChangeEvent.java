package org.pml.gnd.gwt.client.event;

import static com.google.common.base.Preconditions.checkNotNull;

import org.bitbucket.es4gwt.client.result.SearchResult;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class SearchResultChangeEvent extends GwtEvent<SearchResultChangeEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();
	private final SearchRequest searchRequest;
	private final SearchResult searchResult;

	public interface Handler extends EventHandler {

		void onSearchResultChange(SearchResultChangeEvent event);
	}

	public SearchResultChangeEvent(SearchRequest searchRequest, SearchResult searchResult) {
		checkNotNull(searchRequest);
		checkNotNull(searchResult);
		this.searchRequest = searchRequest;
		this.searchResult = searchResult;
	}

	public SearchRequest getSearchRequest() {
		return searchRequest;
	}

	public SearchResult getSearchResult() {
		return searchResult;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onSearchResultChange(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus eventBus, SearchRequest searchRequest, SearchResult searchResult) {
		eventBus.fireEvent(new SearchResultChangeEvent(searchRequest, searchResult));
	}
}
