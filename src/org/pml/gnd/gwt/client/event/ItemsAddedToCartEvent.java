package org.pml.gnd.gwt.client.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class ItemsAddedToCartEvent extends GwtEvent<ItemsAddedToCartEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	public interface Handler extends EventHandler {

		void onItemsAddedToCart(ItemsAddedToCartEvent event);
	}

	public ItemsAddedToCartEvent() {
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onItemsAddedToCart(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus source) {
		source.fireEvent(new ItemsAddedToCartEvent());
	}
}
