package org.pml.gnd.gwt.client.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SearchHitSelectionEvent extends GwtEvent<SearchHitSelectionEvent.Handler> {

	public static Type<Handler> TYPE = new Type<Handler>();

	private final Boolean selected;

	public interface Handler extends EventHandler {

		void onSearchHitSelection(SearchHitSelectionEvent event);
	}

	public interface HasSearchHitSelectionHandlers extends HasHandlers {

		HandlerRegistration addSearchHitSelectionHandler(Handler handler);
	}

	public SearchHitSelectionEvent(Boolean selected) {
		this.selected = selected;
	}

	public Boolean isSelected() {
		return selected;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onSearchHitSelection(this);
	}

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	public static Type<Handler> getType() {
		return TYPE;
	}

	public static void fire(EventBus source, Boolean selected) {
		source.fireEvent(new SearchHitSelectionEvent(selected));
	}
}
