package org.pml.gnd.gwt.client.util;

import com.google.gwt.user.client.ui.ListBox;

public class GWTUtils
{

	public static void setSelectedText(ListBox listBox, String val, boolean ignoreCase)
	{
		if (val == null)
		{
			return;
		}

		int lItemCount = listBox.getItemCount();

		for (int i = 0; i < lItemCount; i++)
		{
			if (ignoreCase)
			{
				if (val.equalsIgnoreCase(listBox.getItemText(i)))
				{
					listBox.setSelectedIndex(i);
				}
			}
			else
			{
				if (val.equals(listBox.getItemText(i)))
				{
					listBox.setSelectedIndex(i);
				}
			}
		}
	}
}
