package org.pml.gnd.gwt.client.util.date;

import com.google.gwt.i18n.shared.DateTimeFormat;

class CustomFormat extends BaseFormat {

	private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	@Override
	DateTimeFormat getFormat() {
		return FORMAT;
	}

}
