package org.pml.gnd.gwt.client.util.date;

import com.google.gwt.i18n.shared.DateTimeFormat;

class ShortFormat extends BaseFormat {

	private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat("yyyy-MM-dd");

	@Override
	DateTimeFormat getFormat() {
		return FORMAT;
	}
}
