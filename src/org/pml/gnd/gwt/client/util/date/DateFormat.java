package org.pml.gnd.gwt.client.util.date;

import java.util.Date;

public interface DateFormat {

	Date parse(String text);

	/**
	 * Format date in GMT time zone. Use this method when formatting dates coming from external sources (server
	 * responses for example)
	 */
	String format(Date date);

	/**
	 * Format date in local time zone. Use this method when formatting dates created locally
	 */
	String formatLocal(Date date);

}
