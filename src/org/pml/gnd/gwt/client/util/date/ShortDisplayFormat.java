package org.pml.gnd.gwt.client.util.date;

import com.google.gwt.i18n.shared.DateTimeFormat;

public class ShortDisplayFormat extends BaseFormat {

	private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat("d/M/yyyy");

	@Override
	DateTimeFormat getFormat() {
		return FORMAT;
	}

}
