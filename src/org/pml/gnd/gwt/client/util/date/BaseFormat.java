package org.pml.gnd.gwt.client.util.date;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.i18n.shared.DateTimeFormat;

abstract class BaseFormat implements DateFormat {

	private static final TimeZone GMT = TimeZone.createTimeZone(0);

	@Override
	public Date parse(String text) {
		checkNotNull(text);
		return getFormat().parse(text);
	}

	@Override
	public String format(Date date) {
		checkNotNull(date);
		return getFormat().format(date, GMT);
	}

	@Override
	public String formatLocal(Date date) {
		checkNotNull(date);
		return getFormat().format(date);
	}

	abstract DateTimeFormat getFormat();
}
