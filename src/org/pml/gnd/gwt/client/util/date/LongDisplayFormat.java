package org.pml.gnd.gwt.client.util.date;

import com.google.gwt.i18n.shared.DateTimeFormat;

public class LongDisplayFormat extends BaseFormat {

	private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");

	@Override
	DateTimeFormat getFormat() {
		return FORMAT;
	}

}
