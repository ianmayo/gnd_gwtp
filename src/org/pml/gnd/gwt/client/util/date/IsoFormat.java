package org.pml.gnd.gwt.client.util.date;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;

class IsoFormat extends BaseFormat {

	private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat(PredefinedFormat.ISO_8601);

	@Override
	DateTimeFormat getFormat() {
		return FORMAT;
	}

}
