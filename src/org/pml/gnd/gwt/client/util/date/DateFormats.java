package org.pml.gnd.gwt.client.util.date;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

public class DateFormats {

	/**
	 * ISO-8601 Format - For example : 2012-02-06T15:56:56.565Z or 2012-02-06T15:56:56.565+00:00
	 */
	public static final DateFormat ISO = new IsoFormat();

	/**
	 * yyyy-MM-dd'T'HH:mm:ssZ - For example : 2012-07-06T11:05:50Z or 2012-07-06T11:05:50+00:00
	 */
	public static final DateFormat CUSTOM = new CustomFormat();

	/**
	 * dd/MM/yyyy HH:mm
	 */
	public static final DateFormat DISPLAY_LONG = new LongDisplayFormat();

	/**
	 * d/M/yyyy
	 */
	public static final DateFormat DISPLAY_SHORT = new ShortDisplayFormat();

	/**
	 * yyyy-MM-dd
	 */
	public static final DateFormat SHORT = new ShortFormat();

	public static String convert(String date, DateFormat from, DateFormat to) {
		checkNotNull(date);
		checkNotNull(from);
		checkNotNull(to);
		Date parsed = from.parse(date);
		return to.format(parsed);
	}

}
