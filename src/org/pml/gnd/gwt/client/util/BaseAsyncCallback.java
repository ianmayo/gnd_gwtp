/**
 * 
 */
package org.pml.gnd.gwt.client.util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * BaseAsyncCallback is a common callback for all RPC calls. Also it defines a
 * behavior on failure event.
 * 
 * @author Munawar Saeed
 */
public abstract class BaseAsyncCallback<T> implements AsyncCallback<T> {
	@Override
	public void onFailure(Throwable caught) {
		DOM.setStyleAttribute(RootPanel.get().getElement(), "cursor", "default");
		Window.alert("Communication Failure: " + caught.getMessage());
		caught.printStackTrace();
		GWT.log(caught.getMessage(), caught);
	}
}
