package org.pml.gnd.gwt.client.util;

public class StringUtils
{
	public static boolean isNonBlank(String val)
	{
		return !(((val == null) || (val.trim().length() < 1) || val.equals("null")));
	}

	public static boolean isBlankOrNull(String val)
	{
		return ((val == null) || (val.trim().length() < 1) || val.equals("null"));
	}

	public static boolean isNumeric(String number)
	{
		if ((number == null) || (number.trim().length() == 0))
		{
			return true;
		}

		try
		{
			Integer.valueOf(number);
			return true;
		}
		catch (IllegalArgumentException ex)
		{
			return false;
		}
	}

}
