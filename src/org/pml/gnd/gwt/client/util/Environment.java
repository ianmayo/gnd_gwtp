package org.pml.gnd.gwt.client.util;

import com.google.gwt.user.client.Window;

public class Environment {

	private static final boolean devmode = Window.Location.getHost().equals("127.0.0.1:8888");

	public static boolean isDevMode() {
		return devmode;
	}
}
