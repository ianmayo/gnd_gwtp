package org.pml.gnd.gwt.client.ui.document;

import static org.pml.gnd.gwt.client.model.Facet.DATA_TYPE;
import static org.pml.gnd.gwt.client.model.Facet.NAME;
import static org.pml.gnd.gwt.client.model.Facet.PLATFORM;
import static org.pml.gnd.gwt.client.model.Facet.PLATFORM_TYPE;
import static org.pml.gnd.gwt.client.model.Facet.SENSOR;
import static org.pml.gnd.gwt.client.model.Facet.SENSOR_TYPE;
import static org.pml.gnd.gwt.client.model.Facet.TRIAL;

import java.util.HashMap;
import java.util.Iterator;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.handlers.InfoHandlers;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.model.Metadata;
import org.pml.gnd.gwt.client.place.DocumentIdSetInUrlGatekeeper;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;

public class InfoPresenter extends
		DocumentBasePresenter<InfoPresenter.MyView, InfoPresenter.MyProxy>
		implements InfoHandlers
{

	@ProxyCodeSplit
	@NameToken(NameTokens.info)
	@UseGatekeeper(DocumentIdSetInUrlGatekeeper.class)
	public interface MyProxy extends ProxyPlace<InfoPresenter>
	{
	}

	@Override
	protected void revealInParent()
	{
		RevealContentEvent.fire(this, DocumentMainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends DocumentBasePresenter.ItsView,
			HasUiHandlers<InfoHandlers>
	{

		void setTable(Metadata doc);

		void setPlatformLink(String platform, String target);

		void setName(String platform, String target);

		void setPlatformTypeLink(String platformType, String target);

		void setSensorLink(String sensor, String target);

		void setSensorTypeLink(String sensorType, String target);

		void setTrialLink(String trial, String target);

		void addDataTypeLink(String dataType, String target, boolean withComma);
	}

	@Inject
	public InfoPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			CurrentDocument document, PlaceManager placeManager, DataStore dataStore)
	{
		super(eventBus, view, proxy, document, placeManager, dataStore);
		getView().setUiHandlers(this);
	}

	@Override
	protected void displayDocumentData()
	{
		Metadata metadata = document.getMetadata();
		getView().setTable(metadata);

		String name = metadata.getName();
		getView().setName(name, target(NAME, name));

		String platform = metadata.getPlatform();
		getView().setPlatformLink(platform, target(PLATFORM, platform));

		String platformType = metadata.getPlatformType();
		getView().setPlatformTypeLink(platformType,
				target(PLATFORM_TYPE, platformType));

		String sensor = metadata.getSensor();
		getView().setSensorLink(sensor, target(SENSOR, sensor));

		String sensorType = metadata.getSensorType();
		getView().setSensorTypeLink(sensorType, target(SENSOR_TYPE, sensorType));

		String trial = metadata.getTrial();
		getView().setTrialLink(trial, target(TRIAL, trial));

		Iterator<String> i = metadata.dataTypes();
		while (i.hasNext())
		{
			String dataType = i.next();
			getView().addDataTypeLink(dataType, target(DATA_TYPE, dataType),
					i.hasNext());
		}
	}

	private String target(Facet facet, String value)
	{
		return placeManager.buildHistoryToken(new PlaceRequest(NameTokens.search)
				.with(facet.name(), value));
	}

	@Override
	public void submitDocument(HashMap<Facet, String> newDataSet)
	{
		dataStore.updateDocument(document.get(), newDataSet,
				new AsyncCallback<Document>()
				{

					@Override
					public void onFailure(Throwable caught)
					{
						Window.alert("Save failed - " + caught.getMessage());
					}

					@Override
					public void onSuccess(Document result)
					{
						document.set(result, result.getId());
						onReset();
					}
				});
	}

}
