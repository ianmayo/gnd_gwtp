package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.ui.map.OpenStreetMap;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class ChartView extends ViewImpl implements ChartPresenter.MyView {

	public interface Binder extends UiBinder<Widget, ChartView> {
	}

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiField Panel mapPanel;

	private final OpenStreetMap map;

	@Inject
	public ChartView(final Binder binder) {
		widget = binder.createAndBindUi(this);

		// Init Map
		map = new OpenStreetMap();
		mapPanel.add(map);

	}

	@Override
	public void clear() {
		map.clear();
	}

	@UiHandler("recenterButton")
	void onRecenterButtonClick(ClickEvent event) {
		map.recenter();
	}

	@UiHandler("measureModeCheckbox")
	void onMeasureModeCheckboxValueChange(ValueChangeEvent<Boolean> event) {
		if (event.getValue())
			map.activateMeasureControl();
		else
			map.deactivateMeasureControl();
	}

	@Override
	public void drawTrackOnMap(Document document) {
		map.drawTrack(document, RGBColor.BLACK);
	}

}
