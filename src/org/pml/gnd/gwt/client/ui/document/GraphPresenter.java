package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.handlers.GraphHandlers;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.place.DocumentIdSetInUrlGatekeeper;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.ui.search.PermalinkDialogPresenter;
import org.pml.gnd.gwt.client.util.StringUtils;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealRootPopupContentEvent;

public class GraphPresenter extends
		DocumentBasePresenter<GraphPresenter.MyView, GraphPresenter.MyProxy>
		implements GraphHandlers
{
	
	public static final String LEFT = "left";
	public static final String RIGHT = "right";
	private final Provider<PermalinkDialogPresenter> permalinkDialog;

	@ProxyCodeSplit
	@NameToken(NameTokens.graph)
	@UseGatekeeper(DocumentIdSetInUrlGatekeeper.class)
	public interface MyProxy extends ProxyPlace<GraphPresenter>
	{
	}

	@Override
	protected void revealInParent()
	{
		RevealContentEvent.fire(this, DocumentMainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends DocumentBasePresenter.ItsView,
			HasUiHandlers<GraphHandlers>
	{

		/**
		 * show this set of data on the LEFT axis
		 * 
		 * @param timeArray
		 *          the time values
		 * @param measurementArray
		 *          the measured data
		 * @param thisMeasurementName
		 *          what it's called
		 */
		void showThisMeasurementAsLeftYAxis(JSONArray timeArray,
				JSONArray measurementArray, String thisMeasurementName);

		/**
		 * show this set of data on the RIGHT axis
		 * 
		 * @param timeArray
		 *          the time values
		 * @param measurementArray
		 *          the measured data
		 * @param thisMeasurementName
		 *          what it's called
		 */
		void showThisMeasurementAsRightYAxis(JSONArray timeArray,
				JSONArray measurementArray, String thisMeasurementName);

		void addItemToLeftAxisBox(String item);

		void addItemToRightAxisBox(String item);

		/**
		 * Clear the data associated with the left Y axis
		 */
		void clearLeftData();

		/**
		 * Clear the data associated with the right Y axis
		 */
		void clearRightData();

		/**
		 * Called whenever the Presenter is revealed
		 */
		void onDisplay();

		void setLeftAxisBox(String left);

		void setRightAxisBox(String right);
	}

	@Inject
	public GraphPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			CurrentDocument document, PlaceManager placeManager, DataStore dataStore,Provider<PermalinkDialogPresenter> searchPermalinkDialogPresenter)
	{
		super(eventBus, view, proxy, document, placeManager, dataStore);
		getView().setUiHandlers(this);
		this.permalinkDialog = searchPermalinkDialogPresenter;
	}

	@Override
	protected void onReveal()
	{
		super.onReveal();
		getView().onDisplay();
	}

	@Override
	protected void update()
	{
		super.update();
		String left = placeManager.getCurrentPlaceRequest().getParameter(LEFT,
				null);
		String right = placeManager.getCurrentPlaceRequest().getParameter(RIGHT,
				null);

		if (StringUtils.isBlankOrNull(left))
		{
			getView().clearLeftData();
		}
		else
		{
			getView().setLeftAxisBox(left);
			JSONArray time = document.get().getMeasurement(Document.TIME);
			JSONArray measurement = document.get().getMeasurement(left);
			getView().showThisMeasurementAsLeftYAxis(time, measurement, left);
		}

		if (StringUtils.isBlankOrNull(right))
		{
			getView().clearRightData();
		}
		else
		{
			getView().setRightAxisBox(right);
			JSONArray time = document.get().getMeasurement(Document.TIME);
			JSONArray measurement = document.get().getMeasurement(right);
			getView().showThisMeasurementAsRightYAxis(time, measurement, right);
		}

	}

	@Override
	protected void displayDocumentData()
	{
		initAxisBoxes(document.get());
	}

	private void initAxisBoxes(Document result)
	{
		// special handling. do we have time?
		final JSONArray timeCol = result.getMeasurement(Document.TIME);
		if ((timeCol == null) || (timeCol.size() == 0))
		{
			throw new RuntimeException(
					"This document has no time facet => impossible to display");
		}
		else
		{
			int numCols = result.getMetadata().getDatatypeCount();
			getView().addItemToLeftAxisBox("");
			getView().addItemToRightAxisBox("");
			for (int i = 0; i < numCols; i++)
			{
				String thisName = result.getMetadata().getDatatype(i);
				if (!thisName.equals(Document.TIME) && !thisName.equals(Document.LOCATION))
				{
					getView().addItemToLeftAxisBox(thisName);
					getView().addItemToRightAxisBox(thisName);
				}
			}
		}
	}

	@Override
	public void onLeftAxisBoxChange(String value)
	{
		PlaceRequest leftRequest = placeManager.getCurrentPlaceRequest();
		leftRequest = leftRequest.with(LEFT, value);
		placeManager.revealPlace(leftRequest);
	}

	@Override
	public void onRightAxisBoxChange(String value)
	{
		PlaceRequest rightRequest = placeManager.getCurrentPlaceRequest();
		rightRequest = rightRequest.with(RIGHT, value);
		placeManager.revealPlace(rightRequest);
	}

	@Override
	public void onPermalinkClick()
	{
		String url = Window.Location.getHref();
		PermalinkDialogPresenter dialog = permalinkDialog.get();
		dialog.showPermalink(url);
		RevealRootPopupContentEvent.fire(this, dialog);
	}
}
