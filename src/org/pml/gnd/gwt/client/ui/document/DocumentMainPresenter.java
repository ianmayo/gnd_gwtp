package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.event.DocumentPresenterRevealedEvent;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.ui.main.MainPresenter;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;

public class DocumentMainPresenter extends Presenter<DocumentMainPresenter.MyView, DocumentMainPresenter.MyProxy> {

	@ProxyCodeSplit
	public interface MyProxy extends Proxy<DocumentMainPresenter> {
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends View {

		void updateTargets(String documentId);

		void activateTab(String nameToken);

	}

	@ContentSlot public static final Type<RevealContentHandler<?>> CONTENT_SLOT = new Type<RevealContentHandler<?>>();

	private final CurrentDocument document;

	@Inject
	public DocumentMainPresenter(EventBus eventBus, MyView view, MyProxy proxy, CurrentDocument document) {
		super(eventBus, view, proxy);
		this.document = document;
	}

	@Override
	protected void onBind() {
		super.onBind();
		addRegisteredHandler(DocumentPresenterRevealedEvent.TYPE, new DocumentPresenterRevealedEvent.Handler() {

			@Override
			public void onDocumentPresenterRevealed(DocumentPresenterRevealedEvent event) {
				getView().updateTargets(document.getId());
				getView().activateTab(event.getNameToken());
			}
		});
	}
}
