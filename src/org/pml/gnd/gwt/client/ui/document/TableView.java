package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class TableView extends ViewImpl implements TablePresenter.MyView {

	private final Widget widget;

	public interface Binder extends UiBinder<Widget, TableView> {
	}

	@UiField FlexTable table;

	@Inject
	public TableView(final Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	@Override
	public Widget asWidget() {
		return widget;
	}

	@Override
	public void showThisMeasurement(JSONArray array, String thisMeasurementName) {
		int colNum = 0;

		// do we have any rows yet?
		if (table.getRowCount() > 0) {
			colNum = table.getCellCount(0);
		}

		// put in the header
		table.setText(0, colNum, thisMeasurementName);

		// now display the data
		for (int i = 0; i < array.size(); i++) {
			// TODO Implement a more robust solution
			if (thisMeasurementName.equals(Document.TIME))
				table.setText(i + 1, colNum, DateFormats.convert(	array.get(i).isString().stringValue(),
																	DateFormats.CUSTOM,
																	DateFormats.DISPLAY_LONG));
			else
				table.setText(i + 1, colNum, array.get(i).toString());
		}
	}

	@Override
	public void clear() {
		table.removeAllRows();
	}
}
