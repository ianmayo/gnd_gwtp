package org.pml.gnd.gwt.client.ui.document;

import static org.pml.gnd.gwt.client.model.Facet.NAME;
import static org.pml.gnd.gwt.client.model.Facet.PLATFORM;
import static org.pml.gnd.gwt.client.model.Facet.PLATFORM_TYPE;
import static org.pml.gnd.gwt.client.model.Facet.SENSOR;
import static org.pml.gnd.gwt.client.model.Facet.SENSOR_TYPE;
import static org.pml.gnd.gwt.client.model.Facet.TRIAL;

import java.util.HashMap;

import org.pml.gnd.gwt.client.handlers.InfoHandlers;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.model.Metadata;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class InfoView extends ViewWithUiHandlers<InfoHandlers> implements
		InfoPresenter.MyView
{

	public interface Binder extends UiBinder<Widget, InfoView>
	{
	}

	private final Widget widget;

	@Override
	public Widget asWidget()
	{
		return widget;
	}

	private final static int PLATFORM_ROW = 0;
	private static final int NAME_ROW = 1;
	private static final int VERSION_ROW = 2;
	private static final int START_ROW = 3;
	private static final int END_ROW = 4;
	private final static int PLATFORM_TYPE_ROW = 5;
	private final static int SENSOR_ROW = 6;
	private final static int SENSOR_TYPE_ROW = 7;
	private final static int TRIAL_ROW = 8;
	private final static int FIELDS_ROW = 9;

	private HashMap<Facet, Label> map = new HashMap<Facet, Label>();
	private HashMap<Facet, Integer> facetRow = new HashMap<Facet, Integer>();
	private HashMap<Facet, TextBox> editDataMap = new HashMap<Facet, TextBox>();

	@UiField(provided = true)
	FlexTable table = new FlexTable();
	@UiField
	Button edit, submit, cancel;

	Panel dataTypesCell = new FlowPanel();

	@Inject
	public InfoView(final Binder binder)
	{
		widget = binder.createAndBindUi(this);
	}

	@Override
	public void clear()
	{
		dataTypesCell.clear();
		table.clear();
	}

	@Override
	public void setTable(Metadata metadata)
	{
		if (metadata == null)
		{
			throw new NullPointerException();
		}
		table.clear();
		table.setText(VERSION_ROW, 0, "Version");
		table.setText(VERSION_ROW, 1, "version here!");
		table.setText(START_ROW, 0, "Start");
		String data = metadata.getTimeBoundsStart() == null ? ""
				: convertDate(metadata.getTimeBoundsStart());
		table.setText(START_ROW, 1, data);
		table.setText(END_ROW, 0, "End");
		data = metadata.getTimeBoundsEnd() == null ? "" : convertDate(metadata
				.getTimeBoundsEnd());
		table.setText(END_ROW, 1, data);
		table.setText(PLATFORM_ROW, 0, PLATFORM.toDisplayString());
		table.setText(NAME_ROW, 0, NAME.toDisplayString());
		table.setText(PLATFORM_TYPE_ROW, 0, PLATFORM_TYPE.toDisplayString());
		table.setText(SENSOR_ROW, 0, SENSOR.toDisplayString());
		table.setText(SENSOR_TYPE_ROW, 0, SENSOR_TYPE.toDisplayString());
		table.setText(TRIAL_ROW, 0, TRIAL.toDisplayString());
		table.setText(FIELDS_ROW, 0, "Fields");
		dataTypesCell.clear();
		table.setWidget(FIELDS_ROW, 1, dataTypesCell);

		table.getRowFormatter().setStyleName(PLATFORM_ROW, "platform");
	}

	private String convertDate(String date)
	{
		return DateFormats.convert(date, DateFormats.CUSTOM,
				DateFormats.DISPLAY_LONG);
	}

	@Override
	public void setName(String platform, String target)
	{
		setLink(NAME_ROW, platform, target, NAME);
	}

	@Override
	public void setPlatformLink(String platform, String target)
	{
		setLink(PLATFORM_ROW, platform, target, PLATFORM);
	}

	@Override
	public void setPlatformTypeLink(String platformType, String target)
	{
		setLink(PLATFORM_TYPE_ROW, platformType, target, PLATFORM_TYPE);
	}

	@Override
	public void setSensorLink(String sensor, String target)
	{
		setLink(SENSOR_ROW, sensor, target, SENSOR);
	}

	@Override
	public void setSensorTypeLink(String sensorType, String target)
	{
		setLink(SENSOR_TYPE_ROW, sensorType, target, SENSOR_TYPE);
	}

	@Override
	public void setTrialLink(String trial, String target)
	{
		setLink(TRIAL_ROW, trial, target, TRIAL);
	}

	private void setLink(int row, String text, final String target, Facet facet)
	{
		Label label = new Label(LabelType.INFO, text);
		label.getElement().getStyle().setCursor(Cursor.POINTER);
		label.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				History.newItem(target, true);
			}
		});
		table.setWidget(row, 1, label);
		facetRow.put(facet, Integer.valueOf(row));
		map.put(facet, label);
	}

	@Override
	public void addDataTypeLink(String dataType, final String target,
			boolean withComma)
	{
		Label label = new Label(LabelType.INFO, dataType);
		label.getElement().getStyle().setCursor(Cursor.POINTER);
		label.addStyleName("pull-left");
		label.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				History.newItem(target, true);
			}
		});
		dataTypesCell.add(label);
		if (withComma)
		{
			HTML space = new HTML("&nbsp;");
			space.addStyleName("pull-left");
			dataTypesCell.add(space);
		}
	}

	@UiHandler("edit")
	void onEditClick(ClickEvent e)
	{
		switchToEditMode();
	}

	@UiHandler("submit")
	void onSubmitClick(ClickEvent e)
	{
		HashMap<Facet, String> newDataSet = new HashMap<Facet, String>();
		for (Facet facet : editDataMap.keySet())
		{
			newDataSet.put(facet, editDataMap.get(facet).getText());
		}
		getUiHandlers().submitDocument(newDataSet);
		switchToViewMode();
	}

	@UiHandler("cancel")
	void onCancelClick(ClickEvent e)
	{
		for (Facet facet : map.keySet())
		{
			table.setWidget(facetRow.get(facet), 1, map.get(facet));
		}
		switchToViewMode();
	}

	private void switchToEditMode()
	{
		edit.setVisible(false);
		submit.setVisible(true);
		cancel.setVisible(true);

		for (Facet facet : map.keySet())
		{
			Label label = map.get(facet);
			TextBox editBox = new TextBox();
			editBox.setText(label.getText());
			table.setWidget(facetRow.get(facet), 1, editBox);
			editDataMap.put(facet, editBox);
		}
	}

	private void switchToViewMode()
	{
		edit.setVisible(true);
		submit.setVisible(false);
		cancel.setVisible(false);
	}

}
