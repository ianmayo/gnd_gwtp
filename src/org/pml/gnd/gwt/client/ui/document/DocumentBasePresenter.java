package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.event.CurrentDocumentChangeEvent;
import org.pml.gnd.gwt.client.event.DocumentPresenterRevealedEvent;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.ui.document.DocumentBasePresenter.ItsView;
import org.pml.gnd.gwt.client.util.BaseAsyncCallback;

import com.google.gwt.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public abstract class DocumentBasePresenter<V extends ItsView, Proxy_ extends ProxyPlace<?>>
		extends Presenter<V, Proxy_> implements CurrentDocumentChangeEvent.Handler {

	public interface ItsView extends View {

		void clear();

	}

	protected final CurrentDocument document;
	protected final PlaceManager placeManager;
	protected final DataStore dataStore;

	private boolean upToDate = false;

	public DocumentBasePresenter(EventBus eventBus, V view, Proxy_ proxy, CurrentDocument document,
			PlaceManager placeManager, DataStore dataStore) {
		super(eventBus, view, proxy);
		this.document = document;
		this.placeManager = placeManager;
		this.dataStore = dataStore;
	}

	@Override
	public void prepareFromRequest(PlaceRequest request) {
		String id = request.getParameter("id", null);
		if (id == null)
			throw new RuntimeException("Document ID not set in URL : a Gatekeeper is missing");
		document.setId(id);
		if (!upToDate)
			getView().clear();
		if (isDataSet()) {
			getProxy().manualReveal(this);
		} else {
			loadAndReveal(id);
		}
	}

	protected boolean isDataSet() {
		return document.isSet();
	}

	protected void loadAndReveal(final String id) {
		dataStore.getDocumentById(id, new LoadCallback<Document>() {

			@Override
			public void onSuccess(Document result) {
				document.set(result, id);
				getProxy().manualReveal(DocumentBasePresenter.this);
				update();
			}

			// TODO : Handle the case when no document matches ID

		});
	}

	protected void update() {
		if (!upToDate) {
			upToDate = true;
			displayDocumentData();
		}
	}

	protected abstract void displayDocumentData();

	@Override
	protected void onBind() {
		super.onBind();
		addRegisteredHandler(CurrentDocumentChangeEvent.TYPE, this);
	}

	@Override
	protected void onReset() {
		super.onReset();
		DocumentPresenterRevealedEvent.fire(getEventBus(), getProxy().getNameToken());
		if (isDataSet())
			update();
	}

	@Override
	public void onCurrentDocumentChange(CurrentDocumentChangeEvent event) {
		upToDate = false;
	}

	protected abstract class LoadCallback<T> extends BaseAsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			getProxy().manualRevealFailed();
			placeManager.revealDefaultPlace();
			super.onFailure(caught);
		}
	}

}
