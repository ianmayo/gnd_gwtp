package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.place.DocumentIdSetInUrlGatekeeper;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;

public class ChartPresenter extends DocumentBasePresenter<ChartPresenter.MyView, ChartPresenter.MyProxy> {

	@ProxyCodeSplit
	@NameToken(NameTokens.chart)
	@UseGatekeeper(DocumentIdSetInUrlGatekeeper.class)
	public interface MyProxy extends ProxyPlace<ChartPresenter> {
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, DocumentMainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends DocumentBasePresenter.ItsView {

		void drawTrackOnMap(Document document);

	}

	@Inject
	public ChartPresenter(EventBus eventBus, MyView view, MyProxy proxy, CurrentDocument document,
			PlaceManager placeManager, DataStore dataStore) {
		super(eventBus, view, proxy, document, placeManager, dataStore);
	}

	@Override
	protected void displayDocumentData() {
		getView().drawTrackOnMap(document.get());
	}
}
