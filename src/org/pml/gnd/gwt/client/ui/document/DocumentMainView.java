package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.ui.main.MainView;

import com.github.gwtbootstrap.client.ui.NavLink;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class DocumentMainView extends ViewImpl implements DocumentMainPresenter.MyView {

	public interface Binder extends UiBinder<Widget, DocumentMainView> {
	}

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiField NavLink infoLink;
	@UiField NavLink tableLink;
	@UiField NavLink graphLink;
	@UiField NavLink chartLink;

	@UiField Panel contentSlot;

	@Inject
	public DocumentMainView(final Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	@Override
	public void updateTargets(String documentId) {
		infoLink.setHref(href(NameTokens.info, documentId));
		tableLink.setHref(href(NameTokens.table, documentId));
		// TODO Replace with NameTokens
		graphLink.setHref(href(NameTokens.graph, documentId));
		chartLink.setHref(href(NameTokens.chart, documentId));

	}

	private String href(String nameToken, String documentId) {
		return "#" + nameToken + ";id=" + documentId;
	}

	@Override
	public void activateTab(String nameToken) {
		deactivateAllTabs();
		if (nameToken.equals(NameTokens.info))
			infoLink.setActive(true);
		else if (nameToken.equals(NameTokens.table))
			tableLink.setActive(true);
		// TODO Replace with NameTokens
		else if (nameToken.equals(NameTokens.graph))
			graphLink.setActive(true);
		else if (nameToken.equals(NameTokens.chart))
			chartLink.setActive(true);
	}

	private void deactivateAllTabs() {
		infoLink.setActive(false);
		tableLink.setActive(false);
		graphLink.setActive(false);
		chartLink.setActive(false);
	}

	@Override
	public void setInSlot(Object slot, Widget content) {
		if (slot == DocumentMainPresenter.CONTENT_SLOT) {
			contentSlot.clear();
			contentSlot.add(content);
		} else
			super.setInSlot(slot, content);
	}

	public static int getUsedHeight() {
		return 58 + MainView.getUsedHeight();
	}

}
