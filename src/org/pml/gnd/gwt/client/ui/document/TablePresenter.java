package org.pml.gnd.gwt.client.ui.document;

import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.model.CurrentDocument;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.place.DocumentIdSetInUrlGatekeeper;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONArray;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;

public class TablePresenter extends DocumentBasePresenter<TablePresenter.MyView, TablePresenter.MyProxy> {

	@ProxyCodeSplit
	@NameToken(NameTokens.table)
	@UseGatekeeper(DocumentIdSetInUrlGatekeeper.class)
	public interface MyProxy extends ProxyPlace<TablePresenter> {
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, DocumentMainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends DocumentBasePresenter.ItsView {

		/**
		 * show this set of data
		 * 
		 * @param array
		 *            the data to show
		 * @param thisMeasurementName
		 *            what it's called
		 */
		void showThisMeasurement(JSONArray array, String thisMeasurementName);
	}

	@Inject
	public TablePresenter(EventBus eventBus, MyView view, MyProxy proxy, CurrentDocument document,
			PlaceManager placeManager, DataStore dataStore) {
		super(eventBus, view, proxy, document, placeManager, dataStore);
	}

	@Override
	protected void displayDocumentData() {

		// special handling. do we have time?
		JSONArray timeCol = document.get().getMeasurement(Document.TIME);

		if ((timeCol == null) || (timeCol.size() == 0)) {
			throw new RuntimeException("No time facet found, unable to display document data");
		} else {
			// ok, how large will the table be?
			int numCols = document.get().getMetadata().getDatatypeCount();

			// display the time data first
			getView().showThisMeasurement(timeCol, Document.TIME);

			for (int i = 0; i < numCols; i++) {
				String thisName = document.get().getMetadata().getDatatype(i);
				if (!thisName.equals(Document.TIME)) {
					JSONArray thisCol = document.get().getMeasurement(thisName);
					if (thisCol != null)
						getView().showThisMeasurement(thisCol, thisName);
				}
			}

		}
	}

}
