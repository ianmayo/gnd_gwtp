package org.pml.gnd.gwt.client.ui.widget.Timeline;

public class DateTime
{
	public static int MILLISECOND()
	{
		return DateTimeImpl.MILLISECOND();
	}

	public static int SECOND()
	{
		return DateTimeImpl.SECOND();
	}

	public static int MINUTE()
	{
		return DateTimeImpl.MINUTE();
	}

	public static int HOUR()
	{
		return DateTimeImpl.HOUR();
	}

	public static int DAY()
	{
		return DateTimeImpl.DAY();
	}

	public static int WEEK()
	{
		return DateTimeImpl.WEEK();
	}

	public static int MONTH()
	{
		return DateTimeImpl.MONTH();
	}

	public static int YEAR()
	{
		return DateTimeImpl.YEAR();
	}

	public static int DECADE()
	{
		return DateTimeImpl.DECADE();
	}

	public static int CENTURY()
	{
		return DateTimeImpl.CENTURY();
	}

	public static int MILLENNIUM()
	{
		return DateTimeImpl.MILLENNIUM();
	}

	public static int EPOCH()
	{
		return DateTimeImpl.EPOCH();
	}

	public static int ERA()
	{
		return DateTimeImpl.ERA();
	}
}
