package org.pml.gnd.gwt.client.ui.widget;

import java.util.Date;

import org.pml.gnd.gwt.client.ui.widget.SliderValueChangeEvent.SliderValueChangeHandler;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class DateRangeInput extends Composite implements HasValue<Date> {

	private final Panel root = new FlowPanel();
	private final EventBus eventBus = new SimpleEventBus();

	private GQuerySlider wrapped;

	public DateRangeInput() {
		root.addStyleName("dateRangeInput");
		resetWrapped();
		initWidget(root);
	}

	private void resetWrapped() {
		if (wrapped != null) {
			root.remove(wrapped);
			wrapped = wrapped.copy();
		} else {
			wrapped = new GQuerySlider();
		}
		root.add(wrapped);
		wrapped.setWidth("99%");
		wrapped.addSliderValueChangeHandler(new SliderValueChangeHandler() {

			@Override
			public void onSliderValueChange(SliderValueChangeEvent event) {
				DateChangeEvent.fire(eventBus, new Date(event.getValue()));
			}
		});
		wrapped.display();
	}

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Date> handler) {
		throw new UnsupportedOperationException("Use DateChangeEvent instead");
	}

	public HandlerRegistration addDateChangeHandler(DateChangeEvent.Handler handler) {
		return eventBus.addHandler(DateChangeEvent.getType(), handler);
	}

	@Override
	public Date getValue() {
		return new Date(wrapped.getValue());
	}

	@Override
	public void setValue(Date value) {
		wrapped.setValue(value.getTime());
		resetWrapped();
	}

	@Override
	public void setValue(Date value, boolean fireEvents) {
		setValue(value);
		DateChangeEvent.fire(eventBus, value);
	}

	public void setMin(Date minTime) {
		wrapped.setMin(minTime.getTime());
		if (wrapped.getValue() == null)
			wrapped.setValue(minTime.getTime());
		resetWrapped();
	}

	public Date getMin() {
		return new Date(wrapped.getMin());
	}

	public void setMax(Date maxTime) {
		wrapped.setMax(maxTime.getTime());
		resetWrapped();
	}

	public static class DateChangeEvent extends GwtEvent<DateChangeEvent.Handler> {

		private static Type<Handler> TYPE = new Type<Handler>();

		public interface Handler extends EventHandler {

			void onDateChange(DateChangeEvent event);

		}

		private final Date value;

		DateChangeEvent(Date date) {
			this.value = date;
		}

		public Date getValue() {
			return value;
		}

		@Override
		protected void dispatch(Handler handler) {
			handler.onDateChange(this);
		}

		@Override
		public Type<Handler> getAssociatedType() {
			return TYPE;
		}

		public static Type<Handler> getType() {
			return TYPE;
		}

		public static void fire(EventBus source, Date value) {
			// The date must be copied in case one handler causes it to change.
			source.fireEvent(new DateChangeEvent(CalendarUtil.copyDate(value)));
		}

	}

}
