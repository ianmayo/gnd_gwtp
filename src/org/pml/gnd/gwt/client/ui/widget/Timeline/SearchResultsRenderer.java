package org.pml.gnd.gwt.client.ui.widget.Timeline;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;

/**
 * Render timeline
 * 
 */
public class SearchResultsRenderer implements ITimeLineRender
{

	Date minDate = new Date();
	Date maxDate = new Date();

	/**
	 * Create timeline custom elements.
	 * 
	 * @param maxDate
	 * @param minDate
	 * 
	 * @param widget
	 *          Timeline rendered into this widget.
	 */
	public SearchResultsRenderer(Date minDate, Date maxDate)
	{
		this.maxDate = maxDate == null ? new Date() : maxDate;
		this.minDate = minDate == null ? new Date() : minDate;
	}

	public SearchResultsRenderer()
	{
		// TODO Auto-generated constructor stub
	}

	public void render(TimeLineWidget widget)
	{
		
		DateTimeFormat format = DateTimeFormat
				.getFormat("MMM dd yyyy HH:mm:ss z");
		
		ArrayList bandInfos = widget.getBandInfos();
		ArrayList bandHotZones = widget.getBandHotZones();
		EventSource eventSource = widget.getEventSource();

		Theme theme = widget.getTheme();
		theme.setEventLabelWidth(400);

		// ---------------------------------------------------------------
		// HotZones, two events too close together so we are going to
		// 'stretch' the time along them to make the gap between them
		// larger but still maintain the flow of time.
		// ---------------------------------------------------------------
		HotZoneBandOptions hotZone1Opts = HotZoneBandOptions.create();
		hotZone1Opts.setStart(format.format(minDate));
		hotZone1Opts.setEnd(format.format(maxDate));
		hotZone1Opts.setMagnify(1);
		hotZone1Opts.setUnit(DateTime.MONTH());
		hotZone1Opts.setMultiple(1);
		bandHotZones.add(hotZone1Opts);

		// ---------------------------------------------------------------
		// Bands
		// ---------------------------------------------------------------

		BandOptions topOpts = BandOptions.create();
		topOpts.setWidth("5%");
		topOpts.setIntervalUnit(DateTime.YEAR());
		topOpts.setIntervalPixels(200);
		topOpts.setShowEventText(false);
		topOpts.setTheme(theme);
		topOpts.setDate(format.format(minDate));

		BandInfo top = BandInfo.create(topOpts);
		// top.setDecorators(bandDecorators);
		//bandInfos.add(top);

		// Bands
		BandOptions bottomOpts = BandOptions.create();
		bottomOpts.setWidth("100%");
		bottomOpts.setTrackHeight(1.3f);
		bottomOpts.setTrackGap(0.1f);
		bottomOpts.setIntervalUnit(DateTime.MONTH());
		bottomOpts.setIntervalPixels(50);
		bottomOpts.setZoomIndex(10);
		bottomOpts.setShowEventText(true);
		bottomOpts.setTheme(theme);
		bottomOpts.setEventSource(eventSource);
		bottomOpts.setDate(format.format(minDate));
		bottomOpts.setZones(bandHotZones);
		//bottomOpts.setTimeZone(0);

		BandInfo bottom = BandInfo.createHotZone(bottomOpts);
		bandInfos.add(bottom);

		//bottom.setSyncWith(0);
		//bottom.setHighlight(true);
	}

}
