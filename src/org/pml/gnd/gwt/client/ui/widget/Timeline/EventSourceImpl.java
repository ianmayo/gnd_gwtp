package org.pml.gnd.gwt.client.ui.widget.Timeline;

import org.pml.gnd.gwt.client.ui.search.TimelinePresenter.MyView;

import com.google.gwt.core.client.JavaScriptObject;

class EventSourceImpl
{
	// -------------------------------------------------------------------
	// EventSource
	// -------------------------------------------------------------------

	public native static EventSource createEventSourceObject(MyView timelineView) /*-{
		var impl = this;
		var source = new $wnd.Timeline.DefaultEventSource();

		if (!$wnd.default_fillInfo)
			$wnd.default_fillInfo = $wnd.Timeline.DefaultEventSource.Event.prototype.fillInfoBubble;

		$wnd.Timeline.DefaultEventSource.Event.prototype.fillInfoBubble = function(
				elmt, theme, labeller) {
			// if not a depth labeller, use the original
			if (!labeller.isDepthLabeller) {
				$wnd.default_fillInfo.apply(this, arguments);
				timelineView.@org.pml.gnd.gwt.client.ui.search.TimelinePresenter.MyView::callback(Lcom/google/gwt/core/client/JavaScriptObject;)(elmt);
				return;
			}
			// build our custom markup
		};

		return source;
	}-*/;

	public native static void clear(EventSource source) /*-{
		source.clear();
	}-*/;

	public native static void loadXML(JavaScriptObject xml, String url,
			EventSource source) /*-{
		source.loadXML(xml, url);
	}-*/;

	public native static void loadXMLText(String xml, String url,
			EventSource source) /*-{
		var xmlDoc = $wnd.TimelineHelper.parseXML(xml);

		source.loadXML(xmlDoc, url);
	}-*/;

	public native static void loadXML(String dataUrl, EventSource eventSource) /*-{
		$wnd.Timeline.loadXML(dataUrl, function(xml, url) {
			eventSource.loadXML(xml, url);
		});
	}-*/;

	public native static void loadJSON(String jsonString, String url,
			EventSource eventSource)/*-{
		var jsonData = eval('(' + jsonString + ')');
		eventSource.loadJSON(jsonData, url);
	}-*/;

}
