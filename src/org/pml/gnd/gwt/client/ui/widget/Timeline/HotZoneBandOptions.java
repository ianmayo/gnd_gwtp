package org.pml.gnd.gwt.client.ui.widget.Timeline;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Window;


/**
 * BandOptions
 *
 * @author ajr
 */
public class HotZoneBandOptions extends JavaScriptObject
{
    protected HotZoneBandOptions()
    {
        super();
    }

    public static HotZoneBandOptions create()
    {
        return HotZoneBandOptionsImpl.create();
    }

    /**
     * required, a String or a Date object that specifies the beginning date/time of the zone. It is parsed by Timeline.DateTime.parseGregorianDateTime()  
     * to get a Date object.
     */
    public final void setStart(String value)
    {
        JavaScriptObjectHelper.setAttribute(this, "start", value);
    }

    /**
     * required, a String or a Date object that specifies the ending date/time of the zone. It is parsed by Timeline.DateTime.parseGregorianDateTime()  
     * to get a Date object.
     */
    public final void setEnd(String value)
    {
        JavaScriptObjectHelper.setAttribute(this, "end", value);
    }

    /**
     * required, a number specifying the magnification of the mapping in this zone. A greater-than-1 number causes more pixels to be mapped 
     * to the same time interval, resulting in a zoom-in effect.
     */
    public final void setMagnify(int value)
    {
        JavaScriptObjectHelper.setAttribute(this, "magnify", value);
    }

    /**
     * required, one of the Gregorian calendar unit defined in Timeline.DateTime, e.g., Timeline.DateTime.MINUTE. This argument specifies 
     * the interval at which ticks and labels are painted on the band's background inside this hot-zone..
     */
    public final void setUnit(int value)
    {
        JavaScriptObjectHelper.setAttribute(this, "unit", value);
    }
    
    /**
     * optional, default to 1. A label is painted for every multiple of unit. For example, if unit is Timeline.DateTime.MINUTE and 
     * multiple is 15, then there is a label for every 15 minutes (i.e., 15, 30, 45,...). 
     */
    public final void setMultiple(int value)
    {
        JavaScriptObjectHelper.setAttribute(this, "multiple", value);
    }
        
}
