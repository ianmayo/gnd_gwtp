package org.pml.gnd.gwt.client.ui.widget.Timeline;

import com.google.gwt.core.client.JavaScriptObject;

public class Theme extends JavaScriptObject
{
	protected Theme()
	{
		super();
	}

	public static Theme create()
	{
		return ThemeImpl.create();
	}

	public final void setEventLabelWidth(int width)
	{
		ThemeImpl.setEventLabelWidth(this, width);
	}

}
