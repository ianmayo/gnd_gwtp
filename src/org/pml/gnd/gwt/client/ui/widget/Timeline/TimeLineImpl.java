package org.pml.gnd.gwt.client.ui.widget.Timeline;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Element;

/**
 * TimeLineImpl
 * 
 *
 */
class TimeLineImpl
{
    // -------------------------------------------------------------------
    // Timeline
    // -------------------------------------------------------------------

    public native static TimeLine create(JavaScriptObject bands, Element divElement) /*-{
        return $wnd.Timeline.create(divElement, bands);
    }-*/;

 /*   public native static void loadXML(String dataUrl, TimelineXMLHandler handler) -{
	    $wnd.Timeline.loadXML(dataUrl, function(xml, url) { handler.@com.netthreads.gwt.simile.timeline.client.TimelineXMLHandler::onCompletion(Lcom/google/gwt/core/client/JavaScriptObject;Ljava/lang/String;)(xml,url) });
	}-;*/

    public native static int getBandCount(TimeLine timeLine) /*-{
        return timeLine.getBandCount();
    }-*/;
    
    public native static void layout(TimeLine timeLine) /*-{
        timeLine.layout();
    }-*/;

    public native static void closeBubble(int index, TimeLine timeLine) /*-{
			timeLine.getBand(index).closeBubble();
		}-*/;
    
}
