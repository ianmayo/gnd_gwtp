package org.pml.gnd.gwt.client.ui.widget.Timeline;

public interface ITimeLineRender
{
	public void render(TimeLineWidget widget);
}
