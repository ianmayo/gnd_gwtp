package org.pml.gnd.gwt.client.ui.widget;

import static com.google.gwt.query.client.GQuery.$;
import static gwtquery.plugins.ui.Ui.Ui;
import gwtquery.plugins.ui.widgets.Slider;

import org.pml.gnd.gwt.client.ui.widget.SliderValueChangeEvent.HasSliderValueChangeHandlers;
import org.pml.gnd.gwt.client.ui.widget.SliderValueChangeEvent.SliderValueChangeHandler;

import com.google.common.base.Preconditions;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.query.client.Function;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;

public class GQuerySlider extends Widget implements HasSliderValueChangeHandlers {

	private static final int step = 60 * 1000; // One minute

	private Long min;
	private Long max;
	private Long value;

	private final EventBus eventBus = new SimpleEventBus();

	public GQuerySlider() {
		setElement(DOM.createDiv());
	}

	public GQuerySlider(Long min, Long max, Long value) {
		Preconditions.checkNotNull(min);
		Preconditions.checkNotNull(max);

		Slider.Options options = Slider.Options.create();
		options.step(step);
		this.min = min;
		options.min(0);
		this.max = max;
		options.max(max - min);
		this.value = value;
		if (value == null)
			options.value(0);
		else
			options.value(value - min);

		Element root = DOM.createDiv();
		root.setClassName("sliderContainer");

		Element child = DOM.createDiv();
		child.setId("slider");
		root.appendChild(child);

		setElement(root);
		// Add slider to page with bound SlideEventHandler
		$("#slider", root).as(Ui).slider(options).bind(Slider.Event.slide, new SlideEventHandler());
	}

	public void display() {
	}

	public void setMin(Long min) {
		Preconditions.checkNotNull(min);
		this.min = min;
	}

	public Long getMin() {
		return min;
	}

	public void setMax(Long max) {
		Preconditions.checkNotNull(max);
		this.max = max;
	}

	public Long getMax() {
		return max;
	}

	public void setValue(Long newValue) {
		Preconditions.checkArgument(newValue >= min, "Can't set value smaller than minimum value");
		Preconditions.checkArgument(max == null || newValue <= max, "Can't set value bigger than maximum value");
		this.value = newValue;
	}

	public Long getValue() {
		return value;
	}

	public GQuerySlider copy() {
		if (isConfigured()) {
			return new GQuerySlider(min, max, value);
		} else {
			GQuerySlider toReturn = new GQuerySlider();
			if (min != null)
				toReturn.setMin(min);
			if (max != null)
				toReturn.setMax(max);
			return toReturn;
		}
	}

	@Override
	public HandlerRegistration addSliderValueChangeHandler(SliderValueChangeHandler handler) {
		return eventBus.addHandler(SliderValueChangeEvent.TYPE, handler);
	}

	private class SlideEventHandler extends Function {

		@Override
		public boolean f(Event e, Object data) {
			Slider.Event slideEvent = ((JavaScriptObject) data).cast();
			value = Long.valueOf(slideEvent.intValue() + min);
			SliderValueChangeEvent.fire(eventBus, value);
			return false;
		}
	}

	public boolean isConfigured() {
		return min != null && max != null;
	}

}
