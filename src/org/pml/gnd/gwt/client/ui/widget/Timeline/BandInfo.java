package org.pml.gnd.gwt.client.ui.widget.Timeline;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * BandInfo
 * 
 */
public class BandInfo extends JavaScriptObject
{
	protected BandInfo()
	{
		super();
	}

	public static BandInfo create(BandOptions options)
	{
		return BandInfoImpl.create(options);
	}

	public static BandInfo createHotZone(BandOptions options)
	{
		return BandInfoImpl.createHotZone(options);
	}

	public final void setSyncWith(int value)
	{
		BandInfoImpl.setSyncWith(this, value);
	}

	public final void setHighlight(boolean value)
	{
		BandInfoImpl.setHighlight(this, value);
	}

	/**
	 * optional, Set decorator list
	 * 
	 * @param decorators
	 */
	public final void setDecorators(List decorators)
	{
		JavaScriptObject[] decoratorArr = JavaScriptObjectHelper
				.listToArray(decorators);

		JavaScriptObject jarr = JavaScriptObjectHelper.arrayConvert(decoratorArr);

		BandInfoImpl.setDecorators(this, jarr);
	}
}
