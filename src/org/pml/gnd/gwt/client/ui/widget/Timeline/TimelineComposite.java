/**
 * 
 */
package org.pml.gnd.gwt.client.ui.widget.Timeline;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Akash-Gupta
 * 
 */
public class TimelineComposite extends Composite
{
	Date maxDate;
	Date minDate;
	DateTimeFormat format = DateTimeFormat
			.getFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public TimelineComposite()
	{
		final SimplePanel panel = new SimplePanel();
		initWidget(panel);
		Scheduler.get().scheduleDeferred(new ScheduledCommand()
		{

			@Override
			public void execute()
			{
				JSONObject obj = initJSON();
				initTimeline(panel.getElement(), obj.getJavaScriptObject());
			}

		});
	}

	private JSONObject initJSON()
	{
		JSONObject obj = new JSONObject();
		obj.put("dateTimeFormat", new JSONString("iso8601"));
		obj.put("wikiURL", new JSONString("http://wiki.url.todo/")); // TODO
		obj.put("wikiSection", new JSONString("wikiSection"));// TODO

		JSONArray array = new JSONArray();

		JSONObject data = new JSONObject(getData());

		JSONArray arr = data.get("hits").isObject().get("hits").isArray();
		for (int i = 0; i < arr.size(); i++)
		{
			JSONObject event = new JSONObject();

			JSONString start = arr.get(i).isObject().get("fields").isObject()
					.get("metadata.time_bounds.start").isString();
			Date startDate = format.parse(start.stringValue());

			event.put("start", start);

			if (minDate == null || minDate.after(startDate))
			{
				minDate = startDate;
			}

			JSONString stop = arr.get(i).isObject().get("fields").isObject()
					.get("metadata.time_bounds.end").isString();
			Date stopDate = format.parse(stop.stringValue());

			event.put("end", stop);

			if (maxDate == null || maxDate.before(stopDate))
			{
				maxDate = stopDate;
			}

			event.put("title",
					arr.get(i).isObject().get("fields").isObject().get("metadata.name")
							.isString());
			array.set(i, event);
		}

		obj.put("events", array);// TODO

		return obj;

	}

	public static native JavaScriptObject getData()
	/*-{
		return window.parent.jsonData;
	}-*/;

	public static native void initTimeline(Element tl_el,
			JavaScriptObject javaScriptObject) /*-{
		var eventSource1 = new window.parent.Timeline.DefaultEventSource();
		var theme1 = window.parent.Timeline.ClassicTheme.create();
		theme1.autoWidth = true; // Set the Timeline's "width" automatically.
		// Set autoWidth on the Timeline's first band's theme,
		// will affect all bands.
		theme1.timeline_start = new Date(Date.UTC(2011, 9, 2));
		theme1.timeline_stop = new Date(Date.UTC(2011, 9, 3));

		var d = window.parent.Timeline.DateTime
				.parseGregorianDateTime(new Date(Date.UTC(2011, 9, 3, 12, 0, 0)))
		var bandInfos = [ window.parent.Timeline.createBandInfo({
			width : 45, // set to a minimum, autoWidth will then adjust
			intervalUnit : window.parent.Timeline.DateTime.DAY,
			intervalPixels : 200,
			eventSource : eventSource1,
			date : d,
			theme : theme1,
			layout : 'original' // original, overview, detailed
		}) ];

		var bandInfos = [ window.parent.Timeline.createBandInfo({
			width : "70%",
			intervalUnit : window.parent.Timeline.DateTime.HOUR,
			intervalPixels : 100,
			eventSource : eventSource1,
			date : d,
			theme : theme1,
			layout : 'original'
		}), window.parent.Timeline.createBandInfo({
			width : "30%",
			intervalUnit : window.parent.Timeline.DateTime.DAY,
			intervalPixels : 200,
			eventSource : eventSource1,
			date : d,
			theme : theme1,
			layout : 'original'
		}) ];

		// create the Timeline
		tl = window.parent.Timeline.create(tl_el, bandInfos,
				window.parent.Timeline.HORIZONTAL);

		var url = '.'; // The base url for image, icon and background image
		// references in the data

		eventSource1.loadJSON(javaScriptObject, url); // The data was stored into the 

		// timeline_data variable.
		tl.layout(); // display the Timeline

	}-*/;
}
