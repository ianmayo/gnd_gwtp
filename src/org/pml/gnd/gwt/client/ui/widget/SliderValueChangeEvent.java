package org.pml.gnd.gwt.client.ui.widget;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.EventHandler;
import java.lang.Long;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.event.shared.HandlerRegistration;

public class SliderValueChangeEvent extends GwtEvent<SliderValueChangeEvent.SliderValueChangeHandler> {

	public static Type<SliderValueChangeHandler> TYPE = new Type<SliderValueChangeHandler>();
	private Long value;

	public interface SliderValueChangeHandler extends EventHandler {

		void onSliderValueChange(SliderValueChangeEvent event);
	}

	public interface HasSliderValueChangeHandlers extends HasHandlers {

		HandlerRegistration addSliderValueChangeHandler(SliderValueChangeHandler handler);
	}

	public SliderValueChangeEvent(Long value) {
		this.value = value;
	}

	public Long getValue() {
		return value;
	}

	@Override
	protected void dispatch(SliderValueChangeHandler handler) {
		handler.onSliderValueChange(this);
	}

	@Override
	public Type<SliderValueChangeHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<SliderValueChangeHandler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source, Long value) {
		source.fireEvent(new SliderValueChangeEvent(value));
	}
}