package org.pml.gnd.gwt.client.ui.widget.Timeline;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.UIObject;

/**
 * TimeLine
 * 
 */
public class TimeLine extends JavaScriptObject
{
	protected TimeLine()
	{
		super();
	}

	/**
	 * Create TimeLine object
	 */
	public static TimeLine create(List bands, EventSource source,
			Element divElement, Element clientElement)
	{
		JavaScriptObject[] bandArr = JavaScriptObjectHelper.listToArray(bands);

		JavaScriptObject jarr = JavaScriptObjectHelper.arrayConvert(bandArr);

		boolean currVisible = UIObject.isVisible(clientElement);
		UIObject.setVisible(clientElement, true);

		TimeLine timeLine = TimeLineImpl.create(jarr, divElement);

		UIObject.setVisible(clientElement, currVisible);

		return timeLine;

	}

	/**
	 * Redraw timeline
	 * 
	 */
	public final void layout()
	{
		TimeLineImpl.layout(this);
	}

	/**
	 * loadXML through handler function.
	 * 
	 * @param dataUrl
	 * @param handler
	 */
	/*
	 * public final void loadXML(String dataUrl, TimelineXMLHandler handler) {
	 * TimeLineImpl.loadXML(dataUrl, handler); }
	 */

	/**
	 * Close info bubble for indicated band
	 * 
	 * @param index
	 */
	public final void closeBubble(int index)
	{
		TimeLineImpl.closeBubble(index, this);
	}

}
