package org.pml.gnd.gwt.client.ui.widget.Timeline;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Window;

/**
 * BandOptions
 * 
 */
public class BandOptions extends JavaScriptObject
{
	protected BandOptions()
	{
		super();
	}

	public static BandOptions create()
	{
		return BandOptionsImpl.create();
	}

	/**
	 * how much of the timeline's space this band takes up, expressed as a percent
	 * in a string, e.g., "30%"
	 * 
	 */
	public final String getWidth()
	{
		return JavaScriptObjectHelper.getAttribute(this, "width");
	}

	/**
	 * required, how much of the timeline's space this band takes up, expressed as
	 * a percent in a string, e.g., "30%"
	 */
	public final void setWidth(String width)
	{
		JavaScriptObjectHelper.setAttribute(this, "width", width);
	}

	/**
	 * a time unit from Timeline.DateTime, e.g., Timeline.DateTime.WEEK.
	 * 
	 */
	public final int getIntervalUnit()
	{
		return JavaScriptObjectHelper.getAttributeAsInt(this, "intervalUnit");
	}

	/**
	 * required, a time unit from Timeline.DateTime, e.g., Timeline.DateTime.WEEK.
	 */
	public final void setIntervalUnit(int value)
	{
		JavaScriptObjectHelper.setAttribute(this, "intervalUnit", value);
	}

	/**
	 * the number of pixels that the time unit above is mapped to, e.g., 100.
	 * 
	 */
	public final int getIntervalPixels()
	{
		return JavaScriptObjectHelper.getAttributeAsInt(this, "intervalPixels");
	}

	/**
	 * required, the number of pixels that the time unit above is mapped to, e.g.,
	 * 100.
	 */
	public final void setIntervalPixels(int value)
	{
		JavaScriptObjectHelper.setAttribute(this, "intervalPixels", value);
	}
	
	
	/**
	 * intervalPixels/intervalUnit steps that you would like the user to be able to zoom to.
	 */
	public final void setZoomIndex(int value)
	{
		JavaScriptObjectHelper.setAttribute(this, "zoomIndex", value);

		/*List<JavaScriptObject> list = new ArrayList<JavaScriptObject>();
		
		JavaScriptObject obj1 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj1, "pixelsPerInterval", 280);
		JavaScriptObjectHelper.setAttribute(obj1, "unit", DateTime.HOUR());
		
		JavaScriptObject obj2 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj2, "pixelsPerInterval", 140);
		JavaScriptObjectHelper.setAttribute(obj2, "unit", DateTime.HOUR());
		
		JavaScriptObject obj3 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj3, "pixelsPerInterval", 70);
		JavaScriptObjectHelper.setAttribute(obj3, "unit", DateTime.HOUR());
		
		JavaScriptObject obj4 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj4, "pixelsPerInterval", 35);
		JavaScriptObjectHelper.setAttribute(obj4, "unit", DateTime.HOUR());
		
		JavaScriptObject obj5 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj5, "pixelsPerInterval", 400);
		JavaScriptObjectHelper.setAttribute(obj5, "unit", DateTime.DAY());
		
		JavaScriptObject obj6 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj6, "pixelsPerInterval", 200);
		JavaScriptObjectHelper.setAttribute(obj6, "unit", DateTime.DAY());
		
		JavaScriptObject obj7 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj7, "pixelsPerInterval", 100);
		JavaScriptObjectHelper.setAttribute(obj7, "unit", DateTime.DAY());
		
		JavaScriptObject obj8 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj8, "pixelsPerInterval", 50);
		JavaScriptObjectHelper.setAttribute(obj8, "unit", DateTime.DAY());
		
		JavaScriptObject obj9 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj9, "pixelsPerInterval", 400);
		JavaScriptObjectHelper.setAttribute(obj9, "unit", DateTime.MONTH());
		
		JavaScriptObject obj10 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj10, "pixelsPerInterval", 200);
		JavaScriptObjectHelper.setAttribute(obj10, "unit", DateTime.MONTH());
		
		JavaScriptObject obj11 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj11, "pixelsPerInterval", 100);
		JavaScriptObjectHelper.setAttribute(obj11, "unit", DateTime.MONTH());
		
		JavaScriptObject obj12 = JavaScriptObject.createObject();
		JavaScriptObjectHelper.setAttribute(obj12, "pixelsPerInterval", 50);
		JavaScriptObjectHelper.setAttribute(obj12, "unit", DateTime.MONTH());
		
		list.add(obj1);
		list.add(obj2);
		list.add(obj3);
		list.add(obj4);
		list.add(obj5);
		list.add(obj6);
		list.add(obj7);
		list.add(obj8);
		list.add(obj9);
		list.add(obj10);
		list.add(obj11);
		list.add(obj12);
		
		JavaScriptObject[] zoomSteps = JavaScriptObjectHelper.listToArray(list);
		
		JavaScriptObjectHelper.setAttribute(this, "zoomSteps",zoomSteps);*/
		JavaScriptObjectHelper.setAttribute(this, "zoomSteps", getZoomSteps());
	}

	private native JavaScriptObject getZoomSteps()/*-{
   return new Array(
              {pixelsPerInterval: 280,  unit: $wnd.Timeline.DateTime.HOUR},
              {pixelsPerInterval: 140,  unit: $wnd.Timeline.DateTime.HOUR},
              {pixelsPerInterval:  70,  unit: $wnd.Timeline.DateTime.HOUR},
              {pixelsPerInterval:  35,  unit: $wnd.Timeline.DateTime.HOUR},
              {pixelsPerInterval: 400,  unit: $wnd.Timeline.DateTime.DAY},
              {pixelsPerInterval: 200,  unit: $wnd.Timeline.DateTime.DAY},
              {pixelsPerInterval: 100,  unit: $wnd.Timeline.DateTime.DAY},
              {pixelsPerInterval:  50,  unit: $wnd.Timeline.DateTime.DAY},
              {pixelsPerInterval: 400,  unit: $wnd.Timeline.DateTime.MONTH},
              {pixelsPerInterval: 200,  unit: $wnd.Timeline.DateTime.MONTH},
              {pixelsPerInterval: 100,  unit: $wnd.Timeline.DateTime.MONTH}, // DEFAULT zoomIndex
              {pixelsPerInterval: 400,  unit: $wnd.Timeline.DateTime.YEAR},
              {pixelsPerInterval: 200,  unit: $wnd.Timeline.DateTime.YEAR},
              {pixelsPerInterval: 100,  unit: $wnd.Timeline.DateTime.YEAR},
              {pixelsPerInterval: 400,  unit: $wnd.Timeline.DateTime.DECADE},
              {pixelsPerInterval: 200,  unit: $wnd.Timeline.DateTime.DECADE},
              {pixelsPerInterval: 100,  unit: $wnd.Timeline.DateTime.DECADE}
            )
}-*/;

	/**
	 * a boolean specifying whether event titles are to be painted. The default is
	 * true.
	 * 
	 */
	public final boolean getShowEventText()
	{
		return JavaScriptObjectHelper.getAttributeAsBoolean(this, "showEventText");
	}

	/**
	 * optional, a boolean specifying whether event titles are to be painted. The
	 * default is true.
	 */
	public final void setShowEventText(boolean value)
	{
		JavaScriptObjectHelper.setAttribute(this, "showEventText", value);
	}

	/**
	 * the number of em (dependent on the current font) to be left between
	 * adjacent tracks on which events are painted. The default value is retrieved
	 * from the provided or default theme. E.g., 0.5.
	 * 
	 */
	public final float getTrackGap()
	{
		return JavaScriptObjectHelper.getAttributeAsFloat(this, "trackGap");
	}

	/**
	 * optional, the number of em (dependent on the current font) to be left
	 * between adjacent tracks on which events are painted. The default value is
	 * retrieved from the provided or default theme. E.g., 0.5.
	 */
	public final void setTrackGap(float value)
	{
		JavaScriptObjectHelper.setAttribute(this, "trackGap", value);
	}

	/**
	 * the height of each track in em (dependent on the current font). The default
	 * value is retrieved from the provided or default theme. E.g., 1.5.
	 * 
	 */
	public final float getTrackHeight()
	{
		return JavaScriptObjectHelper.getAttributeAsFloat(this, "trackHeight");
	}

	/**
	 * optional, the height of each track in em (dependent on the current font).
	 * The default value is retrieved from the provided or default theme. E.g.,
	 * 1.5.
	 */
	public final void setTrackHeight(float value)
	{
		JavaScriptObjectHelper.setAttribute(this, "trackHeight", value);
	}

	/**
	 * required, Band theme
	 */
	public final void setTheme(Theme value)
	{
		JavaScriptObjectHelper.setAttribute(this, "theme", value);
	}

	/**
	 * Band theme
	 */
	public final JavaScriptObject getTheme()
	{
		return JavaScriptObjectHelper.getAttributeAsJavaScriptObject(this, "theme");
	}

	/**
	 * required, Event source
	 */
	public final void setEventSource(EventSource value)
	{
		JavaScriptObjectHelper.setAttribute(this, "eventSource", value);
	}

	/**
	 * Event source
	 */
	public final JavaScriptObject getEventSource()
	{
		return JavaScriptObjectHelper.getAttributeAsJavaScriptObject(this,
				"eventSource");
	}

	/**
	 * optional, set date
	 */
	public final void setDate(String value)
	{
		JavaScriptObjectHelper.setAttribute(this, "date", value);
	}

	/**
	 * optional, set time zone
	 */
	public final void setTimeZone(int value)
	{
		JavaScriptObjectHelper.setAttribute(this, "timeZone", value);
	}

	/**
	 * optional, set hot zones
	 */
	public final void setZones(List zones)
	{
		JavaScriptObject[] zonesArr = JavaScriptObjectHelper.listToArray(zones);

		JavaScriptObject jarr = JavaScriptObjectHelper.arrayConvert(zonesArr);

		JavaScriptObjectHelper.setAttribute(this, "zones", jarr);
	}

}
