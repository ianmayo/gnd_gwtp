package org.pml.gnd.gwt.client.ui.widget.Timeline;

import org.pml.gnd.gwt.client.ui.search.TimelinePresenter.MyView;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;

public class EventSource extends JavaScriptObject
{
    protected EventSource()
    {
        super();
    }

    public static EventSource create(MyView timelineView)
    {
        return EventSourceImpl.createEventSourceObject(timelineView);
    }
    
    /**
     * Load data stream into timeline event source
     * @param dataUrl
     */
    public final void loadXML(String dataUrl)
    {
        EventSourceImpl.loadXML(dataUrl, this);
    }

    /**
     * Load xml string directly into source
     * 
     * @param dataUrl
     */
    public final void loadXMLText(String xmlText)
    {
        EventSourceImpl.loadXMLText(xmlText, GWT.getModuleBaseURL(), this);
    }
    
    /**
     * Load json string directly into source
     *  
     * @param json
     */
    public final void loadJSON(String json) 
    {
		EventSourceImpl.loadJSON(json, GWT.getModuleBaseURL(),this);
	}
    
    /**
     * Load data stream into timeline event source
     * @param dataUrl
     */
    public final void load(JavaScriptObject xml, String url)
    {
        EventSourceImpl.loadXML(xml, url, this);
    }
    

    public final void clear()
    {
        EventSourceImpl.clear(this);
    }
    
}
