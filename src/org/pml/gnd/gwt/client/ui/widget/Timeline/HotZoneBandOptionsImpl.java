package org.pml.gnd.gwt.client.ui.widget.Timeline;

/**
 * BandOptionsImpl
 * 
 * @author ajr
 *
 */
class HotZoneBandOptionsImpl
{
    public static native HotZoneBandOptions create()/*-{
        return new Object;
    }-*/;
}
