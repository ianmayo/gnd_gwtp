package org.pml.gnd.gwt.client.ui.widget;

import com.github.gwtbootstrap.client.ui.Heading;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.Constants;

public class CompatibleAlertBlock extends CompatibleAlertBase {

	private final Heading heading = new Heading(4);

	/**
	 * Creates an empty widget with a close icon.
	 */
	public CompatibleAlertBlock() {
		this("");
	}

	public CompatibleAlertBlock(String html) {
		super(html);
		setUp();
	}

	private void setUp() {
		super.addStyleName(Constants.ALERT_BLOCK);
		heading.setStyleName(Constants.ALERT_HEADING);
		getHeadingContainer().add(heading);
	}

	/**
	 * Initializes the widget with an optional close icon.
	 * 
	 * @param hasClose
	 *            whether the Alert should have a close icon.
	 */
	public CompatibleAlertBlock(boolean hasClose) {
		super("", hasClose);
		setUp();
	}

	/**
	 * Creates an Alert with a close icon and the given style.
	 * 
	 * @param type
	 *            of the Alert
	 */
	public CompatibleAlertBlock(AlertType type) {
		super(type);
		setUp();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setType(AlertType type) {
		super.setType(type);
		addStyleName(Constants.ALERT_BLOCK);
	}

	/**
	 * Sets the text of an optional heading. It is wrapped in {@code <h4>} tags and placed above the text.
	 */
	@Override
	public void setHeading(String text) {
		heading.setText(text);
	}
}
