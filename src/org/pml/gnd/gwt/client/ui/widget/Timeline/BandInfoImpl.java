package org.pml.gnd.gwt.client.ui.widget.Timeline;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * BandInfoImpl
 * 
 */
class BandInfoImpl
{
	// -------------------------------------------------------------------
	// Band
	// -------------------------------------------------------------------

	public native static BandInfo create(BandOptions options) /*-{
		return $wnd.Timeline.createBandInfo(options)
	}-*/;

	public native static BandInfo createHotZone(BandOptions options) /*-{
		return $wnd.Timeline.createHotZoneBandInfo(options)
	}-*/;

	public native static void setSyncWith(BandInfo band, int index) /*-{
		band.syncWith = index;
	}-*/;

	public native static void setHighlight(BandInfo band, boolean value) /*-{
		band.highlight = value;
	}-*/;

	public native static void setDecorators(BandInfo band, JavaScriptObject value) /*-{
		band.decorators = value;
	}-*/;

}