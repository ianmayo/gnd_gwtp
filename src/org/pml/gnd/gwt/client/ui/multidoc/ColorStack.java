package org.pml.gnd.gwt.client.ui.multidoc;

import static com.google.gwt.query.client.css.RGBColor.*;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.gwt.query.client.css.RGBColor;

public class ColorStack {

	private final List<RGBColor> colors = Lists.newArrayList();

	public ColorStack() {
		// TODO Check if we can add more colors
		// PINK
		// CYAN
		colors.add(RED);
		colors.add(GREEN);
		colors.add(BLUE);
		colors.add(ORANGE);
		colors.add(PURPLE);
		colors.add(MAROON);
		colors.add(AQUA);
		colors.add(TEAL);
		colors.add(LIME);
		colors.add(YELLOW);
		colors.add(SILVER);
		colors.add(NAVY);
		colors.add(OLIVE);
		colors.add(GREY);
	}

	public void add(RGBColor color) {
		if (!colors.contains(color))
			colors.add(color);
	}

	public RGBColor pop() {
		if (colors.isEmpty())
			return BLACK;
		return colors.remove(0);
	}
}
