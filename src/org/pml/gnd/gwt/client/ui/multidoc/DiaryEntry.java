package org.pml.gnd.gwt.client.ui.multidoc;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Narrative;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.common.collect.Lists;
import com.google.gwt.json.client.JSONArray;

public class DiaryEntry implements Comparable<DiaryEntry> {

	private final Date time;
	private final String platform;
	private final String comment;

	public DiaryEntry(Date time, String platform, String comment) {
		this.time = time;
		this.platform = platform;
		this.comment = comment;
	}

	public Date getTime() {
		return time;
	}

	public String getPlatform() {
		return platform;
	}

	public String getComment() {
		return comment;
	}

	public static Collection<DiaryEntry> listAll(Document document) {
		List<DiaryEntry> entries = Lists.newArrayList();
		String platform = document.getMetadata().getPlatform();
		JSONArray time = document.getMeasurement(Document.TIME);
		JSONArray obs = document.getMeasurement(Narrative.OBSERVATION);
		for (int i = 0; i < time.size(); i++) {
			Date date = DateFormats.CUSTOM.parse(time.get(i).isString().stringValue());
			DiaryEntry entry = new DiaryEntry(date, platform, obs.get(i).toString());
			entries.add(entry);
		}
		return entries;
	}

	@Override
	public String toString() {
		return "DiaryEntry(" + time + ", " + platform + ", " + comment + ")";
	}

	@Override
	public int compareTo(DiaryEntry that) {
		return this.time.compareTo(that.time);
	}
}
