package org.pml.gnd.gwt.client.ui.multidoc;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class DocumentCell extends AbstractCell<LegendItem> {

	@Override
	public void render(Context context, LegendItem value, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}

		sb.appendHtmlConstant("<table><tr>");

		// Colored square
		sb.appendHtmlConstant("<td style='background-color:" + value.getColor().getCssName() + ";width:1.5em;'></td>");

		// Document Name
		sb.appendHtmlConstant("<td style='font-size:95%;'>&nbsp;");
		sb.appendEscaped(value.getDocument().getMetadata().getName() + " - "
							+ value.getDocument().getMetadata().getTrial());
		sb.appendHtmlConstant("</td>");

		// Just a spacing between rows
		sb.appendHtmlConstant("</tr><tr><td></td>");

		sb.appendHtmlConstant("</tr></table>");
	}
}
