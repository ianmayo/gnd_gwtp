package org.pml.gnd.gwt.client.ui.multidoc;

import org.pml.gnd.gwt.client.model.Document;

import com.google.gwt.query.client.css.RGBColor;

public class LegendItem {

	private final Document document;
	private final RGBColor color;

	public LegendItem(Document document, RGBColor color) {
		this.document = document;
		this.color = color;
	}

	public Document getDocument() {
		return document;
	}

	public RGBColor getColor() {
		return color;
	}

}
