package org.pml.gnd.gwt.client.ui.multidoc;

import java.util.Date;

import org.pml.gnd.gwt.client.handlers.MultidocHandlers;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.ui.map.OpenStreetMap;
import org.pml.gnd.gwt.client.ui.widget.DateRangeInput;
import org.pml.gnd.gwt.client.ui.widget.DateRangeInput.DateChangeEvent;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.github.gwtbootstrap.client.ui.Badge;
import com.github.gwtbootstrap.client.ui.CellTable;
import com.github.gwtbootstrap.client.ui.constants.BadgeType;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class MultidocMapView extends ViewWithUiHandlers<MultidocHandlers>
		implements MultidocMapPresenter.MyView
{

	public interface Binder extends UiBinder<Widget, MultidocMapView>
	{
	}

	private final Widget widget;

	@Override
	public Widget asWidget()
	{
		return widget;
	}

	@UiField
	FlowPanel toolbar;
	@UiField
	DateRangeInput slider;
	@UiField
	Badge timeLabel;
	@UiField
	Anchor permalink;

	@UiField
	ScrollPanel diarySlot;
	private final CellTable<DiaryEntry> diary = new CellTable<DiaryEntry>(
			5000,
			GWT.<CellTable.SelectableResources> create(CellTable.SelectableResources.class));

	@UiField
	Panel listSlot;
	private final CellList<LegendItem> legend = new CellList<LegendItem>(
			new DocumentCell());

	@UiField
	Panel mapPanel;

	// Map-related fields

	private final OpenStreetMap mapWidget;

	final SingleSelectionModel<DiaryEntry> diarySelectionModel = new SingleSelectionModel<DiaryEntry>();
	final SingleSelectionModel<LegendItem> legendSelectionModel = new SingleSelectionModel<LegendItem>();

	// Lock to prevent view to update when programmatically selecting a diary
	// entry for highlighting
	private boolean diarySelectionLock = false;

	@Inject
	public MultidocMapView(final Binder binder)
	{
		widget = binder.createAndBindUi(this);

		timeLabel.setType(BadgeType.INVERSE);

		initDiary();

		// Init Cell List
		listSlot.add(legend);
		// Add a selection model so we can select cells
		legendSelectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler()
				{

					@Override
					public void onSelectionChange(SelectionChangeEvent event)
					{
						if (getUiHandlers() != null)
							getUiHandlers().onLegendItemSelected(
									legendSelectionModel.getSelectedObject());
					}
				});
		legend.setSelectionModel(legendSelectionModel);

		// Init Map
		mapWidget = new OpenStreetMap();
		mapPanel.add(mapWidget);
	}

	private void initDiary()
	{
		diarySlot.add(diary);

		// TIME
		Cell<Date> timeCell = new DateCell(DateTimeFormat.getFormat("HH:mm"));
		Column<DiaryEntry, Date> timeColumn = new Column<DiaryEntry, Date>(timeCell)
		{

			@Override
			public Date getValue(DiaryEntry object)
			{
				return object.getTime();
			}
		};
		diary.addColumn(timeColumn, "Time");

		// PLATFORM
		Cell<String> platformCell = new TextCell();
		Column<DiaryEntry, String> platformColumn = new Column<DiaryEntry, String>(
				platformCell)
		{

			@Override
			public String getValue(DiaryEntry object)
			{
				return object.getPlatform();
			}
		};
		diary.addColumn(platformColumn, "Platform");

		// COMMENT
		Cell<String> commentCell = new TextCell();
		Column<DiaryEntry, String> commentColumn = new Column<DiaryEntry, String>(
				commentCell)
		{

			@Override
			public String getValue(DiaryEntry object)
			{
				return object.getComment();
			}
		};
		diary.addColumn(commentColumn, "Comment");

		// Add a selection model so we can detect click events
		diarySelectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler()
				{

					@Override
					public void onSelectionChange(SelectionChangeEvent event)
					{
						if (!diarySelectionLock && getUiHandlers() != null)
							getUiHandlers().onDiaryItemSelected(
									diarySelectionModel.getSelectedObject());
						diarySelectionLock = false;
					}
				});
		diary.setSelectionModel(diarySelectionModel);

		diary.addStyleName("diary");
	}

	@UiHandler("recenterButton")
	void onRecenterButtonClick(ClickEvent event)
	{
		mapWidget.recenter();
		legendSelectionModel.clear();
	}

	@UiHandler("measureModeCheckbox")
	void onMeasureModeCheckboxValueChange(ValueChangeEvent<Boolean> event)
	{
		if (event.getValue())
			mapWidget.activateMeasureControl();
		else
			mapWidget.deactivateMeasureControl();
	}

	@UiHandler("slider")
	void onTimeSlide(DateChangeEvent event)
	{
		displayTime(slider.getValue());
		if (getUiHandlers() != null)
			getUiHandlers().onSliderValueChange();
	}

	@Override
	public HasData<DiaryEntry> getDiary()
	{
		return diary;
	}

	@Override
	public HasData<LegendItem> getLegend()
	{
		return legend;
	}

	@Override
	public void drawTrackOnMap(Document document, RGBColor color)
	{
		mapWidget.drawTrack(document, color);
	}

	@Override
	public void centerMapOn(Document document)
	{
		mapWidget.centerMapOn(document);
	}

	@Override
	public void clear()
	{
		mapWidget.clear();
	}

	@Override
	public void setTimeSliderMin(Date minTime)
	{
		slider.setMin(minTime);
	}

	@Override
	public void setTimeSliderMax(Date maxTime)
	{
		slider.setMax(maxTime);
	}

	@Override
	public HasValue<Date> getSlider()
	{
		return slider;
	}

	@Override
	public void highlightTrackTime(String id, Long time)
	{
		mapWidget.highlightTrackTime(id, time);
	}

	@Override
	public void displayTime(Date newTime)
	{
		String toDisplay = DateFormats.DISPLAY_LONG.format(newTime);
		timeLabel.setText(toDisplay);
		toolbar.setVisible(true);
	}

	@Override
	public void showDiaryEntry(int rowIndex)
	{
		TableRowElement row = diary.getRowElement(rowIndex);
		diarySlot.setVerticalScrollPosition(row.getOffsetTop());
	}

	@Override
	public void highlightDiaryEntry(DiaryEntry toHighlight)
	{
		diarySelectionLock = true;
		diarySelectionModel.setSelected(toHighlight, true);
	}

	@Override
	public Date getTimeSliderMin()
	{
		return slider.getMin();
	}

	@UiHandler("permalink")
	void onPermalinkClick(ClickEvent e)
	{
		if (getUiHandlers() != null)
			getUiHandlers().onPermalinkClick();
	}

}
