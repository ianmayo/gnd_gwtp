package org.pml.gnd.gwt.client.ui.multidoc;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.event.ItemRemovedFromCartEvent;
import org.pml.gnd.gwt.client.handlers.MultidocHandlers;
import org.pml.gnd.gwt.client.model.DataEntry;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.ShoppingCart;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.ui.main.MainPresenter;
import org.pml.gnd.gwt.client.ui.search.PermalinkDialogPresenter;
import org.pml.gnd.gwt.client.util.BaseAsyncCallback;
import org.pml.gnd.gwt.client.util.StringUtils;

import com.google.common.collect.Lists;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealRootPopupContentEvent;

public class MultidocMapPresenter extends
		Presenter<MultidocMapPresenter.MyView, MultidocMapPresenter.MyProxy>
		implements MultidocHandlers, ItemRemovedFromCartEvent.Handler
{

	@ProxyCodeSplit
	@NameToken(NameTokens.multidocmap)
	public interface MyProxy extends ProxyPlace<MultidocMapPresenter>
	{
	}

	@Override
	protected void revealInParent()
	{
		RevealContentEvent.fire(this, MainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends View, HasUiHandlers<MultidocHandlers>
	{

		void clear();

		HasData<DiaryEntry> getDiary();

		HasData<LegendItem> getLegend();

		void drawTrackOnMap(Document document, RGBColor color);

		void centerMapOn(Document document);

		void setTimeSliderMin(Date minTime);

		Date getTimeSliderMin();

		void setTimeSliderMax(Date maxTime);

		HasValue<Date> getSlider();

		void highlightTrackTime(String id, Long time);

		void displayTime(Date newTime);

		void showDiaryEntry(int rowIndex);

		void highlightDiaryEntry(DiaryEntry toHighlight);

	}

	private final DataStore dataStore;
	private final ShoppingCart shoppingCart;
	private final ListDataProvider<DiaryEntry> narratives = new ListDataProvider<DiaryEntry>();
	private final ListDataProvider<LegendItem> tracks = new ListDataProvider<LegendItem>();
	private final Provider<PermalinkDialogPresenter> permalinkDialog;
	private String permalinkUrl;

	private ColorStack colorStack = new ColorStack();

	private Date minTime;
	private Date maxTime;

	private int diarySelectionRequestID = 0;

	private final PlaceManager placeManager;

	public static final String ID = "id";

	@Inject
	public MultidocMapPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			DataStore dataStore, ShoppingCart shoppingCart,
			PlaceManager placeManager,
			Provider<PermalinkDialogPresenter> searchPermalinkDialogPresenter)
	{
		super(eventBus, view, proxy);
		getView().setUiHandlers(this);
		this.dataStore = dataStore;
		this.shoppingCart = shoppingCart;
		this.placeManager = placeManager;
		narratives.addDataDisplay(getView().getDiary());
		tracks.addDataDisplay(getView().getLegend());
		this.permalinkDialog = searchPermalinkDialogPresenter;

	}

	@Override
	protected void onBind()
	{
		super.onBind();
		addRegisteredHandler(ItemRemovedFromCartEvent.TYPE, this);
	}

	@Override
	protected void onReset()
	{
		super.onReset();
		reset();
	}

	private void reset()
	{
		tracks.getList().clear();
		colorStack = new ColorStack();
		getView().clear();
		String idValue = placeManager.getCurrentPlaceRequest().getParameter(ID, "");

		List<String> items = Lists.newArrayList();

		if (idValue != null && StringUtils.isNonBlank(idValue))
		{
			items = Lists.newArrayList(idValue.split(","));

		} // TODO Remove when this feature is complete
		else if (shoppingCart.isEmpty())
		{
			items = Lists.newArrayList();
			items.add("4d1726ed5cbaaa646a72f055f002bd0c");
			items.add("4d1726ed5cbaaa646a72f055f00286b9");
			items.add("4d1726ed5cbaaa646a72f055f0079ce3");
			items.add("4d1726ed5cbaaa646a72f055f011812c");
			items.add("19498aded85a47324ae5cfdfc3009cd7");
			items.add("19498aded85a47324ae5cfdfc302d36f");
		}
		else
		{
			for (ResultTableElement element : shoppingCart.listItems())
			{
				items.add(element.getId());
			}
		}
		dataStore.getDocumentsByIds(items, new DocumentLoadedCallback());
		updatePermalink(items);
	}

	private void updatePermalink(List<String> items)
	{
		StringBuffer url = new StringBuffer();
		url.append(Window.Location.getProtocol() + "//" + Window.Location.getHost()
				+ Window.Location.getPath() + "#" + NameTokens.multidocmap + ";" + ID
				+ "=");
		for (String id : items)
		{
			url.append(id).append(",");
		}
		
		this.permalinkUrl = url.substring(0, url.length() - 1).toString();
	}

	protected void displayDocumentData(Document document)
	{
		expandTimeSliderBoundsToInclude(document);
		if (document.getMetadata().isNarrative())
		{
			narratives.getList().addAll(DiaryEntry.listAll(document));
			Collections.sort(narratives.getList());
		}
		if (document.getMetadata().isTrack())
		{
			RGBColor color = colorStack.pop();
			tracks.getList().add(new LegendItem(document, color));
			getView().drawTrackOnMap(document, color);
		}
		getView().highlightTrackTime(document.getMetadata().getId(),
				minTime.getTime());
		getView().displayTime(minTime);
	}

	private void expandTimeSliderBoundsToInclude(Document document)
	{
		if (document.getMetadata().listDataTypes().contains(Document.TIME))
		{
			Iterator<DataEntry> i = document.timeIterator();
			Date startTime = i.next().getTime();
			Date endTime = document.getLastTimeMeasurement();
			// Ignoring dates before year 2000
			while (startTime.getTime() < 946681200000l)
				startTime = i.next().getTime();
			if (minTime == null || startTime.compareTo(minTime) < 0)
			{
				minTime = startTime;
				getView().setTimeSliderMin(minTime);
			}
			if (maxTime == null || endTime.compareTo(maxTime) > 0)
			{
				maxTime = endTime;
				getView().setTimeSliderMax(maxTime);
			}
		}
	}

	@Override
	public void onLegendItemSelected(LegendItem selectedObject)
	{
		if (selectedObject != null)
			getView().centerMapOn(selectedObject.getDocument());
	}

	private class DocumentLoadedCallback extends BaseAsyncCallback<Document>
	{

		@Override
		public void onSuccess(Document result)
		{
			displayDocumentData(result);
		}
	}

	@SuppressWarnings("unused")
	private class DocumentsLoadedCallback extends
			BaseAsyncCallback<List<Document>>
	{

		@Override
		public void onSuccess(List<Document> result)
		{
			for (Document document : result)
			{
				displayDocumentData(document);
			}
		}
	}

	@Override
	public void onItemRemovedFromCart(ItemRemovedFromCartEvent event)
	{
		reset();
	}

	private void highlightAllTracks(Long time)
	{
		for (LegendItem track : tracks.getList())
		{
			getView().highlightTrackTime(track.getDocument().getMetadata().getId(),
					time);
		}
	}

	private void showNearestDiaryEntry(Date time)
	{
		// Find nearest entry
		List<DiaryEntry> entries = narratives.getList();
		if (entries.isEmpty())
			return; // No entry to show
		DiaryEntry nearest = entries.get(0);
		int x = 0;
		for (int i = 0; i < entries.size(); i++)
		{
			DiaryEntry next = entries.get(i);
			if (isNearer(next, time.getTime(), nearest))
			{
				nearest = next;
				x = i;
			}
		}
		// Select entry
		getView().showDiaryEntry(x);
		startSelectionTimer(nearest, ++diarySelectionRequestID);
	}

	private void startSelectionTimer(final DiaryEntry toHighlight,
			final int requestID)
	{
		Timer timer = new Timer()
		{

			@Override
			public void run()
			{
				if (requestID == diarySelectionRequestID)
				{
					getView().highlightDiaryEntry(toHighlight);
				}
			}
		};
		timer.schedule(500);
	}

	private boolean isNearer(DiaryEntry entry, Long to, DiaryEntry than)
	{
		Long time = entry.getTime().getTime();
		Long thanTime = than.getTime().getTime();
		if (time < to && to < thanTime)
			return true;
		else if (thanTime < to && to < time)
			return false;
		Long diff = Math.abs(time - to);
		Long thanDiff = Math.abs(thanTime - to);
		return diff < thanDiff;
	}

	@Override
	public void onSliderValueChange()
	{
		Date time = getView().getSlider().getValue();
		highlightAllTracks(time.getTime());
		showNearestDiaryEntry(time);
	}

	@Override
	public void onDiaryItemSelected(DiaryEntry selectedObject)
	{
		Date newTime = selectedObject.getTime();
		highlightAllTracks(newTime.getTime());
		getView().getSlider().setValue(newTime);
		getView().displayTime(newTime);
	}

	@Override
	public void onPermalinkClick()
	{
		PermalinkDialogPresenter dialog = permalinkDialog.get();
		dialog.showPermalink(permalinkUrl);
		RevealRootPopupContentEvent.fire(this, dialog);
	}
}
