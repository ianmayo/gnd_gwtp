package org.pml.gnd.gwt.client.ui.map;

import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.geometry.Point;

import com.google.gwt.json.client.JSONArray;

public class MapUtil {

	public static final Projection DEFAULT_PROJECTION = new Projection("EPSG:4326");

	public static Point[] jsonToPoints(JSONArray latArray, JSONArray lonArray, Projection mapProjection) {
		Point[] points = new Point[latArray.size()];
		for (int i = 0; i < latArray.size(); i++) {
			double lon = lonArray.get(i).isNumber().doubleValue();
			double lat = latArray.get(i).isNumber().doubleValue();
			Point point = new Point(lon, lat);
			// transform point to OSM coordinate system
			point.transform(DEFAULT_PROJECTION, mapProjection);
			points[i] = point;
		}
		return points;
	}
}
