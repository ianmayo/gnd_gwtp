package org.pml.gnd.gwt.client.ui.map;

import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapWidget;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.control.Measure;
import org.gwtopenmaps.openlayers.client.control.MeasureOptions;
import org.gwtopenmaps.openlayers.client.control.MousePosition;
import org.gwtopenmaps.openlayers.client.control.OverviewMap;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.event.MeasureEvent;
import org.gwtopenmaps.openlayers.client.event.MeasureListener;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.handler.PathHandler;
import org.gwtopenmaps.openlayers.client.layer.OSM;
import org.gwtopenmaps.openlayers.client.layer.OSMOptions;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Track;

import com.google.gwt.i18n.client.Dictionary;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.user.client.Window;

public class OpenStreetMap extends MapWidget
{

	private final Projection mapProjection;

	private final TrackLayer trackLayer = new TrackLayer();

	private Measure measureControl;

	public OpenStreetMap()
	{
		super("100%", "100%", options());
		addBaseLayer();
		addTrackLayer();
		mapProjection = new Projection(getMap().getProjection());
		initControls();
	}

	private static MapOptions options()
	{
		MapOptions defaultMapOptions = new MapOptions();
		defaultMapOptions.setNumZoomLevels(20);
		return defaultMapOptions;
	}

	private void addBaseLayer()
	{
		final OSMOptions options = new OSMOptions();
		options.setNumZoomLevels(19);
		options.crossOriginFix();
		
		OSM theBaseLayer = null;

		// see if there is a configured OSM source. Note: GWT throws an
		// exception if the key
		// isn't found - s
		try
		{
			String URL = Dictionary.getDictionary("URL").get("OSM_URL");
			theBaseLayer = new OSM("Tiled Maps", URL, options);
		}
		catch (java.util.MissingResourceException re)
		{
			theBaseLayer = OSM.Mapnik("Mapnik");
		}

		if (theBaseLayer != null)
		{
			theBaseLayer.setIsBaseLayer(true);
			getMap().addLayer(theBaseLayer);
		}
	}

	private void addTrackLayer()
	{
		getMap().addLayer(trackLayer);
	}

	private void initControls()
	{
		getMap().addControl(new OverviewMap());
		getMap().addControl(new ScaleLine());
		getMap().addControl(new MousePosition());
		initMeasureControl();
	}

	private void initMeasureControl()
	{
		MeasureOptions measOpts = new MeasureOptions();
		measOpts.setGeodesic(true); // earth is not a cylinder
		measureControl = new Measure(new PathHandler(), measOpts);
		getMap().addControl(measureControl);

		measureControl.addMeasureListener(new MeasureListener()
		{

			@Override
			public void onMeasure(MeasureEvent eventObject)
			{
				Window.alert("Measured distance is " + eventObject.getMeasure()
						+ " " + eventObject.getUnits());
			}
		});
	}

	public void recenter()
	{
		getMap().zoomToExtent(trackLayer.getDataExtent());
	}

	public void activateMeasureControl()
	{
		measureControl.activate();
	}

	public void deactivateMeasureControl()
	{
		measureControl.deactivate();
	}

	public void centerMapOn(Document document)
	{
		getMap().zoomToExtent(
				new LineString(MapUtil.jsonToPoints(lat(document),
						lon(document), mapProjection)).getBounds());
	}

	private JSONArray lat(Document document)
	{
		return document.getMeasurement(Track.LATITUDE);
	}

	private JSONArray lon(Document document)
	{
		return document.getMeasurement(Track.LONGITUDE);
	}

	public void clear()
	{
		trackLayer.destroyFeatures();
	}

	public void drawTrack(Document document, RGBColor color)
	{
		trackLayer.drawTrack(document, color, mapProjection);
		recenter();
	}

	public void highlightTrackTime(String id, Long time)
	{
		trackLayer.highlightTrackTime(id, time);
	}
}
