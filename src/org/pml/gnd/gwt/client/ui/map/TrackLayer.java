package org.pml.gnd.gwt.client.ui.map;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.pml.gnd.gwt.client.model.DataEntry;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.Track;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.gwt.query.client.css.RGBColor;

public class TrackLayer extends Vector {

	private final Style pointStyle;
	private final Multimap<String, VectorFeature> trackPoints = ArrayListMultimap.create();
	private final Multimap<String, VectorFeature> highlightedFeatures = ArrayListMultimap.create();

	public TrackLayer() {
		super("Marker layer");
		pointStyle = pointStyle();
	}

	private Style pointStyle() {
		Style style = new Style();
		style.setFillColor("lightGray");
		style.setStrokeColor("black");
		style.setStrokeWidth(1);
		style.setPointRadius(2);
		style.setGraphicName("square");
		return style;
	}

	public void drawTrack(Document document, RGBColor color, Projection mapProjection) {
		List<Point> points = Lists.newArrayList();
		List<VectorFeature> pointFeatures = Lists.newArrayList();

		// Create Point VectorFeatures
		for (Iterator<DataEntry> iterator = document.trackDataIterator(); iterator.hasNext();) {
			DataEntry entry = iterator.next();
			Date time = entry.getTime();
			
			Double lon = entry.getMeasurementAsDouble(Track.LONGITUDE);
			Double lat = entry.getMeasurementAsDouble(Track.LATITUDE);
			Point point = new Point(lon, lat);
			// transform point to OSM coordinate system
			point.transform(MapUtil.DEFAULT_PROJECTION, mapProjection);
			points.add(point);

			VectorFeature pointFeature = new VectorFeature(point, pointStyle);
			pointFeature.getAttributes().setAttribute(Document.TIME, time.getTime());
			pointFeatures.add(pointFeature);

		}
		// TODO Check ID not null
		trackPoints.putAll(document.getMetadata().getId(), pointFeatures);

		// Display lines
		LineString lines = new LineString(points.toArray(new Point[points.size()]));
		addFeature(new VectorFeature(lines, lineStyle(color)));

		// Display markers
		for (VectorFeature pointFeature : pointFeatures)
			addFeature(pointFeature);
	}

	/**
	 * @param trackId
	 *            the ID returned by Document.getMetadata().getId()
	 */
	public void highlightTrackTime(String trackId, final Long targetTime) {
		// De-highlight
		for (VectorFeature feature : highlightedFeatures.get(trackId))
			feature.setStyle(pointStyle);
		highlightedFeatures.get(trackId).clear();

		Collection<VectorFeature> c = trackPoints.get(trackId);
		Style style = pointStyle();
		style.setFillColor("black");
		style.setStrokeColor("black");
		style.setStrokeWidth(1.5);
		style.setPointRadius(6);
		style.setGraphicName("x");
		style.setFill(true);
		Iterator<VectorFeature> i = Collections2.filter(c, new Predicate<VectorFeature>() {

			@Override
			public boolean apply(@Nullable VectorFeature feature) {
				Long time = (long) feature.getAttributes().getAttributeAsDouble("time");
				return time > targetTime;
			}
		}).iterator();
		if (i.hasNext()) {
			VectorFeature feature = i.next();
			feature.setStyle(style);
			highlightedFeatures.put(trackId, feature);
		}
		redraw();
	}

	private Style lineStyle(RGBColor color) {
		Style style = new Style();
		style.setStrokeColor(color.getCssName());
		style.setStrokeWidth(2);
		return style;
	}

}
