package org.pml.gnd.gwt.client.ui.main;

import java.util.Iterator;

import org.bitbucket.es4gwt.client.result.ResultTerm;
import org.bitbucket.es4gwt.client.result.SearchResult;
import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.model.CouchView;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.model.DocumentList;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.search.SearchService;
import org.pml.gnd.gwt.client.util.BaseAsyncCallback;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;

public class HomePresenter extends Presenter<HomePresenter.MyView, HomePresenter.MyProxy> {

	@ProxyStandard
	@NameToken(NameTokens.home)
	public interface MyProxy extends ProxyPlace<HomePresenter> {
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends View {

		void addTrial(ResultTerm trial, String targetHistoryToken);

		void addRecentDocument(org.pml.gnd.gwt.client.model.CouchView document, String targetHistoryToken);

	}

	private final DataStore datastore;
	private final SearchService searchService;
	private final PlaceManager placeManager;

	@Inject
	public HomePresenter(EventBus eventBus, MyView view, MyProxy proxy, DataStore datastore,
			SearchService searchService, PlaceManager placeManager) {
		super(eventBus, view, proxy);
		this.datastore = datastore;
		this.searchService = searchService;
		this.placeManager = placeManager;
	}

	@Override
	protected void onReveal() {
		super.onReveal();
		loadTrials();
		loadRecentDocuments();
	}

	private void loadTrials() {
		searchService.trialFacet(new BaseAsyncCallback<SearchResult>() {

			@Override
			public void onSuccess(SearchResult result) {
				Iterator<ResultTerm> i = result.getFacet(Facet.TRIAL).iterator();
				while (i.hasNext()) {
					ResultTerm trial = i.next();
					PlaceRequest placeRequest = new PlaceRequest(NameTokens.search).with(	Facet.TRIAL.name(),
																							trial.getTerm());
					getView().addTrial(trial, placeManager.buildHistoryToken(placeRequest));
				}
			}
		});
	}

	private void loadRecentDocuments() {
		datastore.getRecentDocuments(new BaseAsyncCallback<Document>() {

			@Override
			public void onSuccess(Document result) {
				Iterator<Document> i = ((DocumentList) result).iterator();
				while (i.hasNext()) {
					CouchView doc = (CouchView) i.next();
					PlaceRequest placeRequest = new PlaceRequest(NameTokens.info).with("id", doc.getId());
					getView().addRecentDocument(doc, placeManager.buildHistoryToken(placeRequest));
				}
			}
		});
	}
}
