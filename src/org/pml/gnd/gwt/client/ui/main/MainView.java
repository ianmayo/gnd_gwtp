package org.pml.gnd.gwt.client.ui.main;

import org.pml.gnd.gwt.client.handlers.MainHandlers;

import com.github.gwtbootstrap.client.ui.Form.SubmitEvent;
import com.github.gwtbootstrap.client.ui.Form.SubmitHandler;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.NavSearch;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class MainView extends ViewWithUiHandlers<MainHandlers> implements MainPresenter.MyView {

	public interface Binder extends UiBinder<Widget, MainView> {
	}

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiField Panel contentSlot;

	@UiField Icon cartIcon;
	@UiField NavLink cartItemCount;
	private boolean cartLinksEnabled = false;

	@UiField NavSearch navSearch;

	@Inject
	public MainView(final Binder binder) {
		widget = binder.createAndBindUi(this);
		navSearch.addSubmitHandler(new SubmitHandler() {

			@Override
			public void onSubmit(SubmitEvent event) {
				if (getUiHandlers() != null)
					getUiHandlers().onSearchSubmit();
			}
		});
	}

	@Override
	public void setInSlot(Object slot, Widget content) {
		if (slot == MainPresenter.CONTENT_SLOT) {
			contentSlot.clear();
			contentSlot.add(content);
		} else
			super.setInSlot(slot, content);
	}

	@UiHandler("shoppingCartIcon")
	void onCartIconClick(ClickEvent event) {
		handleCartClick();
	}

	@UiHandler("cartItemCount")
	void onCartLinkClick(ClickEvent event) {
		handleCartClick();
	}

	private void handleCartClick() {
		if (cartLinksEnabled && getUiHandlers() != null)
			getUiHandlers().onShoppingCartIconClick();
	}

	public static int getUsedHeight() {
		return 93;
	}

	@Override
	public HasText getCartItemCount() {
		return cartItemCount;
	}

	@Override
	public void enableCartLinks() {
		cartLinksEnabled = true;
	}

	@Override
	public void disableCartLinks() {
		cartLinksEnabled = false;
	}

	@Override
	public HasText getSearchBox() {
		return navSearch.getTextBox();
	}

}