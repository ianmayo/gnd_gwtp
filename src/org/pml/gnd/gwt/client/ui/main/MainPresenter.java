package org.pml.gnd.gwt.client.ui.main;

import org.pml.gnd.gwt.client.event.ItemRemovedFromCartEvent;
import org.pml.gnd.gwt.client.event.ItemsAddedToCartEvent;
import org.pml.gnd.gwt.client.handlers.MainHandlers;
import org.pml.gnd.gwt.client.model.ShoppingCart;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.ui.cart.ManageCartPresenter;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.ui.HasText;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;
import com.gwtplatform.mvp.client.proxy.RevealRootLayoutContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealRootPopupContentEvent;

public class MainPresenter extends Presenter<MainPresenter.MyView, MainPresenter.MyProxy>
		implements MainHandlers, ItemsAddedToCartEvent.Handler, ItemRemovedFromCartEvent.Handler {

	@ProxyStandard
	public interface MyProxy extends Proxy<MainPresenter> {
	}

	@Override
	protected void revealInParent() {
		RevealRootLayoutContentEvent.fire(this, this);
	}

	public interface MyView extends View, HasUiHandlers<MainHandlers> {

		HasText getCartItemCount();

		void enableCartLinks();

		void disableCartLinks();

		HasText getSearchBox();

	}

	@ContentSlot public static final Type<RevealContentHandler<?>> CONTENT_SLOT = new Type<RevealContentHandler<?>>();

	private final PlaceManager placeManager;

	private final ShoppingCart shoppingCart;
	private final Provider<ManageCartPresenter> manageCartPresenter;

	@Inject
	public MainPresenter(EventBus eventBus, MyView view, MyProxy proxy, PlaceManager placeManager,
			ShoppingCart shoppingCart, Provider<ManageCartPresenter> manageCartPresenter) {
		super(eventBus, view, proxy);
		getView().setUiHandlers(this);
		this.placeManager = placeManager;
		this.shoppingCart = shoppingCart;
		this.manageCartPresenter = manageCartPresenter;
	}

	@Override
	protected void onBind() {
		super.onBind();
		addRegisteredHandler(ItemsAddedToCartEvent.TYPE, this);
		addRegisteredHandler(ItemRemovedFromCartEvent.TYPE, this);
	}

	@Override
	public void onShoppingCartIconClick() {
		if (!shoppingCart.isEmpty())
			RevealRootPopupContentEvent.fire(this, manageCartPresenter.get());
	}

	@Override
	public void onItemsAddedToCart(ItemsAddedToCartEvent event) {
		refreshDisplay();
	}

	@Override
	public void onItemRemovedFromCart(ItemRemovedFromCartEvent event) {
		refreshDisplay();
	}

	private void refreshDisplay() {
		if (shoppingCart.isEmpty()) {
			getView().getCartItemCount().setText("Empty");
		} else if (shoppingCart.countItems() == 1) {
			getView().getCartItemCount().setText("1 item");
		} else {
			getView().getCartItemCount().setText(shoppingCart.countItems() + " items");
		}
		if (shoppingCart.isEmpty())
			getView().disableCartLinks();
		else
			getView().enableCartLinks();
	}

	@Override
	public void onSearchSubmit() {
		placeManager.revealPlace(new PlaceRequest(NameTokens.search).with("TEXT", getView().getSearchBox().getText()));
	}
}
