package org.pml.gnd.gwt.client.ui.main;

import org.bitbucket.es4gwt.client.result.ResultTerm;
import org.pml.gnd.gwt.client.model.CouchView;

import com.github.gwtbootstrap.client.ui.NavList;
import com.github.gwtbootstrap.client.ui.base.ListItem;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class HomeView extends ViewImpl implements HomePresenter.MyView {

	public interface Binder extends UiBinder<Widget, HomeView> {
	}

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiField NavList recentDocumentList;
	@UiField NavList trialList;

	@Inject
	public HomeView(final Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	@Override
	public void addTrial(ResultTerm trial, String targetHistoryToken) {
		trialList.add(new ListItem(new Hyperlink(trial.getTerm(), targetHistoryToken)));
	}

	@Override
	public void addRecentDocument(CouchView document, String targetHistoryToken) {
		recentDocumentList.add(new ListItem(new Hyperlink(document.getValue(), targetHistoryToken)));
	}
}
