package org.pml.gnd.gwt.client.ui.cart;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.bitbucket.es4gwt.shared.SearchConstants;
import org.pml.gnd.gwt.client.handlers.ManageCartHandlers;

import com.github.gwtbootstrap.client.ui.CellTable;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.PopupViewWithUiHandlers;

public class ManageCartView extends PopupViewWithUiHandlers<ManageCartHandlers> implements ManageCartPresenter.MyView {

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	public interface Binder extends UiBinder<Widget, ManageCartView> {
	}

	@UiField(provided = true) SimplePanel tableContainer = new SimplePanel();
	CellTable<ResultTableElement> table = createTable();

	@Inject
	public ManageCartView(final EventBus eventBus, final Binder binder) {
		super(eventBus);
		widget = binder.createAndBindUi(this);
		tableContainer.add(table);
	}

	private CellTable<ResultTableElement> createTable() {

		CellTable<ResultTableElement> table = new CellTable<ResultTableElement>();
		table.setPageSize(SearchConstants.MAX_RESULTS);

		// ///////
		// REF //
		// /////
		TextCell refCell = new TextCell();
		Column<ResultTableElement, String> refColumn = new Column<ResultTableElement, String>(refCell) {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getId();
			}
		};
		table.addColumn(refColumn, "Ref");

		// ////////
		// NAME //
		// //////
		TextCell nameCell = new TextCell();
		Column<ResultTableElement, String> nameColumn = new Column<ResultTableElement, String>(nameCell) {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getName();
			}
		};
		table.addColumn(nameColumn, "Name");

		// //////////
		// REMOVE //
		// ////////
		RemoveCell removeCell = new RemoveCell();
		Column<ResultTableElement, String> removeColumn = new Column<ResultTableElement, String>(removeCell) {

			@Override
			public String getValue(ResultTableElement object) {
				return "";
			}
		};
		removeColumn.setFieldUpdater(new FieldUpdater<ResultTableElement, String>() {

			@Override
			public void update(int index, ResultTableElement object, String value) {
				getUiHandlers().removeItemButtonClicked(object);
			}
		});
		removeColumn.setCellStyleNames("embedded-link");
		RemoveAllHeader header = new RemoveAllHeader();
		header.setHeaderStyleNames("link");
		table.addColumn(removeColumn, header);

		return table;
	}

	private class RemoveAllHeader extends Header<String> {

		public RemoveAllHeader() {
			super(new RemoveCell());
			setUpdater(new ValueUpdater<String>() {

				@Override
				public void update(String value) {
					if (Window.confirm("Are you sure you want to remove all items from cart ?"))
						getUiHandlers().removeAllConfirmed();
				}
			});
		}

		@Override
		public String getValue() {
			return "";
		}
	};

	@Override
	public HasData<ResultTableElement> asDataDisplay() {
		return table;
	}

	@UiHandler("viewButton")
	void onViewButtonClick(ClickEvent event) {
		if (getUiHandlers() != null) {
			getUiHandlers().onViewButtonClick();
		}
	}

}
