package org.pml.gnd.gwt.client.ui.cart;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.pml.gnd.gwt.client.handlers.ManageCartHandlers;
import org.pml.gnd.gwt.client.model.ShoppingCart;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.google.common.collect.Lists;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PopupView;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;

public class ManageCartPresenter extends PresenterWidget<ManageCartPresenter.MyView> implements ManageCartHandlers {

	public interface MyView extends PopupView, HasUiHandlers<ManageCartHandlers> {

		HasData<ResultTableElement> asDataDisplay();

	}

	private final PlaceManager placeManager;
	private final ShoppingCart shoppingCart;
	private final ListDataProvider<ResultTableElement> cartItems = new ListDataProvider<ResultTableElement>();

	@Inject
	public ManageCartPresenter(EventBus eventBus, MyView view, PlaceManager placeManager, ShoppingCart shoppingCart) {
		super(eventBus, view);
		getView().setUiHandlers(this);
		this.placeManager = placeManager;
		this.shoppingCart = shoppingCart;
		cartItems.addDataDisplay(getView().asDataDisplay());
		cartItems.setList(Lists.newArrayList(shoppingCart.listItems()));
	}

	@Override
	public void removeItemButtonClicked(ResultTableElement item) {
		cartItems.getList().remove(item);
		shoppingCart.remove(item);
		if (shoppingCart.isEmpty())
			getView().hide();
	}

	@Override
	public void removeAllConfirmed() {
		shoppingCart.clear();
		getView().hide();
	}

	@Override
	public void onViewButtonClick() {
		placeManager.revealPlace(new PlaceRequest(NameTokens.multidocmap));
		getView().hide();
	}
}
