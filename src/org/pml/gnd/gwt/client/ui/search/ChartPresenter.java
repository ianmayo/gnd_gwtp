package org.pml.gnd.gwt.client.ui.search;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.es4gwt.client.result.SearchResult;
import org.pml.gnd.gwt.client.datastore.DataStore;
import org.pml.gnd.gwt.client.handlers.ChartHandlers;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.ui.multidoc.ColorStack;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.proxy.PlaceManager;

public class ChartPresenter extends PresenterWidget<ChartPresenter.MyView>
		implements ChartHandlers
{

	public interface MyView extends View, HasUiHandlers<ChartHandlers>
	{

		void drawTrackOnMap(Document document, RGBColor rgbColor);

		void clearMap();

	}

	// private final EventBus localEventBus = new SimpleEventBus();

	private final PlaceManager placeManager;
	private final DataStore dataStore;

	@Inject
	public ChartPresenter(final EventBus eventBus, final MyView view,
			PlaceManager placeManager, DataStore dataStore)
	{
		super(eventBus, view);
		getView().setUiHandlers(this);
		this.placeManager = placeManager;
		this.dataStore = dataStore;
	}

	@Override
	public void addToShoppingCart(String id)
	{
		// TODO Implementation

	}

	public void display(SearchResult lastResult)
	{
		getView().clearMap();
		
		final ColorStack colorStack = new ColorStack();

		List<String> keys = new ArrayList<String>();

		for (int i = 0; i < lastResult.getHitCount(); i++)
		{
			keys.add(lastResult.getHit(i).getId());
		}

		dataStore.getDocumentsByIds(keys, new AsyncCallback<Document>()
		{

			@Override
			public void onSuccess(Document document)
			{
				getView().drawTrackOnMap(document, colorStack.pop());
			}

			@Override
			public void onFailure(Throwable caught)
			{
				// Window.alert(caught.getMessage() + " \n " + caught.getStackTrace());
			}
		});

	}

}
