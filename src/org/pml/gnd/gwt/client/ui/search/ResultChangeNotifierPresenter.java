package org.pml.gnd.gwt.client.ui.search;

import org.bitbucket.es4gwt.client.result.SearchResultDiff;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.pml.gnd.gwt.client.event.SearchResultUpdateRequestedEvent;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;

public class ResultChangeNotifierPresenter extends PresenterWidget<ResultChangeNotifierPresenter.MyView> {

	public interface MyView extends View {

		HasText getAddedMessageLabel();

		HasText getRemovedMessageLabel();

		HasClickHandlers getLink();

		HasVisibility getPanel();
	}

	private SearchRequest request;
	private SearchResultDiff resultDiff;

	@Inject
	public ResultChangeNotifierPresenter(final EventBus eventBus, final MyView view) {
		super(eventBus, view);
		getView().getPanel().setVisible(false);
		getView().getLink().addClickHandler(new ShowChangesClickHandler());
	}

	@Override
	protected void onBind() {
		super.onBind();
	}

	public void clear() {
		getView().getAddedMessageLabel().setText(null);
		getView().getRemovedMessageLabel().setText(null);
		getView().getPanel().setVisible(false);
	}

	public void notify(SearchRequest request, SearchResultDiff resultDiff) {
		this.request = request;
		this.resultDiff = resultDiff;
		clear();
		if (resultDiff.hasAdded())
			notifyAdded(resultDiff.added().size());
		if (resultDiff.hasRemoved())
			notifiyRemoved(resultDiff.removed().size());
	}

	private void notifyAdded(int i) {
		getView().getAddedMessageLabel().setText("Added results : " + i);
		getView().getPanel().setVisible(true);
	}

	private void notifiyRemoved(int i) {
		getView().getRemovedMessageLabel().setText("Removed results : " + i);
		getView().getPanel().setVisible(true);
	}

	private class ShowChangesClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			clear();
			SearchResultUpdateRequestedEvent.fire(getEventBus(), request, resultDiff);
		}

	}

}
