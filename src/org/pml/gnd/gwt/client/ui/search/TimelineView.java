package org.pml.gnd.gwt.client.ui.search;

import java.util.Date;

import org.pml.gnd.gwt.client.handlers.TimelineHandlers;
import org.pml.gnd.gwt.client.ui.widget.Timeline.ITimeLineRender;
import org.pml.gnd.gwt.client.ui.widget.Timeline.SearchResultsRenderer;
import org.pml.gnd.gwt.client.ui.widget.Timeline.TimeLineWidget;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class TimelineView extends ViewWithUiHandlers<TimelineHandlers>
		implements TimelinePresenter.MyView
{

	ITimeLineRender render;
	TimeLineWidget simileWidget;
	FlowPanel panel;
	private Button recenterButton;
	private ClickHandler onRecenterButtonClick = new ClickHandler()
	{
		
		@Override
		public void onClick(ClickEvent event)
		{
			getUiHandlers().recreate();
		}
	};


	public TimelineView()
	{
		panel = new FlowPanel();
		panel.setSize("100%", "100%");
	}

	@Override
	public Widget asWidget()
	{
		return panel;
	}

	@Override
	public void loadJSON(JSONObject obj)
	{
		simileWidget.loadJson(obj.toString());
		simileWidget.layout();
	}

	@Override
	public void initTimeline(Date maxDate, Date minDate)
	{
		panel.clear();
		render = new SearchResultsRenderer(minDate, maxDate);
		simileWidget = new TimeLineWidget(panel.getOffsetHeight() + "px",
				panel.getOffsetWidth() + "px", render, this);

		recenterButton= new Button("Recenter", onRecenterButtonClick );
		recenterButton.addStyleName("recenter");
		
		panel.add(recenterButton);
		panel.add(simileWidget);

	}

	@Override
	public void hideBubbles()
	{
		simileWidget.clearBubbles();
	}

	@Override
	public void callback(JavaScriptObject element)
	{
		final com.google.gwt.dom.client.Element el = Element.as(element);
		Anchor anchor = new Anchor("Add to Cart");

		String title = el.getElementsByTagName("a").getItem(0).getString();
		String id = title.substring(title.indexOf("id=") + 3, title.indexOf("\">"));
		anchor.getElement().setId(id);

		final Element anchorElement = anchor.getElement();
		el.appendChild(anchorElement);
		Scheduler.get().scheduleDeferred(new ScheduledCommand()
		{

			@Override
			public void execute()
			{
				final Anchor link = Anchor.wrap(anchorElement);
				link.addClickHandler(new ClickHandler()
				{

					@Override
					public void onClick(ClickEvent event)
					{
						hideBubbles();
						getUiHandlers().addToShoppingCart(link.getElement().getId());
					}
				});
			}
		});
	}
}
