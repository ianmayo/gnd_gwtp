package org.pml.gnd.gwt.client.ui.search;

import org.pml.gnd.gwt.client.ui.widget.CompatibleAlertBlock;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class ResultChangeNotifierView extends ViewImpl implements ResultChangeNotifierPresenter.MyView {

	public interface Binder extends UiBinder<Widget, ResultChangeNotifierView> {
	}

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiField CompatibleAlertBlock alertBlock;
	@UiField Label addedMessageLabel;
	@UiField Label removedMessageLabel;
	@UiField Anchor showChangesLink;

	@Inject
	public ResultChangeNotifierView(Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	@Override
	public HasText getAddedMessageLabel() {
		return addedMessageLabel;
	}

	@Override
	public HasText getRemovedMessageLabel() {
		return removedMessageLabel;
	}

	@Override
	public HasClickHandlers getLink() {
		return showChangesLink;
	}

	@Override
	public HasVisibility getPanel() {
		return alertBlock;
	}

}
