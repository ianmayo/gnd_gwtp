package org.pml.gnd.gwt.client.ui.search;

import org.pml.gnd.gwt.client.handlers.SearchHandlers;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.github.gwtbootstrap.client.ui.NavLink;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class SearchMainView extends ViewWithUiHandlers<SearchHandlers>
		implements SearchMainPresenter.MyView
{

	public interface Binder extends UiBinder<Widget, SearchMainView>
	{
	}

	private final Widget widget;

	@Override
	public Widget asWidget()
	{
		return widget;
	}

	@UiField
	DockLayoutPanel mainPanel;
	@UiField
	Panel northPanel;

	@UiField
	Panel resultChangeNotifierSlot;
	@UiField
	Panel searchFormSlot;
	@UiField
	Panel searchResultTableSlot;

	@UiField
	Label hitCountLabel;
	@UiField
	Button addToCartButton;

	@UiField
	NavLink tableLink;
	@UiField
	NavLink timelineLink;
	@UiField
	NavLink chartLink;

	@Inject
	public SearchMainView(final Binder binder)
	{
		widget = binder.createAndBindUi(this);
	}

	@Override
	public void setInSlot(Object slot, Widget content)
	{
		if (slot == SearchMainPresenter.RESULT_CHANGE_NOTIFIER_SLOT)
		{
			resultChangeNotifierSlot.clear();
			resultChangeNotifierSlot.add(content);
		}
		else if (slot == SearchMainPresenter.SEARCH_FORM_SLOT)
		{
			searchFormSlot.clear();
			searchFormSlot.add(content);
		}
		else if (slot == SearchMainPresenter.SEARCH_RESULT_TABLE_SLOT)
		{
			searchResultTableSlot.clear();
			searchResultTableSlot.add(content);
		}
		else
			super.setInSlot(slot, content);
	}

	@UiHandler("addToCartButton")
	void addToCartButtonPushed(ClickEvent e)
	{
		if (getUiHandlers() != null)
			getUiHandlers().addToCartButtonPushed();
	}

	@UiHandler("tableLink")
	void onTableLinkClick(ClickEvent e)
	{
		deactivateAllTabs();
		tableLink.setActive(true);
		if (getUiHandlers() != null)
			getUiHandlers().onShowTable();
	}
	
	@UiHandler("timelineLink")
	void onTimeLineLinkClick(ClickEvent e)
	{
		deactivateAllTabs();
		timelineLink.setActive(true);
		if (getUiHandlers() != null)
			getUiHandlers().onShowTimeLine();
	}
	
	@UiHandler("chartLink")
	void onChartLinkClick(ClickEvent e)
	{
		deactivateAllTabs();
		chartLink.setActive(true);
		if (getUiHandlers() != null)
			getUiHandlers().onShowChart();
	}

	@Override
	public HasEnabled getAddToCartButton()
	{
		return addToCartButton;
	}

	@Override
	public HasText getHitCountLabel()
	{
		return hitCountLabel;
	}

	@Override
	public void expandHeader()
	{
		mainPanel.setWidgetSize(northPanel, 150.0);
	}

	@Override
	public void shrinkNotifierSlot()
	{
		mainPanel.setWidgetSize(northPanel, 50.0);
	}
	
	@UiHandler("saveSearchLink")
	void onSaveSearchLinkClick(ClickEvent e)
	{
		if (getUiHandlers() != null)
			getUiHandlers().onSaveSearchLinkClick();
	}
	
	private void deactivateAllTabs()
	{
		timelineLink.setActive(false);
		tableLink.setActive(false);
		chartLink.setActive(false);
	}

}
