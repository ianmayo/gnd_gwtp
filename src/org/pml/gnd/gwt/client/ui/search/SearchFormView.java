package org.pml.gnd.gwt.client.ui.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;
import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.pml.gnd.gwt.client.handlers.SearchFormHandlers;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.util.Environment;

import com.google.common.base.Predicate;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.query.client.GQuery;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.watopi.chosen.client.event.ChosenChangeEvent;
import com.watopi.chosen.client.event.ChosenChangeEvent.ChosenChangeHandler;
import com.watopi.chosen.client.gwt.ChosenListBox;

public class SearchFormView extends ViewWithUiHandlers<SearchFormHandlers> implements SearchFormPresenter.MyView {

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	public interface Binder extends UiBinder<Widget, SearchFormView> {
	}

	@UiField TextBox textBox;

	@UiField(provided = true) ChosenListBox platformList = new ChosenListBox(true);
	@UiField(provided = true) ChosenListBox platformTypeList = new ChosenListBox(true);
	@UiField(provided = true) ChosenListBox sensorList = new ChosenListBox(true);
	@UiField(provided = true) ChosenListBox sensorTypeList = new ChosenListBox(true);
	@UiField(provided = true) ChosenListBox trialList = new ChosenListBox(true);
	@UiField ListBox dataTypeFilterMode;
	@UiField(provided = true) ChosenListBox dataTypeList = new ChosenListBox(true);

	@UiField DateBox startDateBox;
	@UiField DateBox endDateBox;

	@Inject
	public SearchFormView(final Binder binder) {
		widget = binder.createAndBindUi(this);
		registerChangeHandlers();
		initDataTypeFilterModeListBox();
		initDateBoxes();
	}

	private void registerChangeHandlers() {
		textBox.addValueChangeHandler(textChangeHandler);
		for (Facet facet : Facet.SEARCH_PARAMS)
			getFilterBox(facet).addChosenChangeHandler(chosenChangeHandler);
		dataTypeFilterMode.addChangeHandler(dataTypeFilterModeChangeHandler);
	}

	private void initDataTypeFilterModeListBox() {
		dataTypeFilterMode.addItem(FilterMode.ANY_OF.toString(), FilterMode.ANY_OF.name());
		dataTypeFilterMode.addItem(FilterMode.ALL_OF.toString(), FilterMode.ALL_OF.name());
	}

	private void initDateBoxes() {
		startDateBox.setFormat(getDefaultDateFormat());
		endDateBox.setFormat(getDefaultDateFormat());
		startDateBox.addValueChangeHandler(dateChangeHandler);
		endDateBox.addValueChangeHandler(dateChangeHandler);
	}

	@Override
	public void clearFilters(Facet facet) {
		ChosenListBox listBox = getFilterBox(facet);
		listBox.clear();
		listBox.update();
	}

	@Override
	public void addFilter(Facet facet, String term, int count) {
		getFilterBox(facet).addItem(term + " (" + count + ")", term);
	}

	@Override
	public void addFilter(ElasticFacet facet, String term) {
		getFilterBox(facet).addItem(term);
	}

	@Override
	public void selectAllFilters(ElasticFacet facet) {
		ChosenListBox listBox = getFilterBox(facet);
		for (int i = 0; i < listBox.getItemCount(); i++)
			listBox.setItemSelected(i, true);
		listBox.update();
	}

	@Override
	public void selectFiltersThat(Facet facet, Predicate<String> predicate) {
		ChosenListBox listBox = getFilterBox(facet);
		for (int i = 0; i < listBox.getItemCount(); i++)
			if (predicate.apply(listBox.getValue(i)))
				listBox.setItemSelected(i, true);
		listBox.update();
	}

	private static final ChosenListBox NULL_LIST = new ChosenListBox() {

		@Override
		public void addItem(String item) {
		}

		@Override
		public boolean isItemSelected(int index) {
			return false;
		}

		@Override
		public void removeItem(int index) {
		}

	};

	private ChosenListBox getFilterBox(ElasticFacet facet) {
		if (facet instanceof Facet)
			switch ((Facet) facet) {
			case PLATFORM_TYPE:
				return platformTypeList;
			case SENSOR:
				return sensorList;
			case SENSOR_TYPE:
				return sensorTypeList;
			case TRIAL:
				return trialList;
			case DATA_TYPE:
				return dataTypeList;
			case PLATFORM:
				if (Environment.isDevMode())
					return NULL_LIST;
				else
					return platformList;
			default:
				throw new IllegalArgumentException("No ListBox for facet " + facet.name());
			}
		throw new IllegalArgumentException("Provided facet should be a GND Facet");
	}

	@Override
	public HasValue<String> getText() {
		return textBox;
	}

	public static DefaultFormat getDefaultDateFormat() {
		return new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_LONG));
	}

	@Override
	public List<String> getSelectedFilters(Facet facet) {
		List<String> terms = new ArrayList<String>();
		ChosenListBox listBox = getFilterBox(facet);
		for (int i = 0; i < listBox.getItemCount(); i++) {
			if (listBox.isItemSelected(i))
				terms.add(listBox.getValue(i));
		}
		return terms;
	}

	@Override
	public FilterMode getDataTypeFilterMode() {
		return FilterMode.valueOf(dataTypeFilterMode.getValue(dataTypeFilterMode.getSelectedIndex()));
	}

	@Override
	public HasValue<Date> getStartDate() {
		return startDateBox;
	}

	@Override
	public HasValue<Date> getEndDate() {
		return endDateBox;
	}

	private final ChosenChangeHandler chosenChangeHandler = new FacetFilterChangeHandler();
	private final DataTypeFilterModeChangeHandler dataTypeFilterModeChangeHandler = new DataTypeFilterModeChangeHandler();
	private final ValueChangeHandler<Date> dateChangeHandler = new DateChangeHandler();
	private final ValueChangeHandler<String> textChangeHandler = new TextChangeHandler();

	private class FacetFilterChangeHandler implements ChosenChangeHandler {

		@Override
		public void onChange(ChosenChangeEvent event) {
			notifyUiHandlers();
			// Hack to solve issue #41
			GQuery.$(Document.get()).click();
		}
	}

	private class DateChangeHandler implements ValueChangeHandler<Date> {

		@Override
		public void onValueChange(ValueChangeEvent<Date> event) {
			notifyUiHandlers();
		}
	}

	private class TextChangeHandler implements ValueChangeHandler<String> {

		@Override
		public void onValueChange(ValueChangeEvent<String> event) {
			notifyUiHandlers();
		}

	}

	private class DataTypeFilterModeChangeHandler implements ChangeHandler {

		@Override
		public void onChange(ChangeEvent event) {
			notifyUiHandlers();
		}

	}

	private void notifyUiHandlers() {
		if (getUiHandlers() != null)
			getUiHandlers().searchParamsChanged();
	}

	@Override
	public void setDataTypeFilterMode(FilterMode filterMode) {
		switch (filterMode) {
		case ANY_OF:
			dataTypeFilterMode.setSelectedIndex(0);
			break;
		case ALL_OF:
			dataTypeFilterMode.setSelectedIndex(1);
			break;
		default:
			throw new IllegalArgumentException(filterMode.name() + " is not supported");
		}
	}

	@Override
	public void updateFilterBox(Facet facet) {
		getFilterBox(facet).update();
	}

	@UiHandler("clearStartDateButton")
	void onClearStartDateButtonClick(ClickEvent e) {
		startDateBox.setValue(null);
		notifyUiHandlers();
	}

	@UiHandler("clearEndDateButton")
	void onClearEndDateButtonClick(ClickEvent e) {
		endDateBox.setValue(null);
		notifyUiHandlers();
	}
}
