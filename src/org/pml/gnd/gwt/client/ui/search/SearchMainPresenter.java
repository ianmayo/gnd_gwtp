package org.pml.gnd.gwt.client.ui.search;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.bitbucket.es4gwt.client.result.SearchResult;
import org.bitbucket.es4gwt.client.result.SearchResultDiff;
import org.bitbucket.es4gwt.shared.SearchConstants;
import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.bitbucket.es4gwt.shared.spec.SearchDate;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.pml.gnd.gwt.client.event.AddToShoppingCartEvent;
import org.pml.gnd.gwt.client.event.SearchHitSelectionEvent;
import org.pml.gnd.gwt.client.event.SearchParamsChangeEvent;
import org.pml.gnd.gwt.client.event.SearchResultChangeEvent;
import org.pml.gnd.gwt.client.event.SearchResultUpdateRequestedEvent;
import org.pml.gnd.gwt.client.handlers.SearchHandlers;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.model.ShoppingCart;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.place.SearchToPlaceRequestConverter;
import org.pml.gnd.gwt.client.search.SearchService;
import org.pml.gnd.gwt.client.ui.main.MainPresenter;
import org.pml.gnd.gwt.client.util.BaseAsyncCallback;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;
import com.gwtplatform.mvp.client.proxy.RevealRootPopupContentEvent;

public class SearchMainPresenter extends
		Presenter<SearchMainPresenter.MyView, SearchMainPresenter.MyProxy>
		implements SearchHandlers, SearchParamsChangeEvent.Handler,
		SearchHitSelectionEvent.Handler, SearchResultChangeEvent.Handler,
		SearchResultUpdateRequestedEvent.Handler, AddToShoppingCartEvent.Handler
{

	@ProxyCodeSplit
	@NameToken(NameTokens.search)
	public interface MyProxy extends ProxyPlace<SearchMainPresenter>
	{
	}

	@Override
	protected void revealInParent()
	{
		RevealContentEvent.fire(this, MainPresenter.CONTENT_SLOT, this);
	}

	public interface MyView extends View, HasUiHandlers<SearchHandlers>
	{

		HasText getHitCountLabel();

		HasEnabled getAddToCartButton();

		void expandHeader();

		void shrinkNotifierSlot();

	}

	@ContentSlot
	public static final Type<RevealContentHandler<?>> RESULT_CHANGE_NOTIFIER_SLOT = new Type<RevealContentHandler<?>>();
	@ContentSlot
	public static final Type<RevealContentHandler<?>> SHOPPING_CART_SLOT = new Type<RevealContentHandler<?>>();
	@ContentSlot
	public static final Type<RevealContentHandler<?>> SEARCH_FORM_SLOT = new Type<RevealContentHandler<?>>();
	@ContentSlot
	public static final Type<RevealContentHandler<?>> SEARCH_RESULT_TABLE_SLOT = new Type<RevealContentHandler<?>>();

	private final SearchService searchService;
	private final PlaceManager placeManager;
	private final ShoppingCart shoppingCart;
	private final Provider<PermalinkDialogPresenter> permalinkDialog;
	private final SearchFormPresenter searchForm;
	private final ResultTablePresenter resultTable;
	private final TimelinePresenter timeline;
	private final ChartPresenter chartPresenter;
	private final SearchToPlaceRequestConverter searchConverter;
	private final ResultChangeNotifierPresenter changeNotifier;

	SearchRequest currentSearch;
	SearchResult lastResult;
	Map<String, ResultTableElement> stringToResultTableElementMap = new HashMap<String, ResultTableElement>();

	@Inject
	public SearchMainPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			SearchService searchService, PlaceManager placeManager,
			ShoppingCart shoppingCart,
			Provider<PermalinkDialogPresenter> searchPermalinkDialogPresenter,
			SearchFormPresenter searchFormPresenter,
			ResultTablePresenter searchResultTablePresenter,
			SearchToPlaceRequestConverter searchConverter,
			ResultChangeNotifierPresenter changeNotifier, TimelinePresenter timeline,
			ChartPresenter chartPresenter)
	{
		super(eventBus, view, proxy);
		getView().setUiHandlers(this);
		this.searchService = searchService;
		this.placeManager = placeManager;
		this.changeNotifier = changeNotifier;
		setInSlot(RESULT_CHANGE_NOTIFIER_SLOT, changeNotifier);
		this.shoppingCart = shoppingCart;
		this.permalinkDialog = searchPermalinkDialogPresenter;
		this.searchForm = searchFormPresenter;
		setInSlot(SEARCH_FORM_SLOT, searchForm);
		this.resultTable = searchResultTablePresenter;
		this.timeline = timeline;
		this.chartPresenter = chartPresenter;
		setInSlot(SEARCH_RESULT_TABLE_SLOT, resultTable);
		this.searchConverter = searchConverter;
	}

	@Override
	protected void onBind()
	{
		super.onBind();
		registerHandler(searchForm.addSearchParamsChangeHandler(this));
		registerHandler(resultTable.addSearchHitSelectionHandler(this));
		addRegisteredHandler(SearchResultChangeEvent.TYPE, this);
		addRegisteredHandler(SearchResultUpdateRequestedEvent.TYPE, this);
		addRegisteredHandler(AddToShoppingCartEvent.TYPE, this);
	}

	@Override
	protected void onReset()
	{
		super.onReset();
		getView().shrinkNotifierSlot();
	}

	@Override
	public void prepareFromRequest(PlaceRequest request)
	{
		super.prepareFromRequest(request);

		SearchRequest searchRequest = extractRequestFromURL(request);

		if (searchRequest.isFiltered())
		{
			currentSearch = searchRequest;
			clear();
			searchForm.load(searchRequest);
			// Lauch the URL-specified search
			send(searchRequest);

		}
		else
		{
			// No valid URL params, simply initialize the filter boxes if needed
			if (resultTable.isEmpty())
			{
				currentSearch = null;
				resetFacets();
			}
		}
	}

	private SearchRequest extractRequestFromURL(PlaceRequest request)
	{
		SearchRequest searchRequest = new SearchRequest();

		// Extracting facet filters
		for (Facet facet : Facet.values())
		{
			String paramValue = request.getParameter(facet.name(), null);
			if (paramValue != null)
			{
				// Add multiple filters for a single facet
				for (String filterValue : paramValue.split(","))
					searchRequest.withFacetFilter(facet, filterValue);
			}
		}

		// Extracting free text
		String text = request.getParameter("TEXT", "");
		if (!text.trim().isEmpty())
			searchRequest.fullTextSearch(text, Facet.SEARCH_PARAMS);

		// Extracting filter mode
		String filterMode = request.getParameter("FILTER_MODE",
				FilterMode.ANY_OF.name());
		searchRequest.withFilterMode(Facet.DATA_TYPE,
				FilterMode.valueOf(filterMode));

		// Extracting Dates
		String startDateParam = request.getParameter("AFTER", "");
		if (!startDateParam.trim().isEmpty())
		{
			Date startDate = DateFormats.SHORT.parse(startDateParam);
			searchRequest.after(new SearchDate(startDate, startDateParam));
		}
		String endDateParam = request.getParameter("BEFORE", "");
		if (!endDateParam.trim().isEmpty())
		{
			Date endDate = DateFormats.SHORT.parse(endDateParam);
			searchRequest.before(new SearchDate(endDate, endDateParam));
		}

		return searchRequest;
	}

	@Override
	public void onSearchParamsChange(SearchParamsChangeEvent event)
	{
		sendRequestFromSearchForm();
		hideNotifier();
	}

	private void sendRequestFromSearchForm()
	{
		SearchRequest request = searchForm.getSearchRequest();
		currentSearch = request;
		if (request.isFiltered())
		{
			send(request);
			updateURL(request);
		}
		else
		{
			// No search query/filter, prompt user
			resetFacets();
			resultTable.clear();
			getView().getHitCountLabel().setText("Please select a filter");
		}
	}

	private void updateURL(SearchRequest request)
	{
		placeManager.updateHistory(searchConverter.convert(request), true);
	}

	@Override
	public void onSearchHitSelection(SearchHitSelectionEvent event)
	{
		if (event.isSelected())
			getView().getAddToCartButton().setEnabled(true);
		else if (!event.isSelected() && !resultTable.hasSelectedItem())
			getView().getAddToCartButton().setEnabled(false);
	}

	private void clear()
	{
		refreshHitCountDisplay(0);
		resultTable.clear();
		hideNotifier();
	}

	private void hideNotifier()
	{
		changeNotifier.clear();
		getView().shrinkNotifierSlot();
	}

	private void resetFacets()
	{
		searchService.facetsOnly(new BaseAsyncCallback<SearchResult>()
		{

			@Override
			public void onSuccess(SearchResult result)
			{
				// Making sure no other search request has been issued since
				if (currentSearch == null || !currentSearch.isFiltered())
					searchForm.updateFacetFilters(result);
			}

		});
	}

	private void send(final SearchRequest request)
	{
		getView().getHitCountLabel().setText("Loading seach results...");
		changeNotifier.clear();
		searchService.send(request, new BaseAsyncCallback<SearchResult>()
		{

			@Override
			public void onSuccess(SearchResult result)
			{
				// Making sure no other search request has been issued since
				if (currentSearch != null && currentSearch.equals(request))
					process(result);
			}

		});
	}

	private void process(SearchResult result)
	{
		this.lastResult = result;
		searchForm.updateFacetFilters(result);
		resultTable.display(result);
		getView().getAddToCartButton().setEnabled(false);
		refreshHitCountDisplay(result.getHitCount());
		if (timeline.isVisible())
		{
			timeline.display(lastResult);
		}

		if (chartPresenter.isVisible())
		{
			chartPresenter.display(lastResult);
		}

		stringToResultTableElementMap.clear();
		for (ResultTableElement resultTableElement : result.list())
		{
			stringToResultTableElementMap.put(resultTableElement.getId(),
					resultTableElement);
		}
	}

	private void refreshHitCountDisplay(int hitCount)
	{
		checkArgument(hitCount >= 0);
		if (hitCount == 0)
			getView().getHitCountLabel().setText("No results");
		else if (hitCount == 1)
			getView().getHitCountLabel().setText("1 result");
		else if (hitCount > SearchConstants.MAX_RESULTS)
			getView().getHitCountLabel().setText(
					SearchConstants.MAX_RESULTS + " results displayed (" + hitCount
							+ " matches)");
		else
			getView().getHitCountLabel().setText(hitCount + " results");
	}

	@Override
	public void addToCartButtonPushed()
	{
		shoppingCart.addAll(resultTable.getSelectedItems());
	}

	@Override
	public void onAddToShoppingCartEvent(AddToShoppingCartEvent event)
	{
		shoppingCart.add(stringToResultTableElementMap.get(event.getDocumentId()));
	}

	@Override
	public void onSaveSearchLinkClick()
	{
		PlaceRequest placeRequest = searchConverter.convert(currentSearch);

		String url = Window.Location.getHref();
		url = url.substring(0, url.indexOf(NameTokens.search));
		PermalinkDialogPresenter dialog = permalinkDialog.get();
		dialog.showPermalink(url + placeManager.buildHistoryToken(placeRequest));
		RevealRootPopupContentEvent.fire(this, dialog);
	}

	@Override
	public void onSearchResultChange(SearchResultChangeEvent event)
	{
		// Making sure the caught event is valid for the current search
		if (currentSearch == null
				|| !currentSearch.equals(event.getSearchRequest())
				|| lastResult == null)
			return;
		SearchResult newResult = event.getSearchResult();
		SearchResultDiff diff = new SearchResultDiff(lastResult, newResult);
		getView().expandHeader();
		changeNotifier.notify(currentSearch, diff);
	}

	@Override
	public void onSearchResultUpdateRequested(
			SearchResultUpdateRequestedEvent event)
	{
		SearchResultDiff diff = event.getResultDiff();
		// Making sure the caught event is valid for the current search
		if (currentSearch == null || !currentSearch.equals(event.getRequest())
				|| lastResult == null
				|| new SearchResultDiff(lastResult, diff.getOldResult()).hasChanges())
			return;
		for (ResultTableElement removedHit : diff.removed())
			resultTable.remove(removedHit);
		for (ResultTableElement addedHit : diff.added())
			resultTable.add(addedHit);
		refreshHitCountDisplay(event.getResultDiff().getNewResult().getHitCount());
		getView().shrinkNotifierSlot();
	}

	@Override
	public void onShowTable()
	{
		setInSlot(SEARCH_RESULT_TABLE_SLOT, resultTable);
	}

	@Override
	public void onShowTimeLine()
	{
		setInSlot(SEARCH_RESULT_TABLE_SLOT, timeline);
		timeline.display(lastResult);
	}

	@Override
	public void onShowChart()
	{
		setInSlot(SEARCH_RESULT_TABLE_SLOT, chartPresenter);
		chartPresenter.display(lastResult);
	}

}
