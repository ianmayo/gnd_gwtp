package org.pml.gnd.gwt.client.ui.search;

import static org.pml.gnd.gwt.client.model.Facet.SEARCH_PARAMS;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.bitbucket.es4gwt.client.result.ResultTerm;
import org.bitbucket.es4gwt.client.result.SearchResult;
import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;
import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.bitbucket.es4gwt.shared.spec.SearchDate;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.pml.gnd.gwt.client.event.SearchParamsChangeEvent;
import org.pml.gnd.gwt.client.event.SearchParamsChangeEvent.Handler;
import org.pml.gnd.gwt.client.event.SearchParamsChangeEvent.HasSearchParamsChangeHandlers;
import org.pml.gnd.gwt.client.handlers.SearchFormHandlers;
import org.pml.gnd.gwt.client.model.Facet;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.HasValue;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;

public class SearchFormPresenter extends PresenterWidget<SearchFormPresenter.MyView>
		implements SearchFormHandlers, HasSearchParamsChangeHandlers {

	public interface MyView extends View, HasUiHandlers<SearchFormHandlers> {

		HasValue<String> getText();

		void clearFilters(Facet facet);

		void addFilter(Facet facet, String term, int count);

		/**
		 * Use when the number of results is unknown, for example when the facet values come from URL parameters
		 */
		void addFilter(ElasticFacet facet, String term);

		void selectAllFilters(ElasticFacet facet);

		void selectFiltersThat(Facet facet, Predicate<String> predicate);

		List<String> getSelectedFilters(Facet facet);

		void setDataTypeFilterMode(FilterMode filterMode);

		FilterMode getDataTypeFilterMode();

		HasValue<Date> getStartDate();

		HasValue<Date> getEndDate();

		void updateFilterBox(Facet facet);

	}

	private final EventBus localEventBus = new SimpleEventBus();

	@Inject
	public SearchFormPresenter(final EventBus eventBus, final MyView view) {
		super(eventBus, view);
		getView().setUiHandlers(this);
	}

	public void clear() {
		// Clear text search
		getView().getText().setValue(null);
		// Clear search params
		for (Facet facet : SEARCH_PARAMS)
			getView().clearFilters(facet);
		// Clear date filters
		getView().getStartDate().setValue(null);
		getView().getEndDate().setValue(null);
	}

	public void updateFacetFilters(SearchResult result) {
		for (Facet facet : SEARCH_PARAMS) {
			Iterator<ResultTerm> filterValues = result.getFacet(facet).iterator();
			if (filterValues.hasNext())
				updateFilters(facet, filterValues);
		}
	}

	private void updateFilters(Facet facet, Iterator<ResultTerm> filterValues) {

		List<String> previouslySelectedFilters = getSelectedFilters(facet);

		getView().clearFilters(facet);

		SortedMap<String, Integer> filters = Maps.newTreeMap();
		// First we add the the previously selected filters with count 0
		for (String filter : previouslySelectedFilters)
			filters.put(filter, 0);
		// Then we add the filters resulting from the search request, overwriting the "previously selected" count (set
		// to 0) if any
		while (filterValues.hasNext()) {
			ResultTerm facetValue = filterValues.next();
			filters.put(facetValue.getTerm(), facetValue.getCount());
		}
		// At this point, "filters" contains all the valid filters PLUS all the previously selected filters

		// Then we add all filters to the filter box
		for (Entry<String, Integer> filterEntry : filters.entrySet())
			getView().addFilter(facet, filterEntry.getKey(), filterEntry.getValue());

		// Last, we programmatically select the filter values that were previously selected
		getView().selectFiltersThat(facet, Predicates.in(previouslySelectedFilters));
	}

	@Override
	public void searchParamsChanged() {
		SearchParamsChangeEvent.fire(localEventBus);
	}

	@Override
	public HandlerRegistration addSearchParamsChangeHandler(Handler handler) {
		return localEventBus.addHandler(SearchParamsChangeEvent.TYPE, handler);
	}

	private void setText(String text) {
		getView().getText().setValue(text);
	}

	private boolean isTextSet() {
		return !getView().getText().getValue().isEmpty();
	}

	public String getText() {
		return getView().getText().getValue();
	}

	public List<String> getSelectedFilters(Facet facet) {
		return getView().getSelectedFilters(facet);
	}

	/**
	 * Adds an unselected filter with unset count
	 */
	private void addFilter(ElasticFacet facet, String term) {
		getView().addFilter(facet, term);
	}

	private void selectAllFilters(ElasticFacet facet) {
		getView().selectAllFilters(facet);
	}

	private void setDataTypeFilterMode(FilterMode filterMode) {
		getView().setDataTypeFilterMode(filterMode);
	}

	public FilterMode getDataTypeFilterMode() {
		return getView().getDataTypeFilterMode();
	}

	private void setStartDate(SearchDate startDate) {
		getView().getStartDate().setValue(startDate.asDate());
	}

	public SearchDate getStartDate() {
		Date startDate = getView().getStartDate().getValue();
		if (startDate == null)
			return null;
		return new SearchDate(startDate, DateFormats.SHORT.formatLocal(startDate));
	}

	private void setEndDate(SearchDate endDate) {
		getView().getEndDate().setValue(endDate.asDate());
	}

	public SearchDate getEndDate() {
		Date endDate = getView().getEndDate().getValue();
		if (endDate == null)
			return null;
		return new SearchDate(endDate, DateFormats.SHORT.formatLocal(endDate));
	}

	public void load(SearchRequest searchRequest) {
		clear();

		// Free text
		if (searchRequest.isTextDefined())
			setText(searchRequest.getText());

		// Facets
		Iterator<Entry<ElasticFacet, Collection<String>>> i = searchRequest.getFacetFiltersIterator();
		while (i.hasNext()) {
			Entry<ElasticFacet, Collection<String>> entry = i.next();
			for (String filterValue : entry.getValue())
				addFilter(entry.getKey(), filterValue);
			selectAllFilters(entry.getKey());
		}

		// Filter Mode
		setDataTypeFilterMode(searchRequest.getFilterMode(Facet.DATA_TYPE));

		// Start date
		if (searchRequest.isStartDateDefined())
			setStartDate(searchRequest.getStartDate());

		// End date
		if (searchRequest.isEndDateDefined())
			setEndDate(searchRequest.getEndDate());
	}

	public SearchRequest getSearchRequest() {
		SearchRequest request = new SearchRequest();
		if (isTextSet())
			request.fullTextSearch(getText(), SEARCH_PARAMS);
		addFacetFilters(request);
		addDateFilters(request);
		return request;
	}

	private void addFacetFilters(SearchRequest request) {
		for (Facet facet : SEARCH_PARAMS) {
			for (String term : getSelectedFilters(facet))
				request.withFacetFilter(facet, term);
		}
		request.withFilterMode(Facet.DATA_TYPE, getDataTypeFilterMode());
	}

	private void addDateFilters(SearchRequest request) {
		SearchDate start = getStartDate();
		if (start != null)
			request.after(start);
		SearchDate end = getEndDate();
		if (end != null)
			request.before(end);
	}

}
