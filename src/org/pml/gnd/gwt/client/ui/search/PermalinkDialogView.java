package org.pml.gnd.gwt.client.ui.search;

import org.pml.gnd.gwt.client.ui.dialog.CloseOnEscapeDialogBox;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.PopupViewImpl;

public class PermalinkDialogView extends PopupViewImpl implements PermalinkDialogPresenter.MyView {

	private final Widget widget;

	@Override
	public Widget asWidget() {
		return widget;
	}

	public interface Binder extends UiBinder<Widget, PermalinkDialogView> {
	}

	@UiField(provided = true) DialogBox dialogBox = new CloseOnEscapeDialogBox();

	@UiField TextBox urlBox;

	@Inject
	public PermalinkDialogView(final EventBus eventBus, final Binder binder) {
		super(eventBus);
		widget = binder.createAndBindUi(this);
	}

	@UiHandler("closeButton")
	void onCloseButtonClick(ClickEvent event) {
		hide();
	}

	@Override
	public HasText getUrlBox() {
		return urlBox;
	}

	@Override
	public void focusUrlBox() {
		urlBox.setFocus(true);
		urlBox.selectAll();
	}

}
