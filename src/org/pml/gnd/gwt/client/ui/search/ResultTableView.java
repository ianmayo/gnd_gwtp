package org.pml.gnd.gwt.client.ui.search;

import static org.pml.gnd.gwt.client.util.date.DateFormats.*;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.bitbucket.es4gwt.shared.SearchConstants;
import org.pml.gnd.gwt.client.handlers.SearchResultTableHandlers;
import org.pml.gnd.gwt.client.ui.cell.HyperlinkCell;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class ResultTableView extends ViewWithUiHandlers<SearchResultTableHandlers>
		implements ResultTablePresenter.MyView {

	private final DataGrid<ResultTableElement> table = new DataGrid<ResultTableElement>();
	private final CheckboxHeader header = new CheckboxHeader();

	public ResultTableView() {

		table.setStyleName("resultTable");

		table.setPageSize(SearchConstants.MAX_RESULTS);

		// ////////
		// NAME //
		// //////
		HyperlinkCell nameCell = new HyperlinkCell();
		Column<ResultTableElement, Hyperlink> nameColumn = new Column<ResultTableElement, Hyperlink>(nameCell) {

			@Override
			public Hyperlink getValue(ResultTableElement object) {
				String displayedName = object.getName();
				if (displayedName == null)
					return new Hyperlink("undefined", getUiHandlers().getTarget(object.getId()));
				if (displayedName.trim().isEmpty())
					displayedName = "N/A";
				return new Hyperlink(displayedName, getUiHandlers().getTarget(object.getId()));
			}
		};
		table.addColumn(nameColumn, "Name");

		// ////////////
		// PLATFORM //
		// //////////
		TextColumn<ResultTableElement> platformColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getPlatform();
			}
		};
		table.addColumn(platformColumn, "Platform");

		// /////////////////
		// PLATFORM TYPE //
		// ///////////////
		TextColumn<ResultTableElement> platformTypeColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getPlatformType();
			}
		};
		table.addColumn(platformTypeColumn, "Platform Type");

		// //////////
		// SENSOR //
		// ////////
		TextColumn<ResultTableElement> sensorColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getSensor();
			}
		};
		table.addColumn(sensorColumn, "Sensor");

		// ///////////////
		// SENSOR TYPE //
		// /////////////
		TextColumn<ResultTableElement> sensorTypeColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getSensorType();
			}
		};
		table.addColumn(sensorTypeColumn, "Sensor Type");

		// /////////
		// TRIAL //
		// ///////
		TextColumn<ResultTableElement> trialColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getTrial();
			}
		};
		table.addColumn(trialColumn, "Trial");

		// /////////
		// TYPE //
		// ///////
		TextColumn<ResultTableElement> typeColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				return object.getType();
			}
		};
		table.addColumn(typeColumn, "Type");

		// /////////
		// DATES //
		// ///////
		TextColumn<ResultTableElement> timePeriodColumn = new TextColumn<ResultTableElement>() {

			@Override
			public String getValue(ResultTableElement object) {
				String start = object.getStart();
				String end = object.getEnd();
				if (start == null && end == null)
					return "";
				if (start == null || start.isEmpty())
					start = "N/A";
				else
					start = convert(start, CUSTOM, DISPLAY_SHORT);
				if (end == null || end.isEmpty())
					end = "N/A";
				else
					end = convert(end, CUSTOM, DISPLAY_SHORT);
				return start + " - " + end;
			}
		};
		table.addColumn(timePeriodColumn, "Time Period");

		// /////////////////
		// ADD TO BASKET //
		// ///////////////
		CheckboxCell addToBasketCell = new CheckboxCell();
		final Column<ResultTableElement, Boolean> addToBasketColumn = new Column<ResultTableElement, Boolean>(addToBasketCell) {

			@Override
			public Boolean getValue(ResultTableElement object) {
				return object.isSelected();
			}
		};
		addToBasketColumn.setFieldUpdater(new FieldUpdater<ResultTableElement, Boolean>() {

			@Override
			public void update(int index, ResultTableElement object, Boolean value) {
				object.setSelected(value);
				if (value)
					getUiHandlers().itemSelected();
				else
					getUiHandlers().itemDeselected();
			}
		});
		addToBasketColumn.setCellStyleNames("rowCheckBox");
		table.addColumn(addToBasketColumn, header);
		table.setColumnWidth(addToBasketColumn, "53px");

		// ////////////////////////
		// HIGHLIGHTING CHANGES //
		// //////////////////////
		table.setRowStyles(new RowStyles<ResultTableElement>() {

			@Override
			public String getStyleNames(ResultTableElement row, int rowIndex) {
				if (row.isAdded())
					return "addedRow";
				else if (row.isRemoved())
					return "removedRow";
				else
					return null;
			}
		});
	}

	@Override
	public HasData<ResultTableElement> asDataDisplay() {
		return table;
	}

	private class CheckboxHeader extends Header<Boolean> {

		public CheckboxHeader() {
			super(new CheckboxCell());
			setHeaderStyleNames("headerCheckBox");
			setUpdater(new ValueUpdater<Boolean>() {

				@Override
				public void update(Boolean value) {
					getUiHandlers().headerClicked(value);
				}
			});
		}

		@Override
		public Boolean getValue() {
			return false;
		}

	}

	@Override
	public Widget asWidget() {
		return table;
	}

	@Override
	public void updateResults() {
		table.setVisibleRangeAndClearData(table.getVisibleRange(), true);
	}

}
