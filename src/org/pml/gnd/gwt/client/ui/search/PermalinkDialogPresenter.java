package org.pml.gnd.gwt.client.ui.search;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.HasText;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.PopupView;
import com.gwtplatform.mvp.client.PresenterWidget;

public class PermalinkDialogPresenter extends PresenterWidget<PermalinkDialogPresenter.MyView> {

	public interface MyView extends PopupView {

		HasText getUrlBox();

		void focusUrlBox();

	}

	@Inject
	public PermalinkDialogPresenter(final EventBus eventBus, final MyView view) {
		super(eventBus, view);
	}

	@Override
	protected void onReveal() {
		super.onReveal();
		getView().focusUrlBox();
	}

	public void showPermalink(String url) {
		getView().getUrlBox().setText(url);
	}

}
