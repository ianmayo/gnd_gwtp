package org.pml.gnd.gwt.client.ui.search;

import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Nullable;

import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.bitbucket.es4gwt.client.result.SearchResult;
import org.pml.gnd.gwt.client.event.SearchHitSelectionEvent;
import org.pml.gnd.gwt.client.event.SearchHitSelectionEvent.Handler;
import org.pml.gnd.gwt.client.event.SearchHitSelectionEvent.HasSearchHitSelectionHandlers;
import org.pml.gnd.gwt.client.handlers.SearchResultTableHandlers;
import org.pml.gnd.gwt.client.place.NameTokens;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;

public class ResultTablePresenter extends PresenterWidget<ResultTablePresenter.MyView>
		implements HasSearchHitSelectionHandlers, SearchResultTableHandlers {

	public interface MyView extends View, HasUiHandlers<SearchResultTableHandlers> {

		HasData<ResultTableElement> asDataDisplay();

		void updateResults();

	}

	private final ListDataProvider<ResultTableElement> model = new ListDataProvider<ResultTableElement>();

	private final EventBus localEventBus = new SimpleEventBus();

	private final PlaceManager placeManager;

	@Inject
	public ResultTablePresenter(final EventBus eventBus, final MyView view, PlaceManager placeManager) {
		super(eventBus, view);
		getView().setUiHandlers(this);
		this.placeManager = placeManager;
		model.addDataDisplay(getView().asDataDisplay());
	}

	@Override
	public HandlerRegistration addSearchHitSelectionHandler(Handler handler) {
		return localEventBus.addHandler(SearchHitSelectionEvent.getType(), handler);
	}

	public void clear() {
		model.getList().clear();
	}

	public void display(SearchResult result) {
		model.setList(result.list());
	}

	public Collection<ResultTableElement> getSelectedItems() {
		return Collections2.filter(model.getList(), IS_SELECTED);
	}

	@Override
	public void itemSelected() {
		SearchHitSelectionEvent.fire(localEventBus, true);
	}

	@Override
	public void itemDeselected() {
		SearchHitSelectionEvent.fire(localEventBus, false);
	}

	private static final Predicate<ResultTableElement> IS_SELECTED = new Predicate<ResultTableElement>() {

		@Override
		public boolean apply(@Nullable ResultTableElement input) {
			return input.isSelected();
		}
	};

	@Override
	public void headerClicked(boolean selected) {
		if (selected)
			selectAll();
		else
			deselectAll();
		getView().updateResults();
	}

	private void selectAll() {
		for (ResultTableElement item : model.getList()) {
			item.setSelected(true);
		}
		itemSelected();
	}

	private void deselectAll() {
		for (ResultTableElement item : model.getList()) {
			item.setSelected(false);
		}
		itemDeselected();
	}

	public boolean hasSelectedItem() {
		return contains(IS_SELECTED);
	}

	public boolean allItemsSelected() {
		return !contains(Predicates.not(IS_SELECTED));
	}

	private boolean contains(Predicate<ResultTableElement> predicate) {
		Iterator<ResultTableElement> i = model.getList().iterator();
		while (i.hasNext()) {
			if (predicate.apply(i.next()))
				return true;
		}
		return false;
	}

	@Override
	public String getTarget(String id) {
		return placeManager.buildHistoryToken(new PlaceRequest(NameTokens.info).with("id", id));
	}

	public boolean isEmpty() {
		return model.getList().isEmpty();
	}

	public int size() {
		return model.getList().size();
	}

	public void remove(ResultTableElement toRemove) {
		toRemove.setRemoved(true);
		moveToTop(toRemove);
	}

	public void add(ResultTableElement toAdd) {
		toAdd.setAdded(true);
		addToTop(toAdd);
	}

	private void addToTop(ResultTableElement hit) {
		model.getList().add(0, hit);
	}

	private void moveToTop(ResultTableElement hit) {
		model.getList().remove(hit);
		addToTop(hit);
	}

}
