package org.pml.gnd.gwt.client.ui.search;

import java.util.Date;

import org.bitbucket.es4gwt.client.result.SearchResult;
import org.pml.gnd.gwt.client.event.AddToShoppingCartEvent;
import org.pml.gnd.gwt.client.handlers.TimelineHandlers;
import org.pml.gnd.gwt.client.place.NameTokens;
import org.pml.gnd.gwt.client.util.StringUtils;
import org.pml.gnd.gwt.client.util.date.DateFormat;
import org.pml.gnd.gwt.client.util.date.DateFormats;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;

public class TimelinePresenter extends
		PresenterWidget<TimelinePresenter.MyView> implements TimelineHandlers
{

	public interface MyView extends View, HasUiHandlers<TimelineHandlers>
	{
		void loadJSON(JSONObject obj);

		void initTimeline(Date maxDate, Date minDate);

		void hideBubbles();

		void callback(JavaScriptObject element);

	}

	// private final EventBus localEventBus = new SimpleEventBus();

	private final PlaceManager placeManager;

	Date maxDate;
	Date minDate;
	DateFormat format = DateFormats.CUSTOM;

	private SearchResult result;
			//DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@Inject
	public TimelinePresenter(final EventBus eventBus, final MyView view,
			PlaceManager placeManager)
	{
		super(eventBus, view);
		getView().setUiHandlers(this);
		this.placeManager = placeManager;
	}

	public void display(SearchResult result)
	{
		this.result = result;
		try
		{
			
			JSONObject obj = new JSONObject();
			obj.put("dateTimeFormat", new JSONString("iso8601"));
			/*
			 * obj.put("wikiURL", new JSONString("http://wiki.url.todo/")); // TODO
			 * obj.put("wikiSection", new JSONString("wikiSection"));// TODO
			 */
			JSONArray events = new JSONArray();

			for (int i = 0; i < result.getHitCount(); i++)
			{

				JSONObject event = new JSONObject();

				if (result.getHit(i).getStart() == null
						|| result.getHit(i).getEnd() == null)
				{
					continue;
				}
				JSONString start = new JSONString(result.getHit(i).getStart());
				Date startDate = format.parse(start.stringValue());

				event.put("start", start);

				if (minDate == null || minDate.after(startDate))
				{
					minDate = startDate;
				}

				JSONString stop = new JSONString(result.getHit(i).getEnd());
				Date stopDate = format.parse(stop.stringValue());

				event.put("end", stop);

				if (maxDate == null || maxDate.before(stopDate))
				{
					maxDate = stopDate;
				}

				event.put("title",
						new JSONString(
								StringUtils.isNonBlank(result.getHit(i).getName()) ? result
										.getHit(i).getName() : ""));

				event.put("description", new JSONString(result.getHit(i).getName()
						+ " - " + result.getHit(i).getTrial()));

				event.put(
						"link",
						new JSONString(Window.Location.getProtocol()
								+ "//"
								+ Window.Location.getHost()
								+ Window.Location.getPath()
								+ "#"
								+ placeManager.buildHistoryToken(new PlaceRequest(
										NameTokens.info).with("id", result.getHit(i).getId()))));

				events.set(i, event);
			}

			obj.put("events", events);
			getView().initTimeline(maxDate, minDate);
			getView().loadJSON(obj);
		}
		catch (Exception e)
		{
			Window.alert("Error handling timeline data.");
		}
	}

	@Override
	protected void onHide()
	{
		super.onHide();
		getView().hideBubbles();
	}

	@Override
	public void addToShoppingCart(String id)
	{
		AddToShoppingCartEvent.fire(getEventBus(), id);
	}

	@Override
	public void recreate()
	{
		display(result);
	}

}
