package org.pml.gnd.gwt.client.ui.search;

import org.pml.gnd.gwt.client.handlers.ChartHandlers;
import org.pml.gnd.gwt.client.model.Document;
import org.pml.gnd.gwt.client.ui.map.OpenStreetMap;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.query.client.css.RGBColor;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class ChartView extends ViewWithUiHandlers<ChartHandlers> implements
		ChartPresenter.MyView
{

	private final Panel mapPanel;
	private final OpenStreetMap map;
	private final Button recenterButton;
	private ClickHandler onRecenterButtonClick = new ClickHandler()
	{
		
		@Override
		public void onClick(ClickEvent event)
		{
			map.recenter();
		}
	};

	public ChartView()
	{
		// Init Map
		map = new OpenStreetMap();
		mapPanel = new FlowPanel();
		
		recenterButton= new Button("Recenter", onRecenterButtonClick );
		recenterButton.addStyleName("recenter");
		
		mapPanel.add(recenterButton);
		mapPanel.add(map);
		
	}

	@Override
	public Widget asWidget()
	{
		return mapPanel;
	}

	@Override
	public void drawTrackOnMap(Document document, RGBColor rgbColor)
	{
		map.drawTrack(document, rgbColor);
	}
	
	@Override
	public void clearMap()
	{
		map.clear();
	}

}
