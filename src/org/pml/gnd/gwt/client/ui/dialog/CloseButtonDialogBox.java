package org.pml.gnd.gwt.client.ui.dialog;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;

public class CloseButtonDialogBox extends CloseOnEscapeDialogBox {

	private final Node buttonNode;

	public CloseButtonDialogBox() {
		Element topRightCell = getCellElement(0, 2);
		topRightCell.setInnerHTML("<div style='margin-right: 10px;'><button class='close'>&times;</button></div>");
		buttonNode = topRightCell.getChild(0).getChild(0);
	}

	@Override
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		if (!event.isCanceled() && clickEvent(event) && eventOnButton(event))
			hide();
		super.onPreviewNativeEvent(event);
	}

	private boolean clickEvent(NativePreviewEvent event) {
		return event.getTypeInt() == Event.ONCLICK;
	}

	private boolean eventOnButton(NativePreviewEvent event) {
		return event.getNativeEvent().getEventTarget().equals(buttonNode);
	}
}