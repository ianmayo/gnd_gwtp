package org.pml.gnd.gwt.client.ui.dialog;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.DialogBox;

public class CloseOnEscapeDialogBox extends DialogBox {

	@Override
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		if (keyDownEvent(event) && escapeKey(event))
			hide();
		super.onPreviewNativeEvent(event);
	}

	private boolean keyDownEvent(NativePreviewEvent event) {
		return event.getTypeInt() == Event.ONKEYDOWN;
	}

	private boolean escapeKey(NativePreviewEvent event) {
		return event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE;
	}
}
