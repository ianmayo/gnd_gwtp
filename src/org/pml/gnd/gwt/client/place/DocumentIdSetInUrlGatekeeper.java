package org.pml.gnd.gwt.client.place;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;

@Singleton
public class DocumentIdSetInUrlGatekeeper implements Gatekeeper {

	private final PlaceManager placeManager;

	@Inject
	public DocumentIdSetInUrlGatekeeper(PlaceManager placeManager) {
		this.placeManager = placeManager;
	}

	@Override
	public boolean canReveal() {
		return placeManager.getCurrentPlaceRequest().getParameter("id", null) != null;
	}
}
