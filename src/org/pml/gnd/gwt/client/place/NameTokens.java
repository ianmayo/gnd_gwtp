package org.pml.gnd.gwt.client.place;

public class NameTokens {

	public static final String home = "home";
	public static final String search = "search";
	public static final String info = "info";
	public static final String table = "table";
	public static final String graph = "graph";
	public static final String chart = "chart";
	public static final String timeline = "timeline";
	public static final String tab = "tab";
	public static final String multidocmap = "multidocmap";

	public static String home() {
		return home;
	}

	public static String search() {
		return search;
	}

	public static String info() {
		return info;
	}

	public static String table() {
		return table;
	}

	public static String graph() {
		return graph;
	}

	public static String chart() {
		return chart;
	}

	public static String getMultidocmap() {
		return multidocmap;
	}

}
