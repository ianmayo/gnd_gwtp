package org.pml.gnd.gwt.client.place;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;
import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.pml.gnd.gwt.client.model.Facet;

import com.gwtplatform.mvp.client.proxy.PlaceRequest;

public class SearchToPlaceRequestConverter {

	public PlaceRequest convert(SearchRequest searchRequest) {
		PlaceRequest placeRequest = new PlaceRequest(NameTokens.search);

		// Facets
		Iterator<Entry<ElasticFacet, Collection<String>>> i = searchRequest.getFacetFiltersIterator();
		while (i.hasNext()) {
			Entry<ElasticFacet, Collection<String>> entry = i.next();
			ElasticFacet facet = entry.getKey();
			Collection<String> filters = entry.getValue();
			switch (filters.size()) {
			case 0:
				break;
			case 1:
				placeRequest = placeRequest.with(facet.name(), filters.iterator().next());
				break;
			default:
				StringBuilder sb = new StringBuilder();
				Iterator<String> j = filters.iterator();
				while (j.hasNext()) {
					sb.append(j.next());
					if (j.hasNext())
						sb.append(",");
				}
				placeRequest = placeRequest.with(facet.name(), sb.toString());
				break;
			}
		}

		// Name filter
		if (searchRequest.isTextDefined())
			placeRequest = placeRequest.with("TEXT", searchRequest.getText());

		// Data type filter mode
		FilterMode filterMode = searchRequest.getFilterMode(Facet.DATA_TYPE);
		if (filterMode.equals(FilterMode.ALL_OF))
			placeRequest = placeRequest.with("FILTER_MODE", filterMode.name());

		// Start date
		if (searchRequest.isStartDateDefined())
			placeRequest = placeRequest.with("AFTER", searchRequest.getStartDate().toString());

		// End date
		if (searchRequest.isEndDateDefined())
			placeRequest = placeRequest.with("BEFORE", searchRequest.getEndDate().toString());

		return placeRequest;
	}
}
